import NextAuth from "next-auth";
import Credential from "next-auth/providers/credentials";
import { authorize, logout } from "@/actions";
import { OpenAPI, UserService } from "@/generated";
import { redirect } from "next/navigation";

//TODO: change name "token" to normal. But it's not works this way as default

declare module "next-auth" {
  /**
   * Returned by `api`, `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface User {
    token: string;
  }
}

export const { auth, handlers, signIn, signOut } = NextAuth({
  providers: [
    Credential({
      credentials: {
        token: { label: "Token", type: "text" },
      },
      authorize,
    }),
  ],
  basePath: "/functions/auth",
  trustHost: true,
  callbacks: {
    async jwt({ token, user, session }) {
      if (user) {
        token.user = user;
      }
      return token;
    },
    async session({ user, session, token }) {
      // @ts-ignore
      OpenAPI.TOKEN = token.user.token;
      // @ts-ignore
      session.user = token.user;
      return session;
    },
    // async redirect({ url, baseUrl }) {
    //   return `${baseUrl}/search`;
    // },
  },
  secret: "kLpsyI93q1ntMzXMaRnG93cJ1kgP5eVd08Q3+Rs1mvk=",
});
