"use server";
import { UserFull } from "@/generated";
import { signIn, signOut } from "@/auth";
import { redirect } from "next/navigation";

export async function authorize(credentials: any) {
  try {
    return credentials;
  } catch (error) {
    return null;
  }
}

export async function sign(
  user: UserFull & { token: string; location: string },
) {
  await signIn("credentials", {
    id: `${user.id}`,
    name: user.name,
    token: user.token,
    email: user.email,
    // redirectTo: user.location,
  });
}

export async function logout(location?: string) {
  await signOut({
    redirectTo: location,
  });
}
