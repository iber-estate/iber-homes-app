export type Property = {};

export type steps = "signup" | "code" | "signin";

export enum queryKeys {
  listings = "listings",
  listing = "listing",
  properties = "properties",
  property = "property",
  savedProperties = "saved-properties",
  me = "user-me",
}
