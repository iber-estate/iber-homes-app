export type SearchType = {
  priceMin?: string;
  priceMax?: string;
  bedrooms?: string;
  bathrooms?: string;
  country?: string;
  propertyType?: string;
  ordering?: string;
  page?: number;
  pageSize?: number;
  address?: string;
};

export type FilterType = {
  priceMin?: string;
  priceMax?: string;
  bedrooms?: string[];
  bathrooms?: string[];
  country?: string[];
  propertyType?: string[];
  ordering?: string;
  page?: string;
  pageSize?: number;
  address?: string;
};

export enum FieldsNames {
  COUNTRIES = "country",
  MIN_PRICE = "priceMin",
  MAX_PRICE = "priceMax",
  PROPERTY_TYPE = "propertyType",
  BEDS = "bedrooms",
  BATHS = "bathrooms",
  PAGE = "page",
  PAGE_SIZE = "pageSize",
  ORDERING = "ordering",
  ADDRESS = "address",
}

export type FormValues = {
  [FieldsNames.MIN_PRICE]: string;
  [FieldsNames.MAX_PRICE]: string;
  [FieldsNames.COUNTRIES]: string[];
  [FieldsNames.PROPERTY_TYPE]: string[];
  [FieldsNames.BEDS]: string[];
  [FieldsNames.BATHS]: string[];
};
