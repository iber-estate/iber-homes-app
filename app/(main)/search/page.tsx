import React from "react";
import { SearchView } from "@/app/(main)/search/SearchView";
import { CountryService, OpenAPI, PropertyService } from "@/generated";
import { SearchType } from "@/app/(main)/search/types";
import { getFiltersFromQuery } from "@/app/(main)/search/utils";

const SearchPage = async (query?: any) => {
  const propertyListParams = query?.searchParams as SearchType;
  // TODO: propertyList actually gets an arrays for countries, property type, beds, baths
  // const getProperties = await PropertyService.propertyList({
  //   ...propertyListParams,
  //   ordering: propertyListParams.ordering || "-created_at",
  //   pageSize: 30,
  // });

  const getPropertyTypes = PropertyService.propertyPropertyTypes({});
  const getCountries = CountryService.countryList({});
  const [propertyTypes, countries] = await Promise.all([
    getPropertyTypes,
    getCountries,
  ]);
  const propertyTypesNormalized = propertyTypes.results.map((item) => ({
    label: item.name,
    // @ts-ignore
    value: item.slug,
  }));
  const countriesNormalized = countries.results.map((item) => ({
    label: item.name,
    value: `${item.id}`,
  }));
  // TODO: actually gets the total pages
  // @ts-ignore
  // const total_pages = properties.total_pages;
  return (
    <div className="wrapper">
      <SearchView
        countries={countriesNormalized}
        propertyTypes={propertyTypesNormalized}
        propertiesDefault={[]}
        defaultFilters={getFiltersFromQuery(propertyListParams)}
        totalPages={0}
        count={0}
      />
    </div>
  );
};

export default SearchPage;
