"use client";
import {
  PropertyService,
  RealPropertyList,
  StatisticService,
} from "@/generated";
import React, { useState } from "react";
import { FormProvider, SubmitHandler, useForm } from "react-hook-form";
import { Select } from "@/components/FormComponents/Select";
import Button from "@/components/Button";
import { Typography } from "@/components/Typography";
import { PropertyCard } from "@/components/PropertyCard";
import Price from "@/components/FilterComponents/Price";
import PropertyType from "@/components/FilterComponents/PropertyType";
import BedsAndBaths from "@/components/FilterComponents/BedsAndBaths";
import { useQuery } from "@tanstack/react-query";

import { FieldsNames, FilterType, SearchType } from "@/app/(main)/search/types";
import {
  getActiveFiltersNumber,
  getCleanFilterValues,
  getFiltersQuery,
} from "@/app/(main)/search/utils";
import Pagination from "@/components/Pagination";
import EmptySearch from "@/app/(main)/search/EmptySearch";
import { queryKeys } from "@/types";
import Image from "next/image";
import { CheckboxGroup } from "@/components/FormComponents/CheckboxGroup";
import SelectDumb from "@/components/SelectDumb";
import Card from "@/components/FilterComponents/Card";
import SkeletonGrid from "@/app/(main)/search/SkeletonGrid";
import Suggests from "@/app/(main)/Suggests";

type SearchViewProps = {
  countries: { value: string; label: string }[];
  propertyTypes: { value: string; label: string }[];
  propertiesDefault: RealPropertyList[];
  defaultFilters: FilterType;
  totalPages: number;
  count: number;
};
// TODO: remove isTouched
export const SearchView = (props: SearchViewProps) => {
  const { countries: countriesDict, propertyTypes, defaultFilters } = props;
  const [isTouched, setIsTouched] = useState(false);
  const [searchFocused, setSearchFocused] = useState(false);
  const [isMobileFiltersOpen, setIsMobileFiltersOpen] = useState(false);
  const [currentFilters, setCurrentFilters] = useState<SearchType | undefined>(
    getCleanFilterValues(defaultFilters),
  );
  const [currentPages, setCurrentPages] = useState(0);
  const [currentCount, setCurrentCount] = useState(0);
  const [currentConfigurations, setCurrentConfigurations] = useState(0);
  const methods = useForm<FilterType>({
    mode: "onBlur",
    defaultValues: defaultFilters,
  });
  const { handleSubmit, reset: clearFilters, watch } = methods;
  const formValues = watch();
  const countries = watch(FieldsNames.COUNTRIES);

  const { data: total_properties, isFetching: isStatisticsFetching } = useQuery(
    {
      queryFn: async () => {
        const data = await StatisticService.statisticList();
        return data.total_properties;
      },
      queryKey: ["statistics"],
    },
  );

  const { data: properties, isFetching } = useQuery({
    queryFn: async () => {
      const data = await PropertyService.propertyList({
        ...currentFilters,
        status: "published",
        pageSize: 30,
      } as SearchType);
      // @ts-ignore
      setCurrentPages(data.total_pages);
      // @ts-ignore
      setCurrentCount(data.total_properties);
      setCurrentConfigurations(data.count);
      return data.results;
    },
    queryKey: [queryKeys.properties, { currentFilters }],
    retry: 0,
  });

  const applyFilters = (data: FilterType, isPagination?: boolean) => {
    const page = isPagination ? data.page : "1";
    const ordering = data.ordering || "updated_at";
    const newData = {
      ...data,
      [FieldsNames.PAGE]: page,
    };
    window.history.pushState({}, "", getFiltersQuery(newData));
    setCurrentFilters(getCleanFilterValues(newData));
    methods.setValue(FieldsNames.PAGE, page);
    methods.setValue(FieldsNames.ORDERING, ordering);
    if (isTouched) return;
    setIsTouched(true);
  };

  const getCountriesTitle = (countriesString: string) => {
    const countriesArr = countriesString.split(",");
    if (countriesArr.length > 1) {
      return `${countriesDict.find((item) => item.value === countriesArr[0])?.label} and ${countriesArr.length - 1} more`;
    }
    const countriesNames = countriesArr.map(
      (id) => countriesDict.find((item) => item.value === id)?.label,
    );
    return countriesNames.join(",");
  };

  const onSubmit: SubmitHandler<FilterType> = (data) => {
    setIsMobileFiltersOpen(false);
    applyFilters(data);
  };

  return (
    <div>
      <FormProvider {...methods}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mb-[24px] flex items-center gap-2">
            <Suggests
              widthClass="w-[440px]"
              methods={methods}
              searchFocused={searchFocused}
              setSearchFocused={setSearchFocused}
              noSuggests
            />
            <div>
              <SelectDumb
                placeholder="Countries"
                className="min-w-[280px] sm:hidden"
                title={getCountriesTitle(currentFilters?.country || "")}
              >
                <Card>
                  <CheckboxGroup
                    options={countriesDict}
                    name={FieldsNames.COUNTRIES}
                    onChange={(e) => {
                      const oldValues = formValues[FieldsNames.COUNTRIES] || [];
                      const newFormValues = {
                        ...formValues,
                        [FieldsNames.COUNTRIES]: e?.target?.checked
                          ? [...oldValues, e?.target?.value]
                          : countries?.filter(
                              (country) => country !== e?.target?.value,
                            ),
                      };
                      methods.setValue(
                        "country",
                        newFormValues[FieldsNames.COUNTRIES],
                      );
                      applyFilters(newFormValues);
                    }}
                  />
                </Card>
              </SelectDumb>
            </div>
          </div>
          <div className="hidden sm:mb-[32px] sm:grid sm:grid-cols-2 sm:gap-[12px]">
            <SelectDumb
              title={`Countries ${countries?.length ? `(${countries?.length})` : ""}`}
            >
              <div className="rounded-[8px] border border-stroke">
                <Card>
                  <CheckboxGroup
                    options={countriesDict}
                    name={FieldsNames.COUNTRIES}
                    onChange={(e) => {
                      const oldValues = formValues[FieldsNames.COUNTRIES] || [];
                      const newFormValues = {
                        ...formValues,
                        [FieldsNames.COUNTRIES]: e?.target?.checked
                          ? [...oldValues, e?.target?.value]
                          : countries?.filter(
                              (country) => country !== e?.target?.value,
                            ),
                      };
                      applyFilters(newFormValues);
                    }}
                  />
                </Card>
              </div>
            </SelectDumb>
            <Button
              type="button"
              variant="secondary"
              iconRight={<img src="/icons/filtersIcon.svg" alt="" />}
              onClick={() => {
                setIsMobileFiltersOpen(true);
              }}
            >
              <Typography type="subtitle2">
                Filters{" "}
                {getActiveFiltersNumber(formValues)
                  ? `(${getActiveFiltersNumber(formValues)})`
                  : ""}
              </Typography>
            </Button>
          </div>
          <div
            className={`sm:fixed sm:left-0 sm:top-0 sm:z-50 sm:h-[100vh] sm:w-[100vw] sm:flex-col sm:overflow-scroll sm:bg-beige sm:p-[24px] ${isMobileFiltersOpen ? "sm:flex" : "sm:hidden"}`}
          >
            <div className="hidden sm:flex sm:justify-end">
              <button
                type="button"
                onClick={() => {
                  setIsMobileFiltersOpen(false);
                }}
              >
                <Image
                  src="/icons/cross.svg"
                  alt="cross"
                  width={16}
                  height={16}
                />
              </button>
            </div>
            <div className="mb-[32px] flex gap-[20px] sm:mb-0 sm:grow sm:flex-col">
              <div className="grid grow grid-cols-3 gap-[20px] sm:flex sm:grow sm:flex-col sm:gap-[32px]">
                <Price setCurrentFilters={setCurrentFilters} />
                <PropertyType propTypes={propertyTypes} />
                <BedsAndBaths />
              </div>
              <Button className="sm:w-full">Search</Button>
              <button
                type="button"
                className={`${getActiveFiltersNumber(formValues) ? "" : "-ml-[20px] sm:ml-0"} text-left ${getActiveFiltersNumber(formValues) ? getActiveFiltersNumber(formValues) && "w-[107px]" : "w-0"} max-h-[48px] overflow-hidden text-nowrap text-primary-dark transition-[width] sm:flex sm:w-full sm:justify-center`}
                onClick={() => {
                  setIsMobileFiltersOpen(false);
                  clearFilters({
                    [FieldsNames.MIN_PRICE]: "",
                    [FieldsNames.MAX_PRICE]: "",
                    [FieldsNames.COUNTRIES]: [],
                    [FieldsNames.PROPERTY_TYPE]: [],
                    [FieldsNames.BEDS]: [],
                    [FieldsNames.BATHS]: [],
                    [FieldsNames.PAGE]: "1",
                    [FieldsNames.ORDERING]: formValues[FieldsNames.ORDERING],
                    [FieldsNames.ADDRESS]: "",
                  });
                  setCurrentFilters({
                    // [FieldsNames.COUNTRIES]: countries?.join(","),
                    [FieldsNames.PAGE]: 1,
                    [FieldsNames.ORDERING]: formValues[FieldsNames.ORDERING],
                  });
                  setIsTouched(true);
                  window.history.pushState(
                    {},
                    "",
                    `/search${countries?.length ? `?country=${countries?.join(",")}` : ``}`,
                  );
                }}
              >
                <Typography
                  type="body"
                  className="sm:mx-auto sm:text-[14px] sm:text-primary-dark sm:underline"
                >
                  Clear {getActiveFiltersNumber(formValues) === 1 ? "" : "all"}
                </Typography>
              </button>
            </div>
          </div>

          <div>
            <div className="mb-[28px] sm:mb-[6px]">
              <div className="mb-[24px] flex w-full flex-wrap items-center justify-between sm:mb-[0]">
                <Typography
                  className="m-0 text-primary-dark sm:hidden"
                  type="h5"
                >
                  {isFetching || isStatisticsFetching
                    ? "Searching for properties..."
                    : `${currentCount} total properties`}
                  {!isFetching && !isStatisticsFetching && (
                    <Typography
                      type="h6"
                      className="block pt-[4px] text-defaultText"
                    >
                      {currentConfigurations} configurations
                    </Typography>
                  )}
                </Typography>
                <Typography
                  className="mb-[12px] hidden text-primary-dark sm:block sm:w-full"
                  type="h2"
                >
                  {isFetching || isStatisticsFetching
                    ? "Searching for properties..."
                    : `${currentCount} total properties`}
                  {!isFetching && !isStatisticsFetching && (
                    <Typography
                      type="subtitle2"
                      className="block pt-[4px] text-defaultText"
                    >
                      {currentConfigurations} configurations
                    </Typography>
                  )}
                </Typography>
                <div className="sm:flex sm:w-full sm:items-center sm:justify-between">
                  {!!properties?.length && (
                    <>
                      <Typography
                        type="body"
                        className="hidden text-defaultText sm:block"
                      >
                        Sort by:
                      </Typography>{" "}
                      <Select
                        className="w-[280px] sm:w-[190px]"
                        type="text"
                        selectedHighlight={false}
                        onChange={(e) => {
                          applyFilters({
                            ...formValues,
                            [FieldsNames.ORDERING]: e.target.value,
                          });
                        }}
                        options={[
                          { label: "Price – Low to High", value: "price" },
                          { label: "Price – High to Low", value: "-price" },
                          { label: "Newest first", value: "-created_at" },
                        ]}
                        prefix={window.innerWidth > 620 && "Sort by: "}
                        name={FieldsNames.ORDERING}
                      />
                    </>
                  )}
                </div>
              </div>
              {!properties?.length && !isFetching && <EmptySearch />}
            </div>
            {isFetching ? (
              <SkeletonGrid />
            ) : (
              <div className="mb-[76px] grid grid-cols-3 gap-x-[28px] gap-y-[40px] sm:grid-cols-1">
                {properties?.map((property) => (
                  <PropertyCard key={property.id} property={property} />
                ))}
              </div>
            )}
          </div>
          {!!properties?.length && (
            <Pagination totalPages={currentPages} applyFilters={applyFilters} />
          )}
        </form>
      </FormProvider>
    </div>
  );
};
