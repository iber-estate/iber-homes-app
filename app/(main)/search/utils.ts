import { FieldsNames, FilterType, SearchType } from "@/app/(main)/search/types";

const makeSureIsString = (value?: string | string[]): string | undefined => {
  if (!value) return undefined;
  if (Array.isArray(value)) {
    return value.join(",");
  }
  return value;
};

const handleBedsBathsFilters = (values?: string[]): string[] => {
  if (values?.includes("any")) return [];
  if (values?.includes("4+")) {
    return values?.reduce((acc: string[], value) => {
      if (value === "4+") {
        return [...acc, "4", "5", "6", "7", "8"];
      }
      return [...acc, value];
    }, []);
  }
  return values || [];
};

const handleBedsBathsQuery = (values?: string): string[] => {
  if (!values) return [];
  if (values === "4,5,6,7,8") return ["4+"];
  return values.split(",");
};

export const getActiveFiltersNumber = (
  currentFilters?: SearchType | FilterType,
) => {
  if (!currentFilters) return 0;
  const values = { ...currentFilters };
  delete values[FieldsNames.PAGE];
  delete values[FieldsNames.ORDERING];
  delete values[FieldsNames.COUNTRIES];
  // delete values[FieldsNames.COUNTRIES];
  const activeFilters = Object.values(values).filter((value) => {
    return Array.isArray(value) ? value.length : value;
  });
  return activeFilters.length;
};

export const getPagesOptions = (totalPages: number, currentPage: number) => {
  return [...Array(totalPages)].map((_, i) => ({
    label: `${i + 1}`,
    value: `${i + 1}`,
  }));

  if (totalPages <= 5) {
    return [...Array(totalPages)].map((_, i) => ({
      label: `${i + 1}`,
      value: `${i + 1}`,
    }));
  }

  const pages = [];
  if (currentPage <= 3) {
    for (let i = 1; i <= 5; i++) {
      pages.push({ label: `${i}`, value: `${i}` });
    }
    pages.push({ label: "...", value: "_disabled_" });
    pages.push({ label: `${totalPages}`, value: `${totalPages}` });
  } else if (currentPage > 3 && currentPage < totalPages - 2) {
    pages.push({ label: "1", value: "1" });
    pages.push({ label: "...", value: "_disabled_" });
    for (let i = currentPage - 1; i <= currentPage + 1; i++) {
      pages.push({ label: `${i}`, value: `${i}` });
    }
    pages.push({ label: "...", value: "_disabled_" });
    pages.push({ label: `${totalPages}`, value: `${totalPages}` });
  } else {
    pages.push({ label: "1", value: "1" });
    pages.push({ label: "...", value: "_disabled_" });
    for (let i = totalPages - 4; i <= totalPages; i++) {
      pages.push({ label: `${i}`, value: `${i}` });
    }
  }

  return pages;
};

export const getCleanFilterValues = (values: FilterType): SearchType => {
  const minPrice = values[FieldsNames.MIN_PRICE];
  const maxPrice = values[FieldsNames.MAX_PRICE];
  const countries = values[FieldsNames.COUNTRIES];
  const propertyType = values[FieldsNames.PROPERTY_TYPE];
  const beds = values[FieldsNames.BEDS];
  const baths = values[FieldsNames.BATHS];
  const page = values[FieldsNames.PAGE];
  const cleanValues: SearchType = {
    [FieldsNames.MIN_PRICE]: minPrice?.replace(/[^0-9]/g, ""),
    [FieldsNames.MAX_PRICE]: maxPrice?.replace(/[^0-9]/g, ""),
    [FieldsNames.COUNTRIES]: makeSureIsString(countries),
    [FieldsNames.PROPERTY_TYPE]: makeSureIsString(propertyType),
    [FieldsNames.BEDS]: makeSureIsString(handleBedsBathsFilters(beds)),
    [FieldsNames.BATHS]: makeSureIsString(handleBedsBathsFilters(baths)),
    [FieldsNames.PAGE]: parseInt(page || "1"),
    [FieldsNames.PAGE_SIZE]: values[FieldsNames.PAGE_SIZE],
    [FieldsNames.ORDERING]: values[FieldsNames.ORDERING],
    [FieldsNames.ADDRESS]: values[FieldsNames.ADDRESS],
  };
  const removedEmpty = Object.fromEntries(
    Object.entries(cleanValues).filter(([, value]) => value),
  );
  return removedEmpty;
};

export const objectToQueryString = (
  baseUrl: string,
  params: Record<string, any>,
) => {
  const queryString = Object.keys(params)
    .reduce((acc: any[], key) => {
      const value = params[key];
      if (!value) return acc;
      // If the value is an array, iterate over its elements
      if (Array.isArray(value)) {
        value.forEach((arrayValue) => {
          // Only add to query string if not undefined
          if (arrayValue !== undefined) {
            acc.push(
              `${encodeURIComponent(key)}=${encodeURIComponent(arrayValue)}`,
            );
          }
        });
      } else if (value !== undefined && value !== null) {
        // Only add to query string if value is not undefined or null
        acc.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`);
      }
      return acc;
    }, [])
    .join("&");

  return `${baseUrl}?${queryString}`;
};

export const getFiltersQuery = (formValues: FilterType) => {
  const base = `${window?.location.origin}/search`;
  const cleanFormValues = getCleanFilterValues(formValues);
  return objectToQueryString(base, cleanFormValues);
};

export const getFiltersFromQuery = (query: SearchType): FilterType => {
  const filters: FilterType = {
    [FieldsNames.MIN_PRICE]: query[FieldsNames.MIN_PRICE],
    [FieldsNames.MAX_PRICE]: query[FieldsNames.MAX_PRICE],
    [FieldsNames.COUNTRIES]: query[FieldsNames.COUNTRIES]
      ? query[FieldsNames.COUNTRIES].split(",")
      : [],
    [FieldsNames.PROPERTY_TYPE]: query[FieldsNames.PROPERTY_TYPE]
      ? query[FieldsNames.PROPERTY_TYPE].split(",")
      : [],
    [FieldsNames.BEDS]: query[FieldsNames.BEDS]
      ? handleBedsBathsQuery(query[FieldsNames.BEDS])
      : [],
    [FieldsNames.BATHS]: query[FieldsNames.BATHS]
      ? handleBedsBathsQuery(query[FieldsNames.BATHS])
      : [],
    [FieldsNames.PAGE]: `${query[FieldsNames.PAGE] || 1}`,
    [FieldsNames.ORDERING]: query[FieldsNames.ORDERING] || "-created_at",
    [FieldsNames.ADDRESS]: query[FieldsNames.ADDRESS],
  };
  return filters;
};
