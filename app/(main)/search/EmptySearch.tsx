import React from "react";
import { Typography } from "@/components/Typography";

const EmptySearch = () => {
  return (
    <div className="w-full">
      <Typography type="body">
        Sorry, we couldn’t find any results for these filters
      </Typography>
      <Typography type="body">Try adjusting your search criteria.</Typography>
    </div>
  );
};

export default EmptySearch;
