import React from "react";
import { GalleryGrid } from "@/components/GalleryGrid";
import { CollapseContainer } from "@/components/CollapseContainer/CollapseContainer";
import { Typography } from "@/components/Typography";
import { Map } from "@/components/Map";
import PrivateInfo from "@/app/(main)/invest-project/[project_id]/PrivateInfo";
import Link from "next/link";
import Button from "@/components/Button";
import { ProjectService } from "@/generated";
import { strings } from "@/utils/strings";
import { marked } from "marked";
import { BackButton } from "@/components/BackButton";
import MobileSlider from "@/app/(main)/property/[property_id]/MobileSlider";
import "../../property/[property_id]/default.css";

const ProjectCard = async ({ params }: { params: { project_id: string } }) => {
  const project = await ProjectService.projectRead({
    id: parseInt(params.project_id),
  });

  const descriptionHtmlString = marked(
    project.description || "No description",
    {
      breaks: true,
    },
  );

  return (
    <div className="wrapper">
      <BackButton label="Back to the list of investment project" />
      <div className="relative">
        <GalleryGrid
          pictures={
            project.photos?.map(
              (item) => item.photo || "/images/beach.jpeg",
            ) || ["/images/beach.jpeg"]
          }
          cropElems
          className="sm:hidden"
        />
        <MobileSlider
          pictures={
            project?.photos?.length
              ? project.photos?.map((p) => p.photo || "/images/beach.jpeg")
              : ["/images/beach.jpeg", "/images/beach.jpeg"]
          }
          propertyId={0}
          is_saved={false}
          noSave
        />
        <div className="absolute bottom-[16px] left-[16px] z-0 rounded-[8px] bg-lightbluethree p-[16px] sm:static sm:bg-transparent sm:px-0 sm:py-[12px]">
          <Typography
            type="h5"
            className="mb-[4px] text-primary-dark sm:text-[24px] sm:leading-[28px]"
          >
            {project.name}
          </Typography>
          {project.address?.line_1 && (
            <Typography
              type="body"
              className="mb-[12px] max-w-[300px] truncate text-primary-dark"
            >
              {project.address?.line_1}
            </Typography>
          )}
          <Typography
            type="h6"
            className="mb-0 text-[16px] font-[500] leading-[20px] text-primary-dark sm:text-[14px] sm:leading-[20px] sm:text-[#222]"
          >
            ${strings.addCommas(`${project.invest_amount}`)}
          </Typography>
        </div>
      </div>
      <div className="grid grid-cols-12 gap-[16px] pt-[40px]">
        <div className="col-span-8 sm:col-span-12">
          <Typography
            type="h6"
            className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
          >
            Description
          </Typography>
          <CollapseContainer maxHeight={190} className="mb-[60px]">
            <div className="text-[16px] font-[300] leading-[24px] text-black sm:text-[0.8125rem] sm:leading-[1rem]">
              <div
                className="reset"
                dangerouslySetInnerHTML={{ __html: descriptionHtmlString }}
              ></div>
            </div>
          </CollapseContainer>
          <div className="mb-[60px]">
            <Typography
              type="h6"
              className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
            >
              Location
            </Typography>
            <Map
              property={{} as any}
              lat={project.address?.lat}
              lng={project.address?.lng}
            />
          </div>
          <PrivateInfo
            title="User"
            properties={{
              name: project.user?.name,
              email: project.user?.email,
              phone: project?.phone,
            }}
          />
          <PrivateInfo
            title="Additional info"
            properties={{
              "financial plan": [
                <Link
                  key={1}
                  href={project.financial_plan || ""}
                  className="underline"
                >
                  link
                </Link>,
              ],
              presentation: project.presentations?.length
                ? project.presentations.map((p) => (
                    <Link
                      key={p.id}
                      href={p.presentation_link || ""}
                      className="underline"
                    >
                      link
                    </Link>
                  ))
                : undefined,
              video: project.videos?.length
                ? project.videos.map((v) => (
                    <Link
                      key={v.id}
                      href={v.video_link || ""}
                      className="underline"
                    >
                      link
                    </Link>
                  ))
                : undefined,
            }}
          />
        </div>
        <div className="col-span-4 sm:order-first sm:col-span-12 sm:mb-[16px]">
          <div className="sticky top-[24px] ">
            <div className="mb-[16px] rounded-[8px] bg-[#559D88] px-[24px] py-[28px] sm:px-[28px]">
              <Typography
                className="text-[20px] font-[500] leading-[28px] text-white"
                type="h6"
              >
                Financial information
              </Typography>
              <div className="flex flex-col gap-[12px]">
                <div className="flex items-center gap-[8px]">
                  <Typography className="m-0 text-white" type="subtitle3">
                    Investment amount:
                  </Typography>
                  <Typography className="text-white" type="body">
                    $
                    {project.invest_amount
                      ? strings.addCommas(project.invest_amount.toString())
                      : "Unknown"}
                  </Typography>
                </div>
                <div className="flex items-center gap-[8px]">
                  <Typography className="m-0 text-white" type="subtitle3">
                    Expected profit:
                  </Typography>
                  <Typography className="text-white" type="body">
                    {project.expected_profit}% per annum
                  </Typography>
                </div>
                {project.payback_period && (
                  <div className="flex items-center gap-[8px]">
                    <Typography className="m-0 text-white" type="subtitle3">
                      Payback period:
                    </Typography>
                    <Typography className="text-white" type="body">
                      {Math.round(project.payback_period / 12)} years
                    </Typography>
                  </div>
                )}
              </div>
            </div>
            <Link
              href={`https://wa.me/${project.user?.phone || "+6282341882726"}`}
              className="w-full"
            >
              <Button
                className="w-full sm:font-[400]"
                iconLeft="/icons/whatsapp.svg"
              >
                Contact us
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProjectCard;
