"use client";
import React, { ReactNode } from "react";
import { Typography } from "@/components/Typography";
import { useSession } from "next-auth/react";
import Button from "@/components/Button";
import { useStore } from "@/store/useStore";

type PrivateInfoProps = {
  title: string;
  properties: Record<string, any>;
};

const PrivateInfo = (props: PrivateInfoProps) => {
  const { title, properties } = props;
  const { setStore } = useStore();
  const openAuth = () => {
    setStore({ isAuthModalOpen: true });
  };
  const session = useSession();

  const getProperties = (properties: Record<string, any>) => {
    return Object.keys(properties).map((key) => {
      if (!properties[key]) return;
      if (typeof properties[key] === "string") {
        return (
          <Typography
            type="body"
            key={key}
            className="flex items-center gap-[8px]"
          >
            <Typography type="subtitle3" className="mb-0 text-black">
              {key.slice(0, 1).toUpperCase()}
              {key.slice(1)}:
            </Typography>{" "}
            {properties[key]}
          </Typography>
        );
      }
      return properties[key].map((property: ReactNode, index: number) => (
        <Typography
          type="body"
          key={index}
          className="flex items-center gap-[8px]"
        >
          <Typography type="subtitle3" className="mb-0 text-black">
            {key.slice(0, 1).toUpperCase()}
            {key.slice(1)} {properties[key].length > 1 ? index + 1 : ""}:
          </Typography>{" "}
          {property}
        </Typography>
      ));
    });
  };

  if (!session?.data?.user) {
    return (
      <div className="mb-[60px]">
        <Typography type="h6">{title}</Typography>
        <Button variant="secondary" onClick={openAuth}>
          Get information
        </Button>
      </div>
    );
  }

  return (
    <div className="mb-[60px]">
      <Typography
        type="h6"
        className="mb-[16px] sm:text-[16px] sm:font-[500] sm:leading-[20px]"
      >
        {title}
      </Typography>
      <div className="flex flex-col gap-[12px]">
        {getProperties(properties)}
      </div>
    </div>
  );
};

export default PrivateInfo;
