import React, { ReactNode } from "react";
import { ComplexService, PropertyService } from "@/generated";
import { Breadcrumbs } from "@/components/Breadcrumbs";
import Link from "next/link";
import { Icons } from "@/components/Icons";
import { Typography } from "@/components/Typography";
import { GalleryGrid } from "@/components/GalleryGrid";
import { CollapseContainer } from "@/components/CollapseContainer/CollapseContainer";
import { twMerge } from "tailwind-merge";
import { marked } from "marked";
import { Map } from "@/components/Map";
import { Features } from "@/app/(main)/property/[property_id]/Features";
import { strings } from "@/utils/strings";
import Contacts from "@/components/Contacts";
import MobileSlider from "@/app/(main)/property/[property_id]/MobileSlider";
import { auth } from "@/auth";
import { AdditionalInfo } from "@/app/(main)/property/[property_id]/AdditionalInfo";
import { PropertyCard } from "@/components/PropertyCard";

const types = {
  residential_complex: "Residential Complex",
  villa_complex: "Villa Complex",
  business_complex: "Business Complex",
  hotel: "Hotel",
};

const Block = ({
  children,
  className,
}: {
  children: ReactNode;
  className?: string;
}) => (
  <section className={twMerge("mb-[60px] sm:mb-[32px]", className)}>
    {children}
  </section>
);

const ComplexPage = async ({ params }: { params: { complex_id: string } }) => {
  const session = await auth();
  const complex = await ComplexService.complexRead({
    id: parseInt(params.complex_id),
  });
  console.log("complex", complex);
  const pictures = complex.photos?.map((item) => item.photo || "") || [];
  const description = complex.description;
  const descriptionHtmlString = marked(description || "No description", {
    breaks: true,
  });
  const documents = complex.documents?.map((document, index) => ({
    name: `Document ${index + 1}`,
    url: document.document_link || "",
  }));
  const videos = complex.videos?.map((video, index) => ({
    name: `Video ${index + 1}`,
    url: video.video_link || "",
  }));
  const additionalInfoList = [...documents, ...videos];
  if (complex.price_list) {
    additionalInfoList.push({ name: "Price List", url: complex.price_list });
  }
  return (
    <div className="wrapper">
      <div>
        <div className="sm:hidn mb-[24px]">
          <Breadcrumbs
            startTitle="Property"
            endTitle={
              // @ts-ignore
              session?.user ? complex.name : types[complex.complex_type]
            }
          />
        </div>
        <Link
          href="/search"
          className="hidden sm:mb-[12px] sm:flex sm:items-center sm:gap-[4px]"
        >
          <Icons.ArrowIcon className="h-[18px] w-[18px] rotate-180" />
          <Typography type="body">Back to Browse</Typography>
        </Link>
      </div>
      <div className="relative">
        <GalleryGrid
          cropElems
          className="mb-[40px] sm:hidden"
          pictures={pictures}
        />
        <div className="hidden sm:mb-[12px] sm:block">
          {/*TODO: change for general purpose, not only property*/}
          <MobileSlider
            pictures={pictures}
            propertyId={0}
            is_saved={false}
            noSave
          />
        </div>
        <div className="absolute bottom-[24px] left-[24px] rounded-[8px] bg-lightbluethree p-[20px] sm:static sm:mb-[16px] sm:bg-transparent sm:p-0">
          <Typography
            type="h5"
            className="mb-[4px] text-primary-dark sm:hidden"
          >
            {/*@ts-ignore*/}
            {session?.user ? complex.name : types[complex.complex_type]}
          </Typography>
          <Typography
            type="h2"
            className="mb-[4px] hidden text-primary-dark sm:block"
          >
            {/*@ts-ignore*/}
            {session?.user ? complex.name : types[complex.complex_type]}
          </Typography>
          <Typography
            type="body"
            className="mb-[12px] max-w-[400px] truncate text-primary-dark"
          >
            {complex?.address.line_1}
          </Typography>
          <Typography
            type="h6"
            className="mb-[4px] text-primary-dark sm:hidden"
          >
            ${strings.addCommas(complex?.min_price || "0")} –{" "}
            {strings.addCommas(complex?.max_price || "0")}
          </Typography>
          <Typography
            type="subtitle2"
            className="mb-[4px] hidden text-primary-dark sm:block"
          >
            ${strings.addCommas(complex?.min_price || "0")} –{" "}
            {strings.addCommas(complex?.max_price || "0")}
          </Typography>
        </div>
        <div className="mb-[32px] hidden sm:block">
          <Features property={complex} />
        </div>
      </div>
      <div className="grid grid-cols-12 gap-[16px]">
        <div className="col-span-8 sm:col-span-12">
          <Block>
            <Typography
              type="h6"
              className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
            >
              Description
            </Typography>
            <CollapseContainer maxHeight={120}>
              {/*TODO: add to typography div*/}
              {/* @ts-ignore*/}
              <div className="text-[16px] font-[300] leading-[24px] text-black sm:text-[0.8125rem] sm:leading-[1rem]">
                <div
                  className="reset"
                  dangerouslySetInnerHTML={{ __html: descriptionHtmlString }}
                ></div>
              </div>
            </CollapseContainer>
          </Block>
          <Block>
            <Typography
              type="h6"
              className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
            >
              Amenities
            </Typography>
            <CollapseContainer
              maxHeight={85}
              expandText={`Show all ${complex?.amenities?.length} amenities`}
            >
              <div className="flex max-w-[400px] flex-wrap gap-[12px]">
                {complex?.amenities?.map((item: any, index: any) => (
                  <Typography
                    type="body"
                    key={index}
                    className="rounded bg-lightbluethree px-[8px] py-[4px]"
                  >
                    {item?.name}
                  </Typography>
                ))}
              </div>
            </CollapseContainer>
          </Block>
        </div>
        <div className="col-span-4 sm:hidden">
          <Features property={complex} />
        </div>
      </div>
      <Block>
        <Typography
          type="h6"
          className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
        >
          Location
        </Typography>
        <Map property={complex} postfix={complex.id?.toString()} />
      </Block>
      <Block>
        <Contacts developer={complex?.developer} />
      </Block>
      <Block>
        <AdditionalInfo
          // @ts-ignore
          property={complex}
          list={additionalInfoList}
        />
      </Block>
      <Block>
        <Typography
          type="h6"
          className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
        >
          Configuration included
        </Typography>
        <div className="grid grid-cols-3 gap-[28px]">
          {complex.property.map((property) => {
            return (
              <PropertyCard
                key={property.id}
                property={property}
                actionButton={<span></span>}
                extended
              />
            );
          })}
        </div>
      </Block>
      {/*<Block>*/}
      {/*  <Typography type="h6" className="w-full">*/}
      {/*    Additional information*/}
      {/*  </Typography>*/}
      {/*  <Button variant="secondary">Get information</Button>*/}
      {/*</Block>*/}
    </div>
  );
};

export default ComplexPage;
