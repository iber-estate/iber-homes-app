"use client";
import React, { useEffect, useRef, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import TextInput from "@/components/FormComponents/TextInput";
import { Suggest, SuggestService } from "@/generated";
import { useQuery } from "@tanstack/react-query";
import { Typography } from "@/components/Typography";
import Link from "next/link";
import { useRouter } from "next/navigation";

type SuggestProps = {
  defaultValue?: string;
  widthClass?: string;
  onChange?: string;
  methods: any;
  searchFocused: boolean;
  setSearchFocused: (p: boolean) => void;
  noSuggests?: boolean;
};

const Suggests = (props: SuggestProps) => {
  const { setSearchFocused, searchFocused, methods, widthClass, noSuggests } =
    props;
  const search = methods.watch("address");
  const [debounceSearch, setDebounceSearch] = useState(search);

  const { data, isFetching } = useQuery({
    queryKey: ["suggests", debounceSearch],
    queryFn: () => SuggestService.suggestList({ input: debounceSearch }),
    enabled: debounceSearch?.length > 2,
  });

  const getData = (data?: Suggest | any) => {
    const isEmpty =
      !data ||
      (!data.addresses?.length &&
        !data.developers?.length &&
        !data.countries?.length &&
        !data.real_property?.length);

    const dataArr = [
      {
        slug: "",
        name: "Addresses",
        data: data?.addresses,
      },
      {
        slug: "",
        name: "Developers",
        data: data?.developers,
      },
      {
        slug: "country",
        name: "Country",
        data: data?.countries,
      },
      {
        slug: "property",
        name: "Real Property",
        data: data?.real_property,
      },
    ].filter((item) => item.data?.length);
    return { isEmpty, dataArr };
  };

  const getLink = (slug: string, id: string) => {
    if (slug !== "property") {
      return `/search?${slug}=${id}`;
    }
    return `/property/${id}`;
  };

  useEffect(() => {
    if (!search || search.length < 3) {
      setDebounceSearch("");
      return;
    }
    const getData = setTimeout(() => {
      setDebounceSearch(search);
    }, 300);
    return () => clearTimeout(getData);
  }, [search]);

  return (
    <div
      className={`relative inline-block ${widthClass || "w-[498px]"} sm:w-full`}
    >
      <TextInput
        className="overflow-clip truncate sm:pr-[42px] sm:text-[14px]"
        name="address"
        placeholder="Enter country, city, address or keywords"
        iconRight="icons/search.svg"
        onBlur={() => {
          setSearchFocused(false);
        }}
        onFocus={() => {
          setSearchFocused(true);
        }}
      />
      <button
        className="absolute right-1 top-0 h-[45px] w-[45px] cursor-pointer"
        type="submit"
      ></button>
      
      {!noSuggests && debounceSearch && searchFocused && (
        <div className="absolute -bottom-[4px] z-50 w-full translate-y-[100%] rounded-[8px] bg-beige py-[8px] shadow">
          {getData(data).isEmpty ? (
            <Typography type="body" className="mb-0 w-full px-[16px]">
              Nothing found
            </Typography>
          ) : (
            getData(data).dataArr.map((item) => {
              return (
                <div key={item.name}>
                  <Typography
                    type="subtitle3"
                    className="mb-0 px-[16px] py-[8px]"
                  >
                    {item.name}
                  </Typography>
                  <div>
                    {item.data?.map(
                      (p: { id: string; name?: string; line_1: string }) => (
                        <Link
                          onMouseDown={(e) => e.preventDefault()}
                          href={getLink(item.slug, p.id)}
                          key={p.id}
                          className="block w-full px-[16px] py-[8px] text-left  outline-none hover:bg-lightbluethree focus:bg-lightbluethree"
                        >
                          <Typography type="body" className="truncate">
                            {p.name || p.line_1}
                          </Typography>
                        </Link>
                      ),
                    )}
                  </div>
                </div>
              );
            })
          )}
        </div>
      )}
    </div>
  );
};

export default Suggests;
