"use client";
import "./default.css";
import React, { ReactNode } from "react";
import { Typography } from "@/components/Typography";
import { GalleryGrid } from "@/components/GalleryGrid";
import { ToastContainer } from "react-toastify";
import { twMerge } from "tailwind-merge";
import { Map } from "@/components/Map";
import quarterOfYear from "dayjs/plugin/quarterOfYear";
import { marked } from "marked";
import { Breadcrumbs } from "@/components/Breadcrumbs";
import { AdditionalInfo } from "@/app/(main)/property/[property_id]/AdditionalInfo";
import { Like } from "@/components/Like";
import { strings } from "@/utils/strings";
import { PropertyService, RealPropertyList } from "@/generated";
import dayjs from "dayjs";
import { useQuery } from "@tanstack/react-query";
import { useParams } from "next/navigation";
import { Icons } from "@/components/Icons";
import MobileSlider from "@/app/(main)/property/[property_id]/MobileSlider";
import Link from "next/link";
import { Features } from "@/app/(main)/property/[property_id]/Features";
import { CollapseContainer } from "@/components/CollapseContainer/CollapseContainer";
import Contacts from "@/components/Contacts";
import { useSession } from "next-auth/react";
import Button from "@/components/Button";
import Image from "next/image";

dayjs.extend(quarterOfYear);

interface IPropertyPageView {
  property: RealPropertyList;
  property_id?: string;
  forPreview?: boolean;
}

const Block = ({
  children,
  className,
}: {
  children: ReactNode;
  className?: string;
}) => (
  <section className={twMerge("mb-[60px] sm:mb-[32px]", className)}>
    {children}
  </section>
);

const types = {
  residential_complex: "Residential Complex",
  villa_complex: "Villa Complex",
  business_complex: "Business Complex",
  hotel: "Hotel",
};

export const PropertyView = (props: IPropertyPageView) => {
  const { forPreview, property_id, property: propertyServer } = props;
  const params: any = useParams();
  const session = useSession();

  const { data: property, isFetching } = useQuery({
    queryFn: async () => {
      const res = await PropertyService.propertyRead({
        id: parseInt(params.property_id || property_id),
      });
      return res as RealPropertyList;
    },
    initialData: propertyServer,
    staleTime: 1000 * 60 * 5,
    queryKey: ["property", params.property_id || property_id],
  });

  const pictures = property.photos?.map((item) => item.photo || "") || [];
  const description = property?.description;
  const complexDescription = property?.property_complex?.description;
  const descriptionHtmlString = marked(description || "No description", {
    breaks: true,
  });
  const complexDescriptionHtmlString = marked(
    complexDescription || "No description",
    {
      breaks: true,
    },
  );

  if (isFetching)
    return (
      <div className="flex h-[500px] items-center justify-center">
        <h2 className="text-[36px] text-primary-light">Loading...</h2>
      </div>
    );
  if (!property)
    return (
      <div className="flex h-[500px] items-center justify-center">
        <h2 className="text-[36px] text-primary-light">Property not found</h2>
      </div>
    );
  // @ts-ignore
  return (
    <>
      <ToastContainer />
      <section className="mb-[16px]">
        {!forPreview && (
          <>
            <div className="mb-[20px] flex items-baseline justify-between sm:hidden">
              <Breadcrumbs startTitle="Browse" endTitle={property.name} />
              <Like
                id={property.id || 0}
                is_saved={!!property.is_saved}
                variant="button"
              />
            </div>
            <Link
              href="/search"
              className="hidden sm:mb-[12px] sm:flex sm:items-center sm:gap-[4px]"
            >
              <Icons.ArrowIcon className="h-[18px] w-[18px] rotate-180" />
              <Typography type="body">Back to Browse</Typography>
            </Link>
          </>
        )}
        {forPreview && (
          <Block className="mb-[20px]">
            <Typography
              type="h5"
              className="mb-[4px] max-w-[675px] capitalize text-primary-dark"
            >
              {property?.name || "Property Name"}
            </Typography>
            <Typography
              type="body"
              className="mb-[12px] flex items-center gap-[8px] text-gray"
            >
              {/*@ts-ignore*/}
              <span>
                {/*@ts-ignore*/}
                {property.address.line_1 ||
                  property.address ||
                  "Address not available"}
              </span>
            </Typography>
            <Typography type="h5" className="text-black">
              ${property.price && strings.addCommas(property.price.toString())}
            </Typography>
          </Block>
        )}
        <GalleryGrid className="sm:hidden" pictures={pictures} />
        <MobileSlider
          pictures={pictures}
          propertyId={property.id as number}
          is_saved={!!property.is_saved}
        />
      </section>
      <div className="grid grid-cols-12 gap-[16px]">
        <div className="col-span-8 sm:col-span-12">
          {!forPreview && (
            <Block>
              <Typography
                type="h5"
                className="mb-[4px] max-w-[675px] capitalize text-primary-dark sm:hidden"
              >
                {property?.name || "Property Name"}
              </Typography>
              {/*Mobile*/}
              <Typography
                type="h2"
                className="mb-[4px] hidden max-w-[675px] capitalize text-primary-dark sm:block"
              >
                {property?.name || "Property Name"}
              </Typography>
              {/*------*/}
              <Typography
                type="body"
                className="mb-[12px] flex items-center gap-[8px] text-gray sm:hidden"
              >
                {property.address.line_1}
              </Typography>
              {/*Mobile*/}
              <Typography
                type="body"
                className="mb-[8px] flex hidden items-center gap-[8px] text-gray sm:block"
              >
                {property.address.line_1}
              </Typography>
              {/*------*/}
              <Typography type="h5" className="text-black sm:hidden">
                $
                {property.price && strings.addCommas(property.price.toString())}
              </Typography>
              {/*Mobile*/}
              <Typography
                type="subtitle2"
                className="mb-[16px] hidden text-black sm:block"
              >
                $
                {property.price && strings.addCommas(property.price.toString())}
              </Typography>
              {/*------*/}
              <div className="hidden w-full sm:block">
                <Like
                  className="h-[44px] w-full text-[14px] font-[300]"
                  id={property.id || 0}
                  is_saved={!!property.is_saved}
                  variant="button"
                />
              </div>
            </Block>
          )}
          <Block className="hidden sm:block">
            <Features property={property} className="static" />
            {property.property_complex.developer && !forPreview && (
              <div className="mt-[16px] rounded-[8px] bg-lightblueone p-[32px]">
                {session.data?.user && (
                  <Typography type="subtitle3">Partnership Program</Typography>
                )}
                <Typography
                  type="body"
                  className="mb-[20px]  sm:text-[14px] sm:leading-[20px]"
                >
                  {session.data?.user
                    ? "Partner with a regional agency and earn rewards"
                    : "Ask regional partner for advice! Legal and transaction support\n" +
                      "                at all stages"}
                </Typography>
                <Link
                  href={
                    session.data?.user
                      ? property.property_complex.developer
                          .private_partner_url || "https://wa.me/+6282341882726"
                      : property.property_complex.developer.partner_url ||
                        "https://wa.me/+6282341882726"
                  }
                >
                  <Button
                    className="h-[44px] w-full"
                    iconLeft="/icons/whatsapp.svg"
                  >
                    Contact us
                  </Button>
                </Link>
              </div>
            )}
          </Block>
          <Block>
            <Typography
              type="h6"
              className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
            >
              Description
            </Typography>
            <CollapseContainer maxHeight={120}>
              {/*TODO: add to typography div*/}
              {/* @ts-ignore*/}
              <div className="text-[16px] font-[300] leading-[24px] text-black sm:text-[0.8125rem] sm:leading-[1rem]">
                <div
                  className="reset"
                  dangerouslySetInnerHTML={{ __html: descriptionHtmlString }}
                ></div>
              </div>
            </CollapseContainer>
          </Block>
          <Block>
            <Typography
              type="h6"
              className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
            >
              Amenities
            </Typography>
            <CollapseContainer
              maxHeight={85}
              expandText={` Show all ${property?.amenities?.length} amenities`}
            >
              <div className="flex max-w-[400px] flex-wrap gap-[12px]">
                {property?.amenities?.map((item: any, index: any) => (
                  <Typography
                    type="body"
                    key={index}
                    className="rounded bg-lightbluethree px-[8px] py-[4px]"
                  >
                    {item.name}
                  </Typography>
                ))}
              </div>
            </CollapseContainer>
          </Block>
          <Block>
            <Typography
              type="h6"
              className="text-primary-dark sm:text-[16px] sm:font-[500] sm:leading-[20px] cursor-pointer hover:text-[#559D88]"
            >
              <Link href={`/complex/${property.property_complex.id}`}>
                Complex{" "}
                {session.data?.user
                  ? property?.property_complex?.name || "Unknown"
                  : property?.property_complex.complex_type
                    ? types[property?.property_complex.complex_type]
                    : ""}
              </Link>
            </Typography>
            <CollapseContainer maxHeight={120}>
              <div className="text-[16px] font-[300] leading-[24px] text-black sm:text-[0.8125rem] sm:leading-[1rem]">
                <div
                  className="reset"
                  dangerouslySetInnerHTML={{
                    __html: complexDescriptionHtmlString,
                  }}
                ></div>
              </div>
            </CollapseContainer>
          </Block>
          {property?.property_complex?.amenities?.length && (
            <Block>
              <Typography
                type="h6"
                className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
              >
                Amenities in the complex
              </Typography>
              <CollapseContainer
                maxHeight={85}
                expandText={`Show all ${property.property_complex?.amenities?.length} amenities`}
              >
                <div className="flex flex-wrap gap-[12px]">
                  {property?.property_complex?.amenities?.map(
                    (item: any, index: any) => (
                      <Typography
                        type="body"
                        key={index}
                        className="rounded bg-lightbluethree p-[8px]"
                      >
                        {item.name}
                      </Typography>
                    ),
                  )}
                </div>
              </CollapseContainer>
            </Block>
          )}
        </div>
        <div className="col-span-4 ml-auto w-[288px] sm:hidden">
          <div className="sticky top-[24px]">
            <Features
              property={property}
              forPreview={forPreview}
              className="static"
            />
            {property?.property_complex?.developer && !forPreview && (
              <div className="rounded-[8px] bg-lightblueone p-[32px]">
                {session.data?.user && (
                  <Typography type="subtitle3">Partnership Program</Typography>
                )}
                {session.data?.user ? (
                  <Typography type="body" className="mb-[20px]">
                    Partner with a regional agency and earn rewards
                  </Typography>
                ) : (
                  <Typography type="body" className="mb-[20px]">
                    Ask regional partner for advice! Legal and transaction support at all stages
                  </Typography>
                )}
                <Link
                  href={
                    session.data?.user
                      ? property.property_complex.developer
                          .private_partner_url || "https://wa.me/+6282341882726"
                      : property.property_complex.developer.partner_url ||
                        "https://wa.me/+6282341882726"
                  }
                >
                  <Button className="w-full" iconLeft="/icons/whatsapp.svg">
                    Contact agency
                  </Button>
                </Link>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="mb-[82px]">
        {property.address.lng && property.address.lat && (
          <Block>
            <Typography
              type="h6"
              className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
            >
              Location
            </Typography>
            <Map property={property} postfix={property.id?.toString()} />
          </Block>
        )}
        {!forPreview && (
          <>
            <Block>
              <Contacts developer={property.property_complex.developer} />
            </Block>
            <Block>
              <AdditionalInfo
                property={property}
                list={[
                  ...property.documents.map((doc, index) => ({
                    name: `Document ${index + 1}`,
                    url: doc.external_url || "",
                  })),
                  ...property.videos.map((vid, index) => ({
                    name: `Video ${index + 1}`,
                    url: vid.external_url || "",
                  })),
                ]}
              />
            </Block>
            <Block>
              <Typography
                type="h6"
                className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
              >
                Sales terms
              </Typography>
              <Typography type="body">{property.sales_term}</Typography>
            </Block>
            <Block>
              {property?.property_complex?.developer && !forPreview && (
                <div className="grid w-full grid-cols-12 rounded bg-[#A6CBDD] p-[48px] sm:p-[24px] sm:grid-cols-1">
                  <div className="col-span-7">
                    <Typography
                      type="h5"
                      className="mb-[8px] text-primary-dark sm:text-[16px] sm:font-[500] sm:leading-[20px]  "
                    >
                      Join partner programs of regional agencies and get rewards for cooperation
                    </Typography>
                    <Typography
                      type="body"
                      className="mb-[24px] max-w-[500px] text-primary-dark"
                    >
                      Want to launch a partnership program in your region? Learn more about partnership opportunities
                    </Typography>
                    <Button variant="secondary" className="sm:w-full mb-[20px]">
                      <Link href="https://wa.me/+6282341882726">
                        <span className="flex items-center gap-[8px]">
                          <Icons.WhatsApp></Icons.WhatsApp> Contact us
                        </span>
                      </Link>
                    </Button>
                  </div>
                  <Image
                    className="col-span-5 h-auto w-full rounded-[16px]"
                    src="/images/beach2.jpg"
                    alt="beach"
                    width={315}
                    height={188}
                  />
                </div>
              )}
            </Block>
          </>
        )}
      </div>
    </>
  );
};
