"use client";
import React, { useRef, useState } from "react";
import Slider from "react-slick";
import Image from "next/image";
import { Like } from "@/components/Like";
import { Typography } from "@/components/Typography";
import { Icons } from "@/components/Icons";

const settings = {
  dots: false,
  infinite: true,
  swipeToSlide: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
};
type MobileSliderProps = {
  pictures: string[];
  propertyId: number;
  is_saved: boolean;
  noSave?: boolean;
};
const MobileSlider = (props: MobileSliderProps) => {
  const { pictures, propertyId, is_saved, noSave } = props;
  const [slide, setSlide] = useState(0);
  let sliderRef: any = useRef(null);
  return (
    <div className="slider-container relative hidden w-full sm:block">
      {/*TODO: toCamelCase*/}
      {!noSave && (
        <div className="absolute right-[12px] top-[12px] z-[1]">
          <Like id={propertyId} is_saved={is_saved} size="small" />
        </div>
      )}
      <div className="absolute bottom-[12px] right-[12px] z-[1] w-[53px] rounded-[8px] bg-primary-light px-[12px] py-[8px] text-center">
        <Typography type="body" className="text-primary-dark">
          {slide + 1}/{pictures.length}
        </Typography>
      </div>
      <button
        onClick={() => sliderRef.slickPrev()}
        className="absolute left-[12px] top-1/2 z-[1] flex h-[24px] w-[24px] -translate-y-1/2 rotate-180 items-center justify-center rounded-full bg-white"
      >
        <Icons.ArrowIcon className="h-[20px] w-[20px]" color="#999A9F" />
      </button>
      <button
        onClick={() => sliderRef.slickNext()}
        className="absolute right-[12px] top-1/2 z-[1] flex h-[24px] w-[24px] -translate-y-1/2 items-center justify-center rounded-full bg-white"
      >
        <Icons.ArrowIcon className="h-[20px] w-[20px]" color="#999A9F" />
      </button>
      <Slider
        ref={(slider) => {
          sliderRef = slider;
        }}
        {...settings}
        afterChange={(currentSlide) => {
          setSlide(currentSlide);
        }}
      >
        {pictures.map((url) => (
          <Image
            key={url}
            src={url}
            alt="property-photo"
            width={320}
            height={180}
            className="aspect-[320/180] w-full rounded-[8px] object-cover object-center"
          />
        ))}
      </Slider>
    </div>
  );
};

export default MobileSlider;
