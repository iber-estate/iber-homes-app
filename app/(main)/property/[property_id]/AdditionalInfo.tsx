"use client";
import React from "react";
import { Typography } from "@/components/Typography";
import Image from "next/image";
import Link from "next/link";
import { RealPropertyList } from "@/generated";
import Button from "@/components/Button";
import { useStore } from "@/store/useStore";
import { useSession } from "next-auth/react";

type AdditionalInfoProps = {
  property: RealPropertyList;
  list?: { name: string; url: string }[];
};

export const AdditionalInfo = (props: AdditionalInfoProps) => {
  const { property, list } = props;
  const session = useSession();
  const { setStore } = useStore();
  const openAuth = () => {
    setStore({ isAuthModalOpen: true });
  };
  console.log(property);
  if (
    !property?.price_list_url &&
    !property.documents[0]?.external_url &&
    !list?.length
  ) {
    return;
  }

  if (!session?.data?.user) {
    return (
      <>
        <Typography
          type="h6"
          className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
        >
          Additional information
        </Typography>
        <Button variant="secondary" onClick={openAuth}>
          Get information
        </Button>
      </>
    );
  }

  return (
    <div className="default-grid">
      <div className="col-span-3 sm:col-span-6 sm:mb-[32px]">
        <Typography
          type="h6"
          className="sm:text-[16px] sm:font-[500] sm:leading-[20px]"
        >
          Additional information
        </Typography>
        <div className="flex flex-col gap-[8px]">
          {!property?.price_list_url &&
            !property.documents[0]?.external_url &&
            !list && (
              <Typography type="body">No additional information</Typography>
            )}
          {!!property?.price_list_url && (
            <Link
              href={property.price_list_url}
              className="flex"
              target="_blank"
            >
              <Image
                className="mr-[8px]"
                src="/icons/price-list.svg"
                alt="icon"
                width={24}
                height={24}
              />
              <Typography type="body">Price list</Typography>
            </Link>
          )}
          {list &&
            list.map((item, index) => (
              <Link
                href={item.url}
                key={item.url}
                className="flex"
                target="_blank"
              >
                <Image
                  className="mr-[8px]"
                  src="/icons/docs.svg"
                  alt="icon"
                  width={24}
                  height={24}
                />
                <Typography type="body">{item.name}</Typography>
              </Link>
            ))}
          {/*{!!property.documents.length &&*/}
          {/*  !list &&*/}
          {/*  property.documents.map((item, index) => (*/}
          {/*    <Link*/}
          {/*      href={item.external_url || ""}*/}
          {/*      key={index}*/}
          {/*      className="flex"*/}
          {/*      target="_blank"*/}
          {/*    >*/}
          {/*      <Image*/}
          {/*        className="mr-[8px]"*/}
          {/*        src="/icons/docs.svg"*/}
          {/*        alt="icon"*/}
          {/*        width={24}*/}
          {/*        height={24}*/}
          {/*      />*/}
          {/*      <Typography type="body">Document {index + 1}</Typography>*/}
          {/*    </Link>*/}
          {/*  ))}*/}
        </div>
      </div>
      {/*)}*/}
    </div>
  );
};
