"use client";
import React from "react";
import Image from "next/image";
import { Typography } from "@/components/Typography";
import plotArea from "@/public/icons/plotArea.svg";
import house from "@/public/icons/house.svg";
import houses from "@/public/icons/houses.svg";
import bed from "@/public/icons/bed.svg";
import shower from "@/public/icons/bath.svg";
import floor from "@/public/icons/stage.svg";
import water from "@/public/icons/sun.svg";
import hammer from "@/public/icons/hammer.svg";
import building from "@/public/icons/building.svg";
import land from "@/public/icons/land.svg";
import letter from "@/public/icons/letter.svg";
import dayjs from "dayjs";
import percent from "@/public/icons/comission.svg";
import { PropertyComplexList, RealPropertyList } from "@/generated";
import { auth } from "@/auth";
import { useSession } from "next-auth/react";
import quarterOfYear from "dayjs/plugin/quarterOfYear";
import classNames from "classnames";
import { twMerge } from "tailwind-merge";
dayjs.extend(quarterOfYear);

const types = {
  residential_complex: "Residential Complex",
  villa_complex: "Villa Complex",
  business_complex: "Business Complex",
  hotel: "Hotel",
};

type FeaturesProps = {
  property: RealPropertyList | PropertyComplexList | any;
  forPreview?: boolean;
  className?: string;
};

export const Features = (props: FeaturesProps) => {
  const session = useSession();
  const { property, forPreview, className } = props;

  const getFeatures = () => {
    const features = [
      {
        icon: house,
        content:
          property.house_area &&
          `${property.property_type[0].toUpperCase() + property.property_type.slice(1)}: ${property.house_area}m2`,
      },
      {
        icon: plotArea,
        content: property.plot_area && `Plot area: ${property.plot_area}m2`,
      },
      {
        icon: bed,
        content: property.bedrooms && `${property.bedrooms} bedrooms`,
      },
      {
        icon: shower,
        content: property.bathrooms && `${property.bathrooms} bathrooms`,
      },
      {
        icon: floor,
        content: property.floor && `Floor: ${property.floor}`,
      },
      {
        icon: water,
        content:
          property?.property_complex?.distance_to_sea &&
          `Distance to sea: ${(property?.property_complex?.distance_to_sea / 1000).toFixed(1)}km`,
      },
      {
        icon: building,
        // @ts-ignore
        content:
          property.complex_type &&
          // @ts-ignore
          `Type: ${types[property.complex_type || "residential_complex"]}`,
      },
      {
        icon: house,
        content:
          property.numbers_of_properties &&
          `Configurations: ${property.numbers_of_properties}`,
      },
      {
        icon: land,
        content: property.land_area && `Land area: ${property.land_area}m`,
      },
      {
        icon: letter,
        content:
          property.year_of_founding &&
          `Year of foundation: ${dayjs(property.year_of_founding).format("YYYY")}`,
      },
      {
        icon: houses,
        content:
          property.total_properties &&
          `Total properties: ${property.total_properties}`,
      },
      {
        icon: water,
        content:
          property.distance_to_sea &&
          `Distance to sea: ${property.distance_to_sea}`,
      },
      {
        icon: hammer,
        content:
          property.year_built &&
          `Completion date: ${dayjs(property.year_built).format("YYYY")} Q${dayjs(property.year_built).quarter()}`,
      },
    ];
    if (!forPreview && session.data?.user) {
      features.push({
        icon: percent,
        content: property.commission && `Commission: ${property.commission}%`,
      });
    }
    return features;
  };

  return (
    <section
      className={twMerge(
        "sticky top-[24px] mb-[8px]  mt-[10px] w-full rounded bg-lightbluethree p-[32px] sm:m-0 sm:w-full sm:p-[28px]",
        className,
      )}
    >
      <ul className="flex flex-col gap-[12px]">
        {getFeatures().map((item, index) => {
          if (!item.content) return null;
          return (
            <li
              key={index}
              className="flex items-center gap-[8px] sm:gap-[12px]"
            >
              <Image src={item.icon} alt={"house"} />
              <Typography
                type="body"
                className="whitespace-nowrap sm:text-[14px]"
              >
                {item.content}
              </Typography>
            </li>
          );
        })}
      </ul>
    </section>
  );
};
