import React from "react";
import { PropertyView } from "./PropertyView";
import { PropertyService } from "@/generated";
import { marked } from "marked";

const Page = async ({ params }: { params: { property_id: string } }) => {
  try {
    const property = await PropertyService.propertyRead({
      id: parseInt(params.property_id),
    });
    console.log(property);
    const description = property.description;
    const complexDescription = property?.property_complex?.description;
    const descriptionHtmlString = await marked(
      description || "No description",
      {
        breaks: true,
      },
    );

    const complexDescriptionHtmlString = await marked(
      complexDescription || "No description",
      {
        breaks: true,
      },
    );
    property.description = descriptionHtmlString;
    property.property_complex.description = complexDescriptionHtmlString;

    return (
      <div className="wrapper">
        <PropertyView property={property} />
      </div>
    );
  } catch (e) {
    // console.log(e);
  }
  return <div>No property</div>;
};

export default Page;
