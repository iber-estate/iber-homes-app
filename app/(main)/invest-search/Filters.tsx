"use client";
import React, { useEffect, useState } from "react";
import Suggests from "@/app/(main)/Suggests";
import { useFormContext } from "react-hook-form";
import { CheckboxGroup } from "@/components/FormComponents/CheckboxGroup";
import SelectDumb from "@/components/SelectDumb";
import Card from "@/components/FilterComponents/Card";
import Price from "@/components/FilterComponents/Price";
import Button from "@/components/Button";
import {
  defaultValues,
  FieldsNames,
} from "@/app/(main)/invest-search/SearchView";
import useGetCountries from "@/services/hooks/dictionaries/useGetCountries";
import {
  countMainFilters,
  getCountriesTitle,
  normalizeCountries,
} from "@/app/(main)/invest-search/utils";
import Modal from "@/components/Modal";
import { Typography } from "@/components/Typography";
import { getActiveFiltersNumber } from "@/app/(main)/search/utils";
import Image from "next/image";

const Filters = () => {
  const { ADDRESS, PRICE_MIN, PRICE_MAX, COUNTRY } = FieldsNames;
  const mainFilters = [ADDRESS, COUNTRY, PRICE_MIN, PRICE_MAX];
  const [isFiltersEmpty, setIsFiltersEmpty] = React.useState(true);
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const methods = useFormContext();
  const formValues = methods.watch();
  const { countries, isCountriesLoading } = useGetCountries(true);
  const countriesValues = methods.watch(COUNTRY);

  useEffect(() => {
    setIsFiltersEmpty(!countMainFilters(mainFilters, methods));
  }, [formValues]);

  const handleClearAll = () => {
    methods.reset(defaultValues);
    setIsMobileMenuOpen(false);
  };

  return (
    <div className="mb-[32px] flex gap-[20px] sm:mb-[20px] sm:flex-wrap">
      <Suggests
        methods={methods}
        searchFocused={false}
        setSearchFocused={() => {}}
        // widthClass="sm:w-full"
      />
      <SelectDumb
        placeholder="Countries"
        className="grow sm:hidden"
        title={getCountriesTitle(countriesValues, countries)}
      >
        <Card>
          <CheckboxGroup
            options={normalizeCountries(countries)}
            name={COUNTRY}
          />
        </Card>
      </SelectDumb>
      <SelectDumb
        title={`Countries ${countriesValues?.length ? `(${countriesValues?.length})` : ""}`}
        className="hidden sm:block sm:grow"
      >
        <div className="rounded-[8px] border border-stroke">
          <Card>
            <CheckboxGroup
              options={normalizeCountries(countries)}
              name={COUNTRY}
            />
          </Card>
        </div>
      </SelectDumb>
      <Button
        variant="secondary"
        iconRight={<img src="/icons/filtersIcon.svg" alt="" />}
        className="hidden sm:flex sm:grow"
        onClick={() => setIsMobileMenuOpen(!isMobileMenuOpen)}
        type="button"
      >
        <Typography type="subtitle2">
          Filters
          {getActiveFiltersNumber(formValues)
            ? `(${getActiveFiltersNumber(formValues)})`
            : ""}
        </Typography>
      </Button>
      <div
        className={`flex grow gap-[20px] ${isMobileMenuOpen ? "sm:fixed sm:left-0 sm:top-0 sm:z-50 sm:flex sm:h-[100vh] sm:w-full sm:flex-col sm:flex-wrap sm:bg-beige sm:px-[24px] sm:py-[36px]" : "sm:hidden"}`}
      >
        <div className="hidden sm:flex sm:justify-end">
          <button
            type="button"
            onClick={() => {
              setIsMobileMenuOpen(false);
            }}
          >
            <Image src="/icons/cross.svg" alt="cross" width={16} height={16} />
          </button>
        </div>
        <div className="grow">
          <Price setCurrentFilters={() => {}} />
        </div>
        <Button
          className="w-[102px] sm:w-full sm:py-[12px]"
          onClick={() => {
            setIsMobileMenuOpen(false);
          }}
        >
          Search
        </Button>
        <Button
          variant="text"
          className="hidden underline sm:block"
          type="button"
          onClick={handleClearAll}
        >
          Clear all
        </Button>
      </div>

      {!isFiltersEmpty && (
        <button
          className="sm:hidden"
          onClick={handleClearAll}
        >
          Clear
        </button>
      )}
    </div>
  );
};

export default Filters;
