"use client";
import React, { useEffect } from "react";
import { FormProvider, useForm } from "react-hook-form";
import Filters from "@/app/(main)/invest-search/Filters";
import Sort from "@/app/(main)/invest-search/Sort";
import Pagination from "@/components/Pagination";
import { useSearchParams } from "next/navigation";
import {
  getFiltersFromUrl,
  prepareFiltersData,
  syncUrlWithFilters,
} from "@/app/(main)/invest-search/utils";
import useSkipFirstRender from "@/services/hooks/useSkipFirstRender";
import { ProjectService } from "@/generated";
import { useQuery } from "@tanstack/react-query";
import ProjectCardSmall from "@/components/ProjectCardSmall";
import SkeletonGrid from "@/components/SkeletonGrid";
import { Typography } from "@/components/Typography";
import { toast } from "react-toastify";

export enum FieldsNames {
  ADDRESS = "address",
  COUNTRY = "country",
  PRICE_MIN = "priceMin",
  PRICE_MAX = "priceMax",
  PAGE = "page",
  ORDERING = "ordering",
}

export const PAGE_SIZE = 9;

const { ADDRESS, COUNTRY, ORDERING, PRICE_MAX, PRICE_MIN, PAGE } = FieldsNames;

export const defaultValues = {
  [ADDRESS]: "",
  [COUNTRY]: [],
  [PRICE_MIN]: "",
  [PRICE_MAX]: "",
  [PAGE]: "1",
  [ORDERING]: "-created_at",
};

const SearchView = () => {
  const params = useSearchParams();
  const methods = useForm({
    defaultValues: params ? getFiltersFromUrl(params) : defaultValues,
  });
  const formValues = methods.watch();
  const { page, ordering, country } = formValues;

  const {
    data: projects,
    isLoading,
    isFetching,
    refetch,
  } = useQuery({
    queryKey: [
      "projects",
      formValues,
    ],
    queryFn: async () => {
      return ProjectService.projectList(prepareFiltersData(formValues));
    },
    retry: false,
  });

  const onPageChange = (data: any) => {
    const nextPage = data.page;
    methods.setValue("page", `${nextPage}`);
  };

  const onMainSubmit = (data: any) => {
    refetch();
    const currentValues = { ...data, page: "1" };
    syncUrlWithFilters(currentValues);
    methods.reset(currentValues, { keepDefaultValues: true });
  };

  const onAutoSubmit = (data: any) => {
    syncUrlWithFilters(data);
  };
  useEffect(() => {
    if (
      params?.entries().next().value &&
      //@ts-ignore
      params?.entries()?.next()?.value[0] === "afterCreate"
    ) {
      toast.success(
        "Your project is under moderation and will appear in the project list after review",
      );
    }
  }, []);

  useSkipFirstRender(() => {
    console.log("formValues", formValues);
    onAutoSubmit(formValues);
    refetch();
  }, [page, ordering, country]);

  useEffect(() => {
    const values = getFiltersFromUrl(params);
    methods.reset(values);
  }, [params]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onMainSubmit)}>
        <Filters />
        <div className="">
          <div className="mb-[20px] flex justify-between sm:flex-wrap">
            <Typography
              className="m-0 text-primary-dark sm:mb-[8px] sm:w-full sm:text-[24px] sm:font-[400] sm:leading-[28px]"
              type="h5"
            >
              {isLoading || isFetching
                ? "Searching..."
                : `${projects?.count} total projects`}
            </Typography>
            <Sort />
          </div>
          {isLoading || isFetching ? (
            <SkeletonGrid />
          ) : (
            <div className="grid grid-cols-3 gap-[28px] pb-[40px] sm:grid-cols-1">
              {projects?.results.map((project) => (
                <ProjectCardSmall
                  key={project.id}
                  title={project.name || ""}
                  location={project.address?.line_1 || ""}
                  price={project.invest_amount || 0}
                  date={project.created_at?.split("T")[0] || ""}
                  link={`/invest-project/${project.id}`}
                  type={
                    project.goal === "investment"
                      ? "investment project"
                      : "sale of business"
                  }
                  image={
                    project.photos
                      ? project.photos[0]?.photo || "/images/beach.jpeg"
                      : "/images/beach.jpeg"
                  }
                />
              ))}
            </div>
          )}
        </div>
        <Pagination
          totalPages={
            projects?.count ? Math.ceil(projects.count / PAGE_SIZE) : 0
          }
          applyFilters={onPageChange}
        />
      </form>
    </FormProvider>
  );
};

export default SearchView;
