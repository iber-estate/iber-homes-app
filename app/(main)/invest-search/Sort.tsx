import React from "react";
import { Typography } from "@/components/Typography";
import { Select } from "@/components/FormComponents/Select";

const Sort = () => {
  return (
    <div className="sm:flex sm:w-full sm:items-center sm:justify-between">
      <Typography type="body" className="hidden text-defaultText sm:block">
        Sort by:
      </Typography>{" "}
      <Select
        className="w-[280px] sm:w-[190px]"
        type="text"
        selectedHighlight={false}
        options={[
          { label: "Price – Low to High", value: "price" },
          { label: "Price – High to Low", value: "-price" },
          { label: "Newest first", value: "-created_at" },
        ]}
        prefix={window.innerWidth > 620 && "Sort by: "}
        name="ordering"
      />
    </div>
  );
};

export default Sort;
