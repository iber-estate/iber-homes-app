import { CountryList } from "@/generated";
import { ReadonlyURLSearchParams } from "next/navigation";
import { PAGE_SIZE } from "@/app/(main)/invest-search/SearchView";

type SearchParamsType = {
  address: string;
  country: string[];
  priceMin: string;
  priceMax: string;
  page: string;
  ordering: string;
};

export const countMainFilters = (mainFilters: string[], methods: any) =>
  mainFilters.reduce((acc, filter) => {
    const currentFilter = methods.getValues(filter);
    if (Array.isArray(currentFilter)) {
      return currentFilter.length ? acc + 1 : acc;
    }
    return methods.getValues(filter) ? acc + 1 : acc;
  }, 0);

export const normalizeCountries = (countries?: CountryList[]) => {
  if (!countries) return [];
  return countries?.map((country) => ({
    label: country.name,
    value: `${country.id}`,
  }));
};

export const getCountriesTitle = (
  countriesIds: string[],
  countriesDict: CountryList[] | undefined,
) => {
  const countriesMap = new Map(
    countriesDict?.map((country) => [country.id, country.name]),
  );
  return countriesIds.length > 1
    ? `${countriesMap.get(parseInt(countriesIds[0]))} and ${countriesIds.length - 1} more`
    : countriesMap.get(parseInt(countriesIds[0])) || "";
};

export const syncUrlWithFilters = (values: SearchParamsType) => {
  const normalizedValues = {
    address: values.address,
    price_from: values.priceMin?.replace(/[$\s]/g, ""),
    price_to: values.priceMax?.replace(/[$\s]/g, ""),
    country: values.country?.join(","),
    page: values.page,
    ordering: values.ordering,
  };
  const nonEmptyValues = Object.entries(normalizedValues).reduce(
    (acc, [key, value]) => {
      if (value) {
        return { ...acc, [key]: value };
      }
      return acc;
    },
    {},
  );
  window.history.pushState(
    {},
    "",
    `/invest-search?${new URLSearchParams(nonEmptyValues).toString()}`,
  );
};

export const getFiltersFromUrl = (params: ReadonlyURLSearchParams) => {
  const nonEmptyValues: Record<string, string> = {};
  for (const [key, value] of params.entries()) {
    if (value) nonEmptyValues[key] = value;
  }
  const filters: SearchParamsType = {
    address: nonEmptyValues.address || "",
    country: nonEmptyValues.country ? nonEmptyValues.country.split(",") : [],
    priceMin: nonEmptyValues.price_from || "",
    priceMax: nonEmptyValues.price_to || "",
    page: nonEmptyValues.page || "1",
    ordering: nonEmptyValues.ordering || "-created_at",
  };

  return filters;
};

export const prepareFiltersData = (filters: SearchParamsType) => ({
  // ...filters,
  country: filters.country.join(","),
  investAmountMin: filters.priceMin.replace(/[$\s]/g, ""),
  investAmountMax: filters.priceMax.replace(/[$\s]/g, ""),
  page: parseInt(filters.page),
  ordering: filters.ordering,
  pageSize: PAGE_SIZE,
});
