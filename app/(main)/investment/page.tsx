import React from "react";
import Hero from "@/app/(main)/investment/Hero";
import Offer from "@/app/(main)/investment/Offer";
import Projects from "@/app/(main)/investment/Projects";
import Faq from "@/app/(main)/investment/FAQ";
import Cta from "@/app/(main)/investment/CTA";
import { Typography } from "@/components/Typography";

const Page = () => {
  return (
    <div>
      <div>
        <div className="wrapper">
          <Hero />
        </div>
        <Offer />
        <div className="wrapper">
          <Projects />
          <Faq />
        </div>
        <Cta />
      </div>
    </div>
  );
};

export default Page;
