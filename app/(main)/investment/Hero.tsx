import React from "react";
import Image from "next/image";
import { Typography } from "@/components/Typography";

const Hero = () => {
  return (
    <div className="relative mb-[80px] flex h-[476px] flex-col justify-center overflow-hidden rounded-[16px] sm:mb-[40px] sm:h-[288px]">
      <Typography
        type="h1"
        className="relative z-[1] text-center text-[80px] leading-[90px] text-white sm:font-[600]"
      >
        Looking for investment opportunities?
      </Typography>
      <Image
        className="absolute -top-[25%] left-0 w-full sm:top-0 sm:h-[288px]"
        src="/images/invest_hero.jpeg"
        alt="invest hero image"
        width={1024}
        height={476}
      />
    </div>
  );
};

export default Hero;
