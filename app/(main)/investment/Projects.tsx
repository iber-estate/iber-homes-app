import React from "react";
import { Typography } from "@/components/Typography";
import Link from "next/link";
import { ProjectService } from "@/generated";
import ProjectCardSmall from "@/components/ProjectCardSmall";

const Projects = async () => {
  const projects = await ProjectService.projectList({});

  const projectOne = projects?.results[0] || {};
  const projectTwo = projects?.results[1] || {};
  const projectThree = projects?.results[2] || {};
  const projectFour = projects?.results[3] || {};

  return (
    <div className="mb-[80px] flex flex-col sm:mb-[40px] sm:w-[calc(100%+24px)]">
      <div className="mb-[80px] sm:mb-[40px]">
        <Typography
          type="h4"
          className="text-primary-darks mb-[32px] text-center sm:text-left sm:text-[24px] sm:leading-[28px]"
        >
          <span className="text-primary">Exclusive</span> options of projects
        </Typography>
        <div className="mb-[12px] flex w-auto justify-center gap-[28px] sm:justify-start sm:overflow-y-hidden sm:overflow-x-scroll sm:pr-[24px] ">
          <ProjectCardSmall
            title={projectOne?.name || ""}
            location={projectOne.address?.line_1 || ""}
            price={projectOne.invest_amount || 0}
            date={projectOne.created_at?.split("T")[0] || ""}
            type={
              projectOne.goal === "investment"
                ? "investment project"
                : "sale of business"
            }
            image={
              projectOne.photos
                ? projectOne.photos[0]?.photo || "/images/beach.jpeg"
                : "/images/beach.jpeg"
            }
            link={`/invest-project/${projectOne.id}`}
          />
          <ProjectCardSmall
            title={projectTwo.name || ""}
            location={projectTwo.address?.line_1 || ""}
            price={projectTwo.invest_amount || 0}
            date={projectTwo.created_at?.split("T")[0] || ""}
            type={
              projectTwo.goal === "investment"
                ? "investment project"
                : "sale of business"
            }
            image={
              projectTwo.photos
                ? projectTwo.photos[0]?.photo || "/images/beach.jpeg"
                : "/images/beach.jpeg"
            }
            link={`/invest-project/${projectTwo.id}`}
          />
        </div>
        <Link
          href="/invest-search"
          className="block w-full self-center text-right text-[16px] text-primary-dark underline sm:hidden"
        >
          See all
        </Link>
      </div>
      <div>
        <Typography
          type="h4"
          className="mb-[32px] text-center text-primary-dark sm:text-left sm:text-[24px] sm:leading-[28px]"
        >
          <span className="text-primary">Urgent sale</span> of projects
        </Typography>
        <div className="mb-[12px] flex justify-center gap-[28px] sm:justify-start sm:overflow-y-hidden sm:overflow-x-scroll sm:pr-[24px]">
          <ProjectCardSmall
            title={projectThree?.name || ""}
            location={projectThree.address?.line_1 || ""}
            price={projectThree.invest_amount || 0}
            date={projectThree.created_at?.split("T")[0] || ""}
            type={
              projectThree.goal === "investment"
                ? "investment project"
                : "sale of business"
            }
            image={
              projectThree.photos
                ? projectThree.photos[0]?.photo || "/images/beach.jpeg"
                : "/images/beach.jpeg"
            }
            link={`/invest-project/${projectThree.id}`}
          />
          <ProjectCardSmall
            title={projectFour?.name || ""}
            location={projectFour.address?.line_1 || ""}
            price={projectFour.invest_amount || 0}
            date={projectFour.created_at?.split("T")[0] || ""}
            type={
              projectFour.goal === "investment"
                ? "investment project"
                : "sale of business"
            }
            image={
              projectFour.photos
                ? projectFour.photos[0]?.photo || "/images/beach.jpeg"
                : "/images/beach.jpeg"
            }
            link={`/invest-project/${projectFour.id}`}
          />
        </div>
        <Link
          href="/invest-search"
          className="block w-full self-center text-right text-[16px] text-primary-dark underline sm:hidden"
        >
          See all
        </Link>
      </div>
    </div>
  );
};

export default Projects;
