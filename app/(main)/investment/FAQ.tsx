import React from "react";
import Accordion from "@/components/Accordion/Accordion";
import { Typography } from "@/components/Typography";

const accordionItems = [
  {
    title:
      "What are the conditions for placing investment projects on the platform?",
    content:
      "The platform features a variety of waterfront projects, businesses for sale, investment projects and business ideas, as well as renovation and restoration opportunities. You can select the projects you are interested in and access detailed information about each one.",
  },
  {
    title: "How do I list my business or project on the platform?",
    content:
      "To list your business or project, fill out the application form. Provide a detailed description of your proposal, investment requirements and contact information. Our team will review your proposal and contact you if any clarifications are required before posting. Need help? Email iberrest@iber.homes",
  },
  {
    title: "What listing options are available on the platform?",
    content:
      "Basic Listing: A project card with a brief description, detailed information, contact details, documents and visuals.\n" +
      "Premium Listing: Prioritized placement on the homepage and in relevant sections, including additional promotional materials and marketing support.\n" +
      "Advertising placement: Integration of your project advertisement on search pages and in thematic sections.",
  },
  {
    title:
      "What are the conditions for placing investment projects on the platform?",
    content:
      "The conditions depend on the type of project and the selected package. You need to provide a full description of the project, investment requirements and expected income. Our team will help you choose the most suitable package and placement format.",
  },
  {
    title: "How does the platform help entrepreneurs attract investments?",
    content:
      "Iber Homes supports entrepreneurs by providing access to a wide network of investors and partners, offering project listings, analytical tools for market evaluation and promotion to target audiences.",
  },
];

const Faq = () => {
  return (
    <div className="mb-[80px]">
      <Typography
        type="h4"
        className="mb-[32px] text-center text-primary-dark sm:mb-[24px]"
      >
        FAQ
      </Typography>
      <Accordion items={accordionItems} />
    </div>
  );
};

export default Faq;
