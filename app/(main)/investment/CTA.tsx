import React from "react";
import Image from "next/image";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import Link from "next/link";

const Cta = () => {
  return (
    <div className="bg-[#A6CBDD] py-[48px]">
      <div className="wrapper grid grid-cols-2 sm:grid-cols-1">
        <div>
          <div className="w-[360px] sm:w-auto">
            <Typography
              type="h4"
              className="text-primary-dark  sm:text-center sm:text-[24px] sm:leading-[28px]"
            >
              Do you have{" "}
              <span className="inline sm:block">
                a <span className="text-primary">business</span> idea?
              </span>
            </Typography>
            <Typography
              type="h6"
              className="text-primary-dark sm:mb-[20px] sm:text-center sm:text-[14px] sm:leading-[20px]"
            >
              Go and fill out the form!
            </Typography>
            <Link href="/admin/invest-project/create">
              <Button variant="secondary" className="sm:mb-[24px] sm:w-full">
                To present a project
              </Button>
            </Link>
          </div>
        </div>
        <div className="h-[280px] overflow-hidden rounded-[16px] sm:h-[160px]">
          <Image
            src="/images/palms.jpeg"
            alt="alt"
            className="w-full"
            width={544}
            height={280}
          />
        </div>
      </div>
    </div>
  );
};

export default Cta;
