import React from "react";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import Image from "next/image";
import Link from "next/link";

const Card = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className="relative flex h-[372px] flex-col overflow-hidden rounded-[16px] bg-primary-light px-[28px] py-[40px] sm:h-auto">
      {children}
    </div>
  );
};

const Offer = () => {
  return (
    <div className="mb-[80px] bg-lightbluethree py-[48px] sm:mb-[40px]">
      <div className="wrapper">
        <Typography
          type="h4"
          className="mb-[32px] text-center text-primary-dark sm:mb-[12px] sm:text-[24px] sm:leading-[28px]"
        >
          What we can <span className="text-primary">offer</span> you?
        </Typography>
        <div className="grid grid-cols-3 gap-[28px] sm:grid-cols-1">
          <Card>
            <Typography
              type="h5"
              className="text-center text-primary-dark sm:text-[20px] sm:font-[400] sm:leading-[24px]"
            >
              Find a business{" "}
              <span className="inline sm:block">or investment project</span>
            </Typography>
            <Typography
              type="body"
              className="mb-[24px] grow text-primary-dark sm:grow-0 sm:text-[14px] sm:leading-[20px]"
            >
              Are you looking for a project or idea for investment, or are you
              interested in buying a business? Here you can find offers from
              entrepreneurs and business owners.
            </Typography>
            <Link href="/invest-search">
              <Button className="w-full">View projects</Button>
            </Link>
          </Card>
          <Card>
            <Image
              className="absolute -top-1/2 left-0 w-full"
              src="/images/beach.jpeg"
              width={333}
              height={593}
              alt="invest hero"
            />
            <div className="sm:h-[272px]"></div>
          </Card>
          <Card>
            <Typography
              type="h5"
              className="text-center text-primary-dark sm:text-[20px] sm:font-[400] sm:leading-[24px]"
            >
              Present your project or business
            </Typography>
            <Typography
              type="body"
              className="mb-[24px] grow text-primary-dark sm:grow-0 sm:text-[14px] sm:leading-[20px]"
            >
              Do you want to sell your business, or do you have a project or
              idea that needs investment? Fill out the form and find potential
              buyers
            </Typography>
            <Link href="/admin/invest-project/create">
              <Button className="w-full">To present a project</Button>
            </Link>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Offer;
