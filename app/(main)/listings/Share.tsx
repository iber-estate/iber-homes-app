import React from "react";
import { FormCard } from "@/components/Auth/FormCard";
import { Typography } from "@/components/Typography";
import TextInput from "@/components/FormComponents/TextInput";
import Button from "@/components/Button";
import { toast } from "react-toastify";
import { type ListingList } from "@/generated";
export const Share = ({
  onClose,
  selectedListing,
}: {
  // TODO
  onClose: any;
  selectedListing: ListingList;
}) => {
  //   TODO change link to real listings
  const link = `${window.location.origin}/listing/${selectedListing.uuid}`;
  const copyLink = () => {
    navigator.clipboard.writeText(link);
    toast.success("Link copied");
    onClose();
  };
  return (
    <FormCard
      className="w-[552px]"
      onSubmit={async (methods, data) => {}}
      onClose={onClose}
      defaultValues={{
        link,
      }}
    >
      <Typography type="h5" className="mb-[24px] text-primary-dark">
        Share collection
      </Typography>
      <Typography type="subtitle3" className="text-[#222]">
        {selectedListing.name}, {selectedListing.properties.length} properties
      </Typography>
      <div className="relative mb-[32px]">
        <TextInput disabled name="link" className="truncate pr-[45px]" />
        <button
          className="absolute right-[8px] top-[8px] cursor-pointer rounded p-[4px] hover:bg-neutral-100"
          onClick={copyLink}
        >
          <img src="/icons/link.svg" alt="" />
        </button>
      </div>
      <Button onClick={copyLink}>Copy link</Button>
    </FormCard>
  );
};
