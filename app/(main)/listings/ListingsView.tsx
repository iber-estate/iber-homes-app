"use client";
import React, { useState } from "react";
import Button from "@/components/Button";
import { RealPropertyList } from "@/generated";
import { Tabs } from "@/components/Tabs";
import SavedTab from "@/app/(main)/listings/SavedTab";
import { ListingsTab } from "@/app/(main)/listings/ListingsTab";
import { NewListingForm } from "@/app/(main)/listings/NewListingForm";
import Modal from "@/components/Modal";
import AddToListingsForm from "@/app/(main)/listings/AddToListingsForm";
import { Share } from "@/app/(main)/listings/Share";
import { useRouter, useSearchParams } from "next/navigation";
import { Icons } from "@/components/Icons";

type ListingViewProps = {
  properties: RealPropertyList[];
};

export const modals = {
  newListing: "new-listing",
  addToListing: "add-to-listing",
  share: "share",
  confirm: "confirm",
};

type tabs = "collections" | "saved";

const ListingsView = (props: ListingViewProps) => {
  const params = useSearchParams();
  const router = useRouter();
  const tabSlug = params.get("tab");
  const [modalOpen, setModalOpen] = useState<string | null>(null);
  const [activeTab, setActiveTab] = useState<string | null>(tabSlug);
  const [currentProperty, setCurrentProperty] = useState<number | null>(null);
  const [selectedListing, setSelectedListing] = useState<any>();

  const getTabIndex = (slug: tabs | null | string) => {
    if (slug === "collections") return 0;
    if (slug === "saved") return 1;
    return 0;
  };

  const getSlugByIndex = (index: number) => {
    switch (index) {
      case 0:
        return "collections";
        break;
      case 1:
        return "saved";
        break;
      default:
        return "collections";
    }
  };

  const onClose = () => {
    setCurrentProperty(null);
    setModalOpen(null);
  };

  return (
    <div className="wrapper pb-[80px]">
      <Modal isOpen={!!modalOpen} onClose={onClose}>
        {modalOpen === modals.newListing && (
          <NewListingForm
            onClose={onClose}
            currentProperty={currentProperty}
            setModalOpen={setModalOpen}
          />
        )}
        {modalOpen === modals.addToListing && currentProperty && (
          <AddToListingsForm
            property_id={currentProperty}
            setModalOpen={setModalOpen}
            onClose={() => {
              setCurrentProperty(null);
              setModalOpen(null);
            }}
          />
        )}
        {modalOpen === modals.share && (
          <Share onClose={onClose} selectedListing={selectedListing} />
        )}
      </Modal>
      <Tabs
        rightElement={
          getTabIndex(activeTab) === 0 && (
            <Button
              className="w-full"
              iconRight={<Icons.PlusIcon color="#fff" />}
              onClick={() => {
                setModalOpen(modals.newListing);
              }}
            >
              Create a new collection
            </Button>
          )
        }
        defaultActive={getTabIndex(activeTab)}
        onChange={(index) => {
          const slug = getSlugByIndex(index);
          setActiveTab(slug);
          router.push(`${window.location.origin}/listings?tab=${slug}`);
        }}
        tabs={[
          {
            title: "Collections",
            content: (
              <ListingsTab
                setModalOpen={setModalOpen}
                setSelectedListing={setSelectedListing}
              />
            ),
          },
          {
            title: "Saved properties",
            content: (
              <SavedTab
                openModal={(id: number) => {
                  setCurrentProperty(id);
                  setModalOpen(modals.addToListing);
                }}
              />
            ),
          },
        ]}
      />
    </div>
  );
};

export default ListingsView;
