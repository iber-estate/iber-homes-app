import React from "react";
import { PropertyService } from "@/generated";
import ListingsView from "./ListingsView";
import { redirect } from "next/navigation";
import { auth } from "@/auth";
export const revalidate = 1;
export const fetchCache = "force-no-store";

const Page = async () => {
  const session = await auth();
  if (!session?.user) {
    redirect("/login");
  }
  try {
    const properties = await PropertyService.propertyList({
      pageSize: 100,
      ordering: "-updated_at",
    });

    return <ListingsView properties={properties.results} />;
  } catch (error) {
    // console.log(error);
    return <div>Something went wrong</div>;
  }
};

export default Page;
