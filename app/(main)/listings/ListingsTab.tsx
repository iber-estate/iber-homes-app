"use client";
import React from "react";
import { useQuery } from "@tanstack/react-query";
import { queryKeys } from "@/types";
import { ListingService } from "@/generated";
import ListingCard from "@/app/(main)/listings/ListingCard";
import { Typography } from "@/components/Typography";

export type ListingTabProps = {
  setModalOpen: (modal: string | null) => void;
  setSelectedListing: (listing: any) => void;
};
export const ListingsTab = (props: ListingTabProps) => {
  const { data: listings, isFetching } = useQuery({
    queryKey: [queryKeys.listings],
    queryFn: async () =>
      ListingService.listingList({ ordering: "-created_at" }),
  });

  return (
    <div>
      <div className="grid grid-cols-3 gap-[28px] sm:grid-cols-1">
        {listings?.results.map((listing) => (
          <ListingCard key={listing.uuid} listing={listing} {...props} />
        ))}
        {isFetching && "Loading..."}
        {(listings?.results.length === 0 || !listings) && !isFetching && (
          <Typography type="subtitle2" className="w-[670px]">
            Create collections to group your saved properties in a way that
            makes sense to you. Once you create them, they will show up here.
          </Typography>
        )}
      </div>
    </div>
  );
};
