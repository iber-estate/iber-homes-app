import React, { useState } from "react";
import Image from "next/image";
import Button from "@/components/Button";
import { Typography } from "@/components/Typography";
import { type ListingList, ListingService } from "@/generated";
import { queryKeys } from "@/types";

import DeleteButton from "@/app/(main)/listings/DeleteButton";
import Link from "next/link";
import { ListingTabProps } from "@/app/(main)/listings/ListingsTab";
import { modals } from "@/app/(main)/listings/ListingsView";
import Modal from "@/components/Modal";
import AreYouSure from "@/app/(main)/listings/AreYouSure";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";

type ListingCardProps = {
  listing: ListingList;
} & ListingTabProps;

const ListingCard = (props: ListingCardProps) => {
  const router = useRouter();
  const queryClient = useQueryClient();
  const { listing, setModalOpen, setSelectedListing } = props;
  const [modalOpen, setInnerModalOpen] = useState<string | null>(null);
  const thumbnail =
    listing.properties[listing.properties.length - 1]?.photos[0]?.thumbnail;

  const { mutate: deleteListing } = useMutation({
    mutationFn: async () =>
      ListingService.listingDelete({
        uuid: listing?.uuid || "",
      }),
    onSuccess: () => {
      toast.success("Listing deleted");
      queryClient.invalidateQueries({
        queryKey: [queryKeys.listings],
      });
      queryClient.invalidateQueries({
        queryKey: [queryKeys.listing],
      });
      router.push("/listings");
    },
  });

  return (
    <div className="relative flex h-[528px] flex-col overflow-hidden rounded-[8px] bg-lightbluethree sm:h-[492px]">
      <Modal
        isOpen={!!modalOpen}
        onClose={() => {
          setInnerModalOpen(null);
        }}
      >
        {modalOpen === modals.confirm && (
          <AreYouSure
            onSubmit={deleteListing}
            onClose={() => {
              setInnerModalOpen(null);
            }}
            description="The properties in this collection will still be saved in Saved properties"
          />
        )}
      </Modal>

      <div className="absolute right-[16px] top-[16px]">
        <DeleteButton
          alert="Listing deleted"
          onClick={() => setInnerModalOpen(modals.confirm)}
        />
      </div>
      <Link
        href={`/listings/${listing.uuid}`}
        className="mb-[12px] block flex min-h-[220px] w-full justify-center bg-primary-light"
      >
        {thumbnail ? (
          <Image
            src={thumbnail}
            alt="listing"
            width={322}
            height={220}
            className="w-full"
          />
        ) : (
          <Image
            src="/icons/noPhoto.svg"
            alt="no image"
            width={48}
            height={48}
          />
        )}
      </Link>
      <div className="flex grow flex-col px-[16px] pb-[24px] sm:p-[20px]">
        <div className="grow">
          <Link href={`/listings/${listing.uuid}`}>
            <Typography
              type="h6"
              className="mb-[8px] text-primary-dark sm:mb-[4px]"
            >
              {listing.name || "No name"}
            </Typography>
          </Link>
          <Typography
            type="subtitle3"
            className="mb-[12px] text-black sm:hidden"
          >
            {listing.properties.length} properties
          </Typography>
          <Typography
            type="subtitle"
            className="mb-[12px] hidden text-black sm:mb-[8px] sm:block"
          >
            {listing.properties.length} properties
          </Typography>
          <Typography
            type="body"
            className="m-0 max-h-[145px] overflow-y-scroll sm:max-h-[110px]"
          >
            {listing.description}
          </Typography>
        </div>
        <div className="flex gap-2">
          <Link className="grow" href={`/listing/${listing.uuid}`}>
            <Button className="w-full" variant="secondary">
              Preview
            </Button>
          </Link>
          <Button
            className="grow"
            onClick={() => {
              setModalOpen(modals.share);
              setSelectedListing(listing);
            }}
          >
            Share
          </Button>
        </div>
      </div>
    </div>
  );
};

export default ListingCard;
