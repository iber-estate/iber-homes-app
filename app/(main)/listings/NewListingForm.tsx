import React from "react";
import TextInput from "@/components/FormComponents/TextInput";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import { FormCard } from "@/components/Auth/FormCard";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { ListingList, ListingService } from "@/generated";
import { queryKeys } from "@/types";
import { toast } from "react-toastify";
import { modals } from "@/app/(main)/listings/ListingsView";
import useCreateListing from "@/services/hooks/useCreateListing";

type NewListingFormProps = {
  onClose: () => void;
  setModalOpen: (modal: string | null) => void;
  initialValues?: ListingList;
  currentProperty: number | null;
};

export const NewListingForm = (props: NewListingFormProps) => {
  const { onClose, initialValues, setModalOpen, currentProperty } = props;
  const queryClient = useQueryClient();
  const { name, description } = initialValues || {};

  const { mutate: createListing } = useCreateListing({
    onSuccess: async (data) => {
      if (currentProperty) {
        await ListingService.listingAddRealPropertyToListing({
          uuid: data.uuid,
          data: { property_id: currentProperty },
        });
        await queryClient.refetchQueries({
          queryKey: [queryKeys.listings],
        });
        setModalOpen(modals.addToListing);
      } else {
        onClose();
      }
    },
  });

  // const { mutate: createListing } = useMutation({
  //   mutationFn: ListingService.listingCreate,
  //   onSuccess: () => {
  //     onClose();
  //     toast.success("Collection updated");
  //     queryClient.invalidateQueries({
  //       queryKey: [queryKeys.listings],
  //     });
  //     queryClient.invalidateQueries({
  //       queryKey: [queryKeys.listing],
  //     });
  //   },
  // });

  const { mutate: updateListing } = useMutation({
    mutationFn: ListingService.listingPartialUpdate,
    onSuccess: () => {
      onClose();
      toast.success("Collection updated");
      queryClient.invalidateQueries({
        queryKey: [queryKeys.listings],
      });
      queryClient.invalidateQueries({
        queryKey: [queryKeys.listing],
      });
    },
  });

  const onSubmit = async (_: any, data: any) => {
    if (initialValues) {
      updateListing({ uuid: initialValues.uuid as string, data });
      return;
    }
    createListing({ data });
  };

  return (
    <FormCard
      onSubmit={onSubmit}
      onClose={onClose}
      back={
        currentProperty
          ? () => {
              setModalOpen(modals.addToListing);
            }
          : undefined
      }
      className="w-[552px] pb-[72px]"
      defaultValues={{ name, description }}
    >
      <Typography type="h5" className="mb-[24px] text-primary-dark sm:hidden">
        {initialValues ? "Edit collection" : "New collection"}
      </Typography>
      <Typography
        type="h2"
        className="mb-[12px] hidden text-primary-dark sm:block"
      >
        {initialValues ? "Edit collection" : "New collection"}
      </Typography>
      <div className="flex flex-col gap-[16px]">
        <div>
          <TextInput
            name="name"
            label="Name"
            placeholder="Name"
            className="mb-[4px]"
          />
          <Typography type="caption" className="text-defaultText">
            Will be visible by everyone you share with
          </Typography>
        </div>
        <div>
          <TextInput
            name="description"
            type="textarea"
            placeholder="Descripiton"
            label="Descripiton"
            className="mb-[8px]"
          />
          <Typography type="caption" className="text-defaultText">
            Visible only to you
          </Typography>
        </div>

        <Button type="submit" className="w-fit">
          Save
        </Button>
      </div>
    </FormCard>
  );
};
