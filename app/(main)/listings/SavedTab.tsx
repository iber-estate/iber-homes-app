"use client";
import React, { useEffect } from "react";
import { PropertyCard } from "@/components/PropertyCard";
import { RealPropertyList, SavedService } from "@/generated";
import Button from "@/components/Button";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { queryKeys } from "@/types";
import { Typography } from "@/components/Typography";
import Link from "next/link";

type SavedTabProps = {
  openModal: (id: number) => void;
};

const SavedTab = (props: SavedTabProps) => {
  const { openModal } = props;
  const queryClient = useQueryClient();
  const {
    data: properties,
    status,
    isFetching,
  } = useQuery({
    queryKey: [queryKeys.savedProperties],
    queryFn: async () => {
      const res = await SavedService.savedList({});
      return res;
    },
    staleTime: 0,
  });

  useEffect(() => {
    if (status !== "pending") {
      queryClient.invalidateQueries({
        queryKey: [queryKeys.me],
      });
    }
  }, [status]);

  return (
    <div className="grid grid-cols-3 gap-[28px] sm:grid-cols-1">
      {properties?.results?.map((property) => (
        <div className="flex flex-col" key={property?.real_property?.id}>
          <div className="grow">
            <PropertyCard
              property={property.real_property as RealPropertyList}
              className="mb-4"
            />
          </div>
          <Button
            type="button"
            className="w-full"
            onClick={() => openModal(property.real_property?.id || 0)}
          >
            Add to collection
          </Button>
        </div>
      ))}
      {isFetching && "Loading..."}
      {(properties?.results.length === 0 || !properties) && !isFetching && (
        <Typography type="subtitle2" className="w-[670px]">
          No saved properties yet. If you want to save them here, please go
          to the{" "}
          <Link href="/search" className="underline">
            Browse
          </Link>
          .
        </Typography>
      )}
    </div>
  );
};

export default SavedTab;
