import React from "react";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import { FormCard } from "@/components/Auth/FormCard";

type AreYouSureProps = {
  onClose: () => void;
  onSubmit?: () => void;
  title?: string;
  description?: string;
};

const AreYouSure = (props: AreYouSureProps) => {
  const { onClose, onSubmit, title, description } = props;
  return (
    <FormCard
      onClose={onClose}
      onSubmit={async () => {}}
      className="w-[329px] text-center"
    >
      <Typography type="subtitle" className="mb-[12px]">
        {title ? title : "Are you sure?"}
      </Typography>
      <Typography type="caption" className="mb-[28px]">
        {description ? description : "This action cannot be undone."}
      </Typography>
      <div className="flex justify-center gap-[16px]">
        <Button variant="secondary" onClick={onClose}>
          Cancel
        </Button>
        <Button onClick={onSubmit}>Delete</Button>
      </div>
    </FormCard>
  );
};

export default AreYouSure;
