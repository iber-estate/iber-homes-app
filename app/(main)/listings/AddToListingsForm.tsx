"use client";
import React from "react";
import { FormCard } from "@/components/Auth/FormCard";
import Switch from "@/components/FormComponents/Switch";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { queryKeys } from "@/types";
import { ListingList, ListingService } from "@/generated";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import { toast } from "react-toastify";
import { Icons } from "@/components/Icons";
import { modals } from "@/app/(main)/listings/ListingsView";
import Image from "next/image";
import CollapseTitle from "@/app/(main)/listings/CollapseTitle";

type AddToListingsFormProps = {
  setModalOpen: (modal: string | null) => void;
  onClose: () => void;
  property_id: number;
};

const AddToListingsForm = (props: AddToListingsFormProps) => {
  const { setModalOpen, onClose, property_id } = props;
  const queryClient = useQueryClient();

  const { data: listings, isFetching } = useQuery({
    queryKey: [queryKeys.listings],
    queryFn: async () =>
      ListingService.listingList({ ordering: "-created_at" }),
  });

  const inListing = (propertyId: number, listing: ListingList) => {
    return listing.properties.some((p) => p.id === propertyId);
  };

  const defaultValues = listings?.results.reduce((acc, item) => {
    const listingId = item.uuid as string;
    return { ...acc, [listingId]: inListing(property_id, item) };
  }, {});

  const { mutate: addToListing } = useMutation({
    mutationFn: ListingService.listingAddRealPropertyToListing,
    onSuccess: () => {
      queryClient.refetchQueries({
        queryKey: [queryKeys.listings],
      });
    },
    onError: (error: any) => {
      toast.error("Error adding property to collection");
      console.log(error);
    },
  });

  const { mutate: removeFromListing } = useMutation({
    mutationFn: ListingService.listingDelPropertyFromListing,
    onSuccess: () => {
      queryClient.refetchQueries({
        queryKey: [queryKeys.listings],
      });
    },
    onError: (error: any) => {
      toast.error("Error adding property to collection");
      console.log(error);
    },
  });

  if (!listings) return null;
  return (
    <FormCard
      title="Add this property to collection"
      onSubmit={async () => {}}
      onClose={onClose}
      className="h-[544px] w-[552px]"
      defaultValues={defaultValues}
    >
      {/*<Typography type="h5" className="mb-[24px] text-primary-dark">*/}
      {/*  Add this property to collection*/}
      {/*</Typography>*/}
      <div className="mb-[12px] flex h-[335px] flex-col gap-[28px] overflow-y-scroll sm:gap-[16px]">
        {listings?.results.map((listing, index) => (
          <div key={listing.id}>
            <CollapseTitle
              title={listing.name}
              description={listing.description}
            >
              <Switch
                name={listing.uuid as string}
                onChange={(name, isChecked) => {
                  if (isChecked) {
                    addToListing({
                      uuid: listing.uuid as string,
                      data: {
                        property_id,
                      },
                    });
                  } else {
                    removeFromListing({
                      uuid: listing.uuid as string,
                      propertyId: property_id as any,
                    });
                  }
                }}
              />
            </CollapseTitle>
          </div>
        ))}
      </div>
      <div className="flex justify-between">
        <Button
          className="-ml-[24px] text-primary-dark underline sm:p-0 sm:pl-[20px] sm:text-left"
          type="button"
          variant="text"
          iconRight={<Icons.PlusIcon color="#fff" h={"24px"} w="24px" />}
          onClick={() => {
            setModalOpen(modals.newListing);
          }}
        >
          Create new collection
        </Button>
        <Button type="button" onClick={onClose}>
          Cancel
        </Button>
      </div>
    </FormCard>
  );
};

export default AddToListingsForm;
