import React from "react";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import { ListingService, OpenAPI } from "@/generated";
import { PropertyCard } from "@/components/PropertyCard";
import { auth } from "@/auth";
import { ListingView } from "@/app/(main)/listings/[listing_id]/ListingView";

const Page = async ({ params }: any) => {
  return <ListingView params={params} />;
};

export default Page;
