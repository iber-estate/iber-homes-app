"use client";
import React from "react";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import { ListingService } from "@/generated";
import { PropertyCard } from "@/components/PropertyCard";
import { Breadcrumbs } from "@/components/Breadcrumbs";
import ActionButton from "@/app/(main)/listings/[listing_id]/ActionButton";
import { useMutation, useQuery } from "@tanstack/react-query";
import { queryKeys } from "@/types";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";
import { Share } from "@/app/(main)/listings/Share";
import Modal from "@/components/Modal";
import Link from "next/link";
import { NewListingForm } from "@/app/(main)/listings/NewListingForm";
import { modals } from "@/app/(main)/listings/ListingsView";
import AreYouSure from "@/app/(main)/listings/AreYouSure";

export const ListingView = ({ params }: any) => {
  const [modalOpen, setModalOpen] = React.useState<string | null>(null);
  const router = useRouter();
  const { listing_id } = params;

  const onClose = () => {
    setModalOpen(null);
  };

  const { data: listing } = useQuery({
    queryKey: [queryKeys.listing, { id: listing_id }],
    queryFn: async () => {
      return ListingService.listingRead({ uuid: listing_id });
    },
  });

  const { mutate: deleteListing } = useMutation({
    mutationFn: async () =>
      ListingService.listingDelete({
        uuid: listing?.uuid || "",
      }),
    onSuccess: () => {
      toast.success("Listing deleted");
      router.push("/listings?tab=collections");
    },
  });

  if (!listing) return null;
  return (
    <div className="wrapper pb-[80px] sm:pb-[40px]">
      <Modal isOpen={!!modalOpen} onClose={onClose}>
        {modalOpen === modals.share && (
          <Share onClose={onClose} selectedListing={listing} />
        )}
        {modalOpen === modals.newListing && (
          <NewListingForm
            onClose={onClose}
            initialValues={listing}
            setModalOpen={() => {}}
            currentProperty={null}
          />
        )}
        {modalOpen === modals.confirm && (
          <AreYouSure
            onSubmit={deleteListing}
            onClose={() => {
              setModalOpen(null);
            }}
            description="The properties in this collection will still be saved in Saved properties"
          />
        )}
      </Modal>
      <div className="mb-[30px]">
        <Breadcrumbs
          startTitle="Collections"
          endTitle={listing.name}
          prevLink="/listings?tab=collections"
        />
      </div>
      <div className="mb-[32px] flex justify-between sm:flex-wrap">
        <Typography
          type="h5"
          className="flex items-center gap-[8px] text-primary-dark"
        >
          <span>{listing.name}</span>
          <button
            className="mb-1"
            onClick={() => {
              setModalOpen(modals.newListing);
            }}
          >
            <img src="/icons/pen.svg" alt="" />
          </button>
        </Typography>
        <div className="flex gap-[16px] sm:w-full">
          <Button
            className="sm:hidden"
            variant="text"
            onClick={() => {
              setModalOpen(modals.confirm);
            }}
          >
            Delete collection
          </Button>
          <Button
            className="sm:w-full"
            onClick={() => {
              setModalOpen(modals.share);
            }}
          >
            Share
          </Button>
        </div>
      </div>
      <div className="default-grid gap-y-[32px]">
        {listing.properties.map((property: any) => (
          <PropertyCard
            key={property.id}
            property={property}
            className="col-span-2 sm:col-span-6"
            actionButton={
              <ActionButton
                propertyId={property.id}
                listingId={listing.uuid as string}
              />
            }
          />
        ))}
        <div className="col-span-2 flex aspect-[323/220] w-full items-center justify-center rounded-[8px] bg-lightbluethree sm:col-span-6 sm:aspect-auto sm:bg-transparent">
          <Link href="/listings?tab=saved" className="sm:w-full">
            <Button className="sm:hidden">Add saved property</Button>
            <Button className="hidden sm:block sm:w-full" variant="secondary">
              Add saved property
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};
