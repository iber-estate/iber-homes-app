"use client";
import React from "react";
import DeleteButton from "@/app/(main)/listings/DeleteButton";
import { ListingService } from "@/generated";
import { queryKeys } from "@/types";

const ActionButton = ({
  propertyId,
  listingId,
}: {
  propertyId: string;
  listingId: string;
}) => {
  return (
    <DeleteButton
      alert="Property deleted from listing"
      deleteMethod={async () =>
        ListingService.listingDelPropertyFromListing({
          uuid: listingId,
          propertyId,
        })
      }
      invalidateKey={queryKeys.listing}
      id={propertyId}
    />
  );
};

export default ActionButton;
