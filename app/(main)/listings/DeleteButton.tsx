"use client";
import React from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import Image from "next/image";
import { toast } from "react-toastify";
import { parseErrorMessage } from "@/utils/errors";

type DeleteButtonProps = {
  deleteMethod?: (id: string) => Promise<unknown>;
  onClick?: () => void;
  id?: string;
  invalidateKey?: string;
  alert?: string;
};

const DeleteButton = (props: DeleteButtonProps) => {
  const queryClient = useQueryClient();
  const { deleteMethod, id, invalidateKey, alert, onClick } = props;
  const { mutate } = useMutation({
    mutationFn: deleteMethod,
    onSuccess: () => {
      toast.success(alert || "Deleted");
      queryClient.invalidateQueries({
        queryKey: [invalidateKey],
      });
    },
    onError(e: any) {
      toast.error("Error: unable to delete");
    },
  });

  return (
    <button
      className="transition-all hover:scale-110 active:scale-90"
      onClick={() => {
        if (onClick) {
          onClick();
          return;
        }
        if (id) mutate(id);
      }}
    >
      <Image src="/icons/delete.svg" alt="delete_icon" width={32} height={32} />
    </button>
  );
};

export default DeleteButton;
