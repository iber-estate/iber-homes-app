import React, { useState } from "react";
import { Icons } from "@/components/Icons";
import { Typography } from "@/components/Typography";
import { isDataView } from "node:util/types";

type CollapseTitleProps = {
  title: string;
  description: string;
  children?: React.ReactNode;
};

const CollapseTitle = (props: CollapseTitleProps) => {
  const [open, setOpen] = useState(false);
  const { title, description, children } = props;
  return (
    <div className="flex w-full flex-wrap justify-between">
      <button
        type="button"
        onClick={() => {
          setOpen((prevState) => !prevState);
        }}
      >
        <Typography
          type="subtitle2"
          className="mb-[4px] flex items-center text-[20px] sm:hidden"
        >
          <span className="mr-[4px]">{title}</span>
          <Icons.ArrowIcon
            className={open ? "-rotate-90" : "rotate-90"}
            color="#104551"
          />
        </Typography>
        <Typography
          type="subtitle"
          className="mb-[4px] hidden items-center text-[20px] sm:flex"
        >
          <span className="mr-[4px]">{title}</span>
          <Icons.ArrowIcon
            className={open ? "-rotate-90" : "rotate-90"}
            color="#104551"
          />
        </Typography>
      </button>
      <div> {children}</div>

      {open && (
        <Typography type="caption" className="w-full text-left">
          {description || "No description"}
        </Typography>
      )}
    </div>
  );
};

export default CollapseTitle;
