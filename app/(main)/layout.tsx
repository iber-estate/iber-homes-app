import "../globals.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "react-toastify/dist/ReactToastify.css";
import { Header } from "@/components/Header";
import { satoshi, onest } from "@/fonts";
import "mapbox-gl/dist/mapbox-gl.css";
import "yet-another-react-lightbox/styles.css";
import { Footer } from "@/components/Footer";
import { OpenAPI, UserService } from "@/generated";
import ContextsProviders from "@/components/ContextsProviders";
import { headers } from "next/headers";
import { auth } from "@/auth";
import TokenSetup from "@/components/TokenSetup";
import NextTopLoader from "nextjs-toploader";
import Script from "next/script";

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const headersList = headers();
  const host = headersList.get("host");
  const baseUrl =
    host === "localhost:3000"
      ? "https://app-dev.iber.homes"
      : `https://${host}`;
  OpenAPI.BASE = `${baseUrl}/api/v1`;
  const session = await auth();
  // @ts-ignore
  OpenAPI.TOKEN = session?.user?.token;
  return (
    <ContextsProviders>
      <html lang="en">
        <head>
          <Script
            async
            id="gtaginit"
            src="https://www.googletagmanager.com/gtag/js?id=G-ZPC2MRJKEN"
          ></Script>
          <Script
            id="gtagscript"
            dangerouslySetInnerHTML={{
              __html: `window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-ZPC2MRJKEN');`,
            }}
          ></Script>
          <meta
            name="google-site-verification"
            content="VQVd0dNe8KZXhlRezVfllV_D832V3wfQsWdCAMs2fso"
          />
        </head>
        <body className={`${onest.className} ${satoshi.variable}`}>
          <TokenSetup>
            <NextTopLoader color="#559D88" />
            <Header />
            <div className="grow">{children}</div>
            <Footer />
          </TokenSetup>
          <Script src="../smartlook.ts" />
        </body>
      </html>
    </ContextsProviders>
  );
}
