import React from "react";
import Hero from "@/components/Hero";
import { Typography } from "@/components/Typography";
import Link from "next/link";
import Image from "next/image";
import { Listing } from "@/components/Listing";

const Section = ({ children }: { children: React.ReactNode }) => {
  return <section className="sm:mb-[4 0px] mb-[80px]">{children}</section>;
};

const CountryCard = ({
  name,
  image,
  href,
  properties,
}: {
  name: string;
  image: string;
  href: string;
  properties: number;
}) => {
  return (
    <Link
      href={href}
      className="overflow-hidden rounded-[8px] bg-cover bg-center bg-no-repeat sm:min-w-[140px]"
      style={{
        backgroundImage: `url(${image})`,
      }}
    >
      <div className="h-full w-full bg-[rgba(0,0,0,0.2)] sm:w-[140px]">
        <div className="sm px-[20px] pb-[20px] pt-[124px] sm:pb-[72px] sm:pt-[12px]">
          <Typography
            type="h3"
            className="mb-0 text-white sm:mb-0 sm:text-[24px] sm:font-[600] sm:leading-[28px]"
          >
            {name}
          </Typography>
        </div>
      </div>
    </Link>
  );
};

const MainPage = () => {
  return (
    <div>
      <Section>
        <Hero />
      </Section>
      <div className="wrapper sm:-mt-[26px]">
        <Section>
          <Typography type="h2" className="text-primary-dark">
            Explore locations
          </Typography>
          <div className="grid w-full grid-cols-3 gap-[28px] sm:flex sm:w-[calc(100%+24px)] sm:gap-[16px] sm:overflow-scroll sm:pr-[16px]">
            <CountryCard
              name="Bali"
              href="/search?country=5"
              image="images/bali.jpeg"
              properties={120}
            />
            <CountryCard
              name="Thailand"
              href="/search?country=2"
              image="images/thai.jpeg"
              properties={86}
            />
            <CountryCard
              name="Turkey"
              href="/search?country=1"
              image="images/turkey.jpeg"
              properties={145}
            />
          </div>
        </Section>
        <Section>
          <Listing
            searchParams={{ ordering: "-created_at", pageSize: 5 }}
            title="Recently added"
          />
        </Section>
        <Section>
          <Listing
            searchParams={{
              ordering: "-created_at",
              pageSize: 5,
              country: ["5"],
            }}
            title="Water view properties"
          />
        </Section>
      </div>
      <div className="w-full bg-lightblueone py-[48px]">
        <div className="wrapper default-grid sm:flex sm:flex-col sm:gap-[12px] sm:px-[20px] sm:py-[32px]">
          <Image
            src="/images/footer.jpeg"
            alt=""
            className="col-span-2 h-[192px] w-full rounded-[8px] object-cover sm:h-[160px] sm:w-full"
            width={323}
            height={192}
          />
          <div className="col-span-4">
            <Typography type="body" className="mb-[12px]">
              <b>Iber</b> — Your International Partner in Real Estate! Iber
              offers a wide selection of properties from developers across
              various countries, including detailed descriptions of locations,
              neighborhood features, and infrastructure. We also feature diverse
              investment projects. Our tools for agents simplify the process of
              selecting, analyzing, and presenting properties to clients,
              enhancing the efficiency of your work.
              
            </Typography>
            <Typography type="body">
              Looking for experts in the local market? On our platform, you can
              find regional partners and independent experts who will assist you
              in unfamiliar real estate markets.Join Iber today and expand your
              business into new markets with our innovative solutions!
            </Typography>
          </div>


        </div>
      </div>
    </div>
  );
};

export default MainPage;
