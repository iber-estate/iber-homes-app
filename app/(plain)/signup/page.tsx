import { Auth } from "@/components/Auth";

const AuthPage = () => {
  return <Auth initialState="signup" />;
};

export default AuthPage;
