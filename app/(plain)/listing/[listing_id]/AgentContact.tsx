"use client";
import React, { useState } from "react";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import { UserFull } from "@/generated";

export type AgentContactProps = {
  user: UserFull;
};
export const AgentContact = (props: AgentContactProps) => {
  const { user } = props;
  const [isContactOpen, setIsContactOpen] = useState(false);
  const open = () => {
    setIsContactOpen(true);
  };
  return (
    <div className="flex items-center sm:w-full">
      <div className="flex items-center gap-[55px] rounded-[16px] bg-lightbluethree p-[24px] sm:w-full sm:flex-col sm:items-start sm:gap-[8px] sm:p-[20px]">
        <Typography type="subtitle3" className="m-0 max-w-[240px]">
          Agent name: <span className="font-[300]">{user?.name}</span>
        </Typography>
        {!isContactOpen && <Button onClick={open}>Get contact details</Button>}
        {isContactOpen && (
          <div className="rounded-[8px] bg-primary-light px-[24px] py-[12px]">
            <Typography type="body" className="text-buttonAquamarine">
              {user?.email}
            </Typography>
          </div>
        )}
      </div>
    </div>
  );
};
