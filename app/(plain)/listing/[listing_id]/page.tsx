import React from "react";
import { PropertyView } from "@/app/(main)/property/[property_id]/PropertyView";
import { ListingService, OpenAPI } from "@/generated";
import { auth } from "@/auth";
import { Typography } from "@/components/Typography";
import { AgentContact } from "@/app/(plain)/listing/[listing_id]/AgentContact";
import { marked } from "marked";

const Page = async ({ params }: { params: { listing_id: string } }) => {
  const session = await auth();
  // @ts-ignore
  OpenAPI.TOKEN = session?.user?.token;
  let listing;
  try {
    listing = await ListingService.listingRead({
      uuid: params.listing_id,
    });
  } catch (error) {
    console.log(error);
  }
  const descriptionHtmlString = marked(
    listing?.description || "No description",
    {
      breaks: true,
    },
  );
  const properties: any = listing?.properties;
  return (
    <div className="wrapper">
      <div className="mb-[60px] flex flex-col justify-between sm:mb-[40px] sm:flex-wrap">
        <div className="my-[48px] mb-[0] sm:my-[16px]">
          <Typography
            type="h2"
            className="text-primary-dark mb-[12px] sm:mb-[8px] sm:text-[28px] sm:leading-[32px]"
          >
            {listing?.name}
          </Typography>
          <Typography
            type="h6"
            className="text-primary-dark sm:m-0 sm:text-[16px] sm:leading-[20px]"
          >
            {listing?.properties.length} properties
          </Typography>
        </div>
        <Typography type="body" className="mb-[24px] max-w-[670px]">
          <div
            className="reset mb-[24px] max-w-[670px]"
            dangerouslySetInnerHTML={{ __html: descriptionHtmlString }}
          ></div>
        </Typography>
        <AgentContact user={listing?.user || {}} />
      </div>
      {properties?.map((property: any, index: number) => (
        <PropertyView
          forPreview
          key={index}
          property={property}
          property_id={property.id}
        />
      ))}
      <div className="mb-[100px] flex justify-center">
        <AgentContact user={listing?.user || {}} />
      </div>
    </div>
  );
};

export default Page;
