import React from "react";
import { PropertyView } from "@/app/(main)/property/[property_id]/PropertyView";
import { ListingService, RealPropertyList } from "@/generated";
import { useQuery } from "@tanstack/react-query";
import { queryKeys } from "@/types";
import { useParams } from "next/navigation";

type ListingPreviewViewProps = {
  properties?: RealPropertyList[];
};

const ListingPreviewView = (props: ListingPreviewViewProps) => {
  // const { listing_id: id } = useParams();
  // const { data: listing } = useQuery({
  //   queryKey: [queryKeys.listing],
  //   queryFn: async () => {
  //     if (typeof id !== "string") return;
  //     return ListingService.listingRead({ id: parseInt(id) });
  //   },
  // });

  return (
    <div>
      {/*{listing?.properties.map((property: any) => {*/}
      {/*  return <PropertyView property={property} />;*/}
      {/*})}*/}
    </div>
  );
};

export default ListingPreviewView;
