import { Auth } from "@/components/Auth";

const AuthPage = () => {
  return <Auth initialState="signin" />;
};

export default AuthPage;
