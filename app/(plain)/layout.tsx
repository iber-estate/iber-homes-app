import "../globals.css";
import "react-toastify/dist/ReactToastify.css";
import React from "react";
import ContextsProviders from "@/components/ContextsProviders";
import { onest, satoshi } from "@/fonts";
import { auth } from "@/auth";
import { redirect } from "next/navigation";
import TokenSetup from "@/components/TokenSetup";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "mapbox-gl/dist/mapbox-gl.css";
import "yet-another-react-lightbox/styles.css";

const Layout = async ({ children }: any) => {
  return (
    <ContextsProviders>
      <html lang="en">
        <body className={`${onest.className} ${satoshi.variable}`}>
          <TokenSetup>{children}</TokenSetup>
        </body>
      </html>
    </ContextsProviders>
  );
};

export default Layout;
