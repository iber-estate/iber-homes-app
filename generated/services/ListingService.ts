/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Listing } from "../models/Listing";
import type { ListingList } from "../models/ListingList";
import type { PropertyToListing } from "../models/PropertyToListing";
import type { CancelablePromise } from "../core/CancelablePromise";
import { OpenAPI } from "../core/OpenAPI";
import { request as __request } from "../core/request";
export class ListingService {
  /**
   * List all listings.
   * @returns any
   * @throws ApiError
   */
  public static listingList({
    name,
    ordering,
    page,
    pageSize,
  }: {
    /**
     * name
     */
    name?: string;
    /**
     * Which field to use when ordering the results.
     */
    ordering?: string;
    /**
     * A page number within the paginated result set.
     */
    page?: number;
    /**
     * Number of results to return per page.
     */
    pageSize?: number;
  }): CancelablePromise<{
    count: number;
    next?: string | null;
    previous?: string | null;
    results: Array<ListingList>;
  }> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/listing/",
      query: {
        name: name,
        ordering: ordering,
        page: page,
        page_size: pageSize,
      },
    });
  }
  /**
   * Create new listing.
   * @returns Listing
   * @throws ApiError
   */
  public static listingCreate({
    data,
  }: {
    data: Listing;
  }): CancelablePromise<Listing> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/listing/",
      body: data,
    });
  }
  /**
   * Return the given listing.
   * @returns ListingList
   * @throws ApiError
   */
  public static listingRead({
    uuid,
  }: {
    uuid: string;
  }): CancelablePromise<ListingList> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/listing/{uuid}/",
      path: {
        uuid: uuid,
      },
    });
  }
  /**
   * Update listing data.
   * @returns Listing
   * @throws ApiError
   */
  public static listingUpdate({
    uuid,
    data,
  }: {
    uuid: string;
    data: Listing;
  }): CancelablePromise<Listing> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/listing/{uuid}/",
      path: {
        uuid: uuid,
      },
      body: data,
    });
  }
  /**
   * Partial update listing data.
   * @returns Listing
   * @throws ApiError
   */
  public static listingPartialUpdate({
    uuid,
    data,
  }: {
    uuid: string;
    data: Listing;
  }): CancelablePromise<Listing> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/listing/{uuid}/",
      path: {
        uuid: uuid,
      },
      body: data,
    });
  }
  /**
   * Mark listing as deleted.
   * @returns void
   * @throws ApiError
   */
  public static listingDelete({
    uuid,
  }: {
    uuid: string;
  }): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/listing/{uuid}/",
      path: {
        uuid: uuid,
      },
    });
  }
  /**
   * View for working with listing data.
   * @returns PropertyToListing
   * @throws ApiError
   */
  public static listingAddRealPropertyToListing({
    uuid,
    data,
  }: {
    uuid: string;
    data: PropertyToListing;
  }): CancelablePromise<PropertyToListing> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/listing/{uuid}/property/",
      path: {
        uuid: uuid,
      },
      body: data,
    });
  }
  /**
   * View for working with listing data.
   * @returns void
   * @throws ApiError
   */
  public static listingDelPropertyFromListing({
    uuid,
    propertyId,
  }: {
    uuid: string;
    propertyId: string;
    /**
     * property id
     */
  }): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/listing/{uuid}/property/{property_id}/",
      path: {
        uuid: uuid,
        property_id: propertyId,
      },
    });
  }
}
