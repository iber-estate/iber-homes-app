/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { District } from '../models/District';
import type { DistrictList } from '../models/DistrictList';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class DistrictService {
    /**
     * List all districts.
     * @returns any
     * @throws ApiError
     */
    public static districtList({
        name,
        city,
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Filter by name. For example: name=d1
         */
        name?: string,
        /**
         * city
         */
        city?: string,
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<DistrictList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/district/',
            query: {
                'name': name,
                'city': city,
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Create new district.
     * @returns District
     * @throws ApiError
     */
    public static districtCreate({
        data,
    }: {
        data: District,
    }): CancelablePromise<District> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/district/',
            body: data,
        });
    }
    /**
     * Return the given district.
     * @returns District
     * @throws ApiError
     */
    public static districtRead({
        id,
    }: {
        /**
         * A unique integer value identifying this district.
         */
        id: number,
    }): CancelablePromise<District> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/district/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Update district data.
     * @returns District
     * @throws ApiError
     */
    public static districtUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this district.
         */
        id: number,
        data: District,
    }): CancelablePromise<District> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/district/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Partial update district data.
     * @returns District
     * @throws ApiError
     */
    public static districtPartialUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this district.
         */
        id: number,
        data: District,
    }): CancelablePromise<District> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/district/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Mark district as deleted.
     * @returns void
     * @throws ApiError
     */
    public static districtDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this district.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/district/{id}/',
            path: {
                'id': id,
            },
        });
    }
}
