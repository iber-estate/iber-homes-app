/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { DocumentLink } from '../models/DocumentLink';
import type { PropertyComplexAmenitiesList } from '../models/PropertyComplexAmenitiesList';
import type { PropertyComplexCreateUpdate } from '../models/PropertyComplexCreateUpdate';
import type { PropertyComplexDetail } from '../models/PropertyComplexDetail';
import type { PropertyComplexList } from '../models/PropertyComplexList';
import type { PropertyComplexPhoto } from '../models/PropertyComplexPhoto';
import type { PropertyComplexPhotoOrder } from '../models/PropertyComplexPhotoOrder';
import type { PropertyComplexVideoLink } from '../models/PropertyComplexVideoLink';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class ComplexService {
    /**
     * List all property complexes.
     * @returns any
     * @throws ApiError
     */
    public static complexList({
        name,
        developer,
        ordering,
        page,
        pageSize,
    }: {
        /**
         * name
         */
        name?: string,
        /**
         * developer
         */
        developer?: string,
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<PropertyComplexList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/complex/',
            query: {
                'name': name,
                'developer': developer,
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Create new property complex.
     * @returns PropertyComplexCreateUpdate
     * @throws ApiError
     */
    public static complexCreate({
        data,
    }: {
        data: PropertyComplexCreateUpdate,
    }): CancelablePromise<PropertyComplexCreateUpdate> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/complex/',
            body: data,
        });
    }
    /**
     * List all amenities.
     * @returns any
     * @throws ApiError
     */
    public static complexAmenities({
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<PropertyComplexAmenitiesList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/complex/amenities/',
            query: {
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * List all complex types.
     * @returns any
     * @throws ApiError
     */
    public static complexComplexTypes({
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<PropertyComplexList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/complex/complex_types/',
            query: {
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * View for working with property complex data.
     * @returns void
     * @throws ApiError
     */
    public static complexDelDocument({
        id,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/complex/document/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * View for working with property complex data.
     * @returns void
     * @throws ApiError
     */
    public static complexDelPhoto({
        id,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/complex/photo/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * View for working with property complex data.
     * @returns void
     * @throws ApiError
     */
    public static complexDelVideo({
        id,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/complex/video/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Return the given property complex.
     * @returns PropertyComplexDetail
     * @throws ApiError
     */
    public static complexRead({
        id,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
    }): CancelablePromise<PropertyComplexDetail> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/complex/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Update property complex data.
     * @returns PropertyComplexCreateUpdate
     * @throws ApiError
     */
    public static complexUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
        data: PropertyComplexCreateUpdate,
    }): CancelablePromise<PropertyComplexCreateUpdate> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/complex/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Partial update property complex data.
     * @returns PropertyComplexCreateUpdate
     * @throws ApiError
     */
    public static complexPartialUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
        data: PropertyComplexCreateUpdate,
    }): CancelablePromise<PropertyComplexCreateUpdate> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/complex/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Mark property complex as deleted.
     * @returns void
     * @throws ApiError
     */
    public static complexDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/complex/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * View for working with property complex data.
     * @returns DocumentLink
     * @throws ApiError
     */
    public static complexAddDocument({
        id,
        documentLink,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
        documentLink?: string,
    }): CancelablePromise<DocumentLink> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/complex/{id}/document/',
            path: {
                'id': id,
            },
            formData: {
                'document_link': documentLink,
            },
        });
    }
    /**
     * View for working with property complex data.
     * @returns PropertyComplexPhoto
     * @throws ApiError
     */
    public static complexAddPhoto({
        id,
        photo,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
        photo: Blob,
    }): CancelablePromise<PropertyComplexPhoto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/complex/{id}/photo/',
            path: {
                'id': id,
            },
            formData: {
                'photo': photo,
            },
        });
    }
    /**
     * View for working with property complex data.
     * @returns PropertyComplexPhotoOrder
     * @throws ApiError
     */
    public static complexPhotoOrder({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
        data: PropertyComplexPhotoOrder,
    }): CancelablePromise<PropertyComplexPhotoOrder> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/complex/{id}/photo_order/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * View for working with property complex data.
     * @returns PropertyComplexVideoLink
     * @throws ApiError
     */
    public static complexAddVideo({
        id,
        videoLink,
    }: {
        /**
         * A unique integer value identifying this property complex.
         */
        id: number,
        videoLink?: string,
    }): CancelablePromise<PropertyComplexVideoLink> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/complex/{id}/video/',
            path: {
                'id': id,
            },
            formData: {
                'video_link': videoLink,
            },
        });
    }
}
