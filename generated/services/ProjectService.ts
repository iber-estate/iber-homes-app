/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { InvestDocumentLink } from '../models/InvestDocumentLink';
import type { InvestProjectCreateUpdate } from '../models/InvestProjectCreateUpdate';
import type { InvestProjectList } from '../models/InvestProjectList';
import type { InvestProjectPhoto } from '../models/InvestProjectPhoto';
import type { InvestProjectPhotoOrder } from '../models/InvestProjectPhotoOrder';
import type { InvestProjectPresentationLink } from '../models/InvestProjectPresentationLink';
import type { InvestProjectVideoLink } from '../models/InvestProjectVideoLink';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class ProjectService {
    /**
     * List all invest projects.
     * @returns any
     * @throws ApiError
     */
    public static projectList({
        address,
        country,
        investAmountMin,
        investAmountMax,
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Filter by address line 1. For example: address=Indonesia
         */
        address?: string,
        /**
         * Filter by country id. You can pass multiple values separated by commas. For example: country=1,2
         */
        country?: string,
        /**
         * Filter by min invest_amount.
         */
        investAmountMin?: string,
        /**
         * Filter by max invest_amount.
         */
        investAmountMax?: string,
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<InvestProjectList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/project/',
            query: {
                'address': address,
                'country': country,
                'invest_amount_min': investAmountMin,
                'invest_amount_max': investAmountMax,
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Create new invest project.
     * @returns InvestProjectCreateUpdate
     * @throws ApiError
     */
    public static projectCreate({
        data,
    }: {
        data: InvestProjectCreateUpdate,
    }): CancelablePromise<InvestProjectCreateUpdate> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/project/',
            body: data,
        });
    }
    /**
     * View for working with country data.
     * @returns void
     * @throws ApiError
     */
    public static projectDelDocument({
        id,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/project/document/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * View for working with country data.
     * @returns void
     * @throws ApiError
     */
    public static projectDelPhoto({
        id,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/project/photo/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * View for working with country data.
     * @returns void
     * @throws ApiError
     */
    public static projectDelPresentation({
        id,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/project/presentation/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * View for working with country data.
     * @returns void
     * @throws ApiError
     */
    public static projectDelVideo({
        id,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/project/video/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Return the given invest project.
     * @returns InvestProjectList
     * @throws ApiError
     */
    public static projectRead({
        id,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
    }): CancelablePromise<InvestProjectList> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/project/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Update invest project data.
     * @returns InvestProjectCreateUpdate
     * @throws ApiError
     */
    public static projectUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
        data: InvestProjectCreateUpdate,
    }): CancelablePromise<InvestProjectCreateUpdate> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/project/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Partial update invest project data.
     * @returns InvestProjectList
     * @throws ApiError
     */
    public static projectPartialUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
        data: InvestProjectList,
    }): CancelablePromise<InvestProjectList> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/project/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Mark invest project as deleted.
     * @returns void
     * @throws ApiError
     */
    public static projectDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/project/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * View for working with country data.
     * @returns InvestDocumentLink
     * @throws ApiError
     */
    public static projectAddDocument({
        id,
        documentLink,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
        documentLink?: string,
    }): CancelablePromise<InvestDocumentLink> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/project/{id}/document/',
            path: {
                'id': id,
            },
            formData: {
                'document_link': documentLink,
            },
        });
    }
    /**
     * View for working with country data.
     * @returns InvestProjectPhoto
     * @throws ApiError
     */
    public static projectAddPhoto({
        id,
        photo,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
        photo: Blob,
    }): CancelablePromise<InvestProjectPhoto> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/project/{id}/photo/',
            path: {
                'id': id,
            },
            formData: {
                'photo': photo,
            },
        });
    }
    /**
     * View for working with country data.
     * @returns InvestProjectPhotoOrder
     * @throws ApiError
     */
    public static projectPhotoOrder({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
        data: InvestProjectPhotoOrder,
    }): CancelablePromise<InvestProjectPhotoOrder> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/project/{id}/photo_order/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * View for working with country data.
     * @returns InvestProjectPresentationLink
     * @throws ApiError
     */
    public static projectAddPresentation({
        id,
        presentationLink,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
        presentationLink?: string,
    }): CancelablePromise<InvestProjectPresentationLink> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/project/{id}/presentation/',
            path: {
                'id': id,
            },
            formData: {
                'presentation_link': presentationLink,
            },
        });
    }
    /**
     * View for working with country data.
     * @returns InvestProjectVideoLink
     * @throws ApiError
     */
    public static projectAddVideo({
        id,
        videoLink,
    }: {
        /**
         * A unique integer value identifying this invest project.
         */
        id: number,
        videoLink?: string,
    }): CancelablePromise<InvestProjectVideoLink> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/project/{id}/video/',
            path: {
                'id': id,
            },
            formData: {
                'video_link': videoLink,
            },
        });
    }
}
