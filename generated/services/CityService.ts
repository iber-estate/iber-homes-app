/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { City } from '../models/City';
import type { CityList } from '../models/CityList';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class CityService {
    /**
     * List all cities.
     * @returns any
     * @throws ApiError
     */
    public static cityList({
        name,
        country,
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Filter by name. For example: name=Denpasar
         */
        name?: string,
        /**
         * country
         */
        country?: string,
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<CityList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/city/',
            query: {
                'name': name,
                'country': country,
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Create new city.
     * @returns City
     * @throws ApiError
     */
    public static cityCreate({
        data,
    }: {
        data: City,
    }): CancelablePromise<City> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/city/',
            body: data,
        });
    }
    /**
     * Return the given city.
     * @returns City
     * @throws ApiError
     */
    public static cityRead({
        id,
    }: {
        /**
         * A unique integer value identifying this city.
         */
        id: number,
    }): CancelablePromise<City> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/city/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Update city data.
     * @returns City
     * @throws ApiError
     */
    public static cityUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this city.
         */
        id: number,
        data: City,
    }): CancelablePromise<City> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/city/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Partial update city data.
     * @returns City
     * @throws ApiError
     */
    public static cityPartialUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this city.
         */
        id: number,
        data: City,
    }): CancelablePromise<City> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/city/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Mark user as deleted.
     * @returns void
     * @throws ApiError
     */
    public static cityDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this city.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/city/{id}/',
            path: {
                'id': id,
            },
        });
    }
}
