/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Document } from '../models/Document';
import type { OwnershipTypesList } from '../models/OwnershipTypesList';
import type { Photo } from '../models/Photo';
import type { PhotoOrder } from '../models/PhotoOrder';
import type { RealPropertyAmenitiesList } from '../models/RealPropertyAmenitiesList';
import type { RealPropertyCreateUpdate } from '../models/RealPropertyCreateUpdate';
import type { RealPropertyList } from '../models/RealPropertyList';
import type { SavedPropertyCrate } from '../models/SavedPropertyCrate';
import type { TagsList } from '../models/TagsList';
import type { Video } from '../models/Video';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class PropertyService {
    /**
     * List all real property.
     * @returns any
     * @throws ApiError
     */
    public static propertyList({
        priceMin,
        address,
        priceMax,
        bedrooms,
        bathrooms,
        country,
        propertyType,
        isSaved,
        status,
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Filter by min price.
         */
        priceMin?: string,
        /**
         * Filter by line1.
         */
        address?: string,
        /**
         * Filter by max price.
         */
        priceMax?: string,
        /**
         * Filter by bedrooms number. You can pass multiple values separated by commas. For example: bedrooms=1,2
         */
        bedrooms?: string,
        /**
         * Filter by bathrooms number. You can pass multiple values separated by commas. For example: bathrooms=1,2
         */
        bathrooms?: string,
        /**
         * Filter by country id. You can pass multiple values separated by commas. For example: country=1,2
         */
        country?: string,
        /**
         * Filter by property type. You can pass multiple values separated by commas. For example: property_type=villa,apartment
         */
        propertyType?: string,
        isSaved?: boolean,
        /**
         * status
         */
        status?: 'draft' | 'published' | 'archived',
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<RealPropertyList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/property/',
            query: {
                'price_min': priceMin,
                'address': address,
                'price_max': priceMax,
                'bedrooms': bedrooms,
                'bathrooms': bathrooms,
                'country': country,
                'property_type': propertyType,
                'is_saved': isSaved,
                'status': status,
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Create new real property.
     * @returns RealPropertyCreateUpdate
     * @throws ApiError
     */
    public static propertyCreate({
        data,
    }: {
        data: RealPropertyCreateUpdate,
    }): CancelablePromise<RealPropertyCreateUpdate> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/property/',
            body: data,
        });
    }
    /**
     * List all amenities.
     * @returns any
     * @throws ApiError
     */
    public static propertyAmenities({
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<RealPropertyAmenitiesList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/property/amenities/',
            query: {
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Mark real property document as deleted.
     * @returns void
     * @throws ApiError
     */
    public static propertyDelDocument({
        id,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/property/document/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * List all ownership types.
     * @returns any
     * @throws ApiError
     */
    public static propertyOwnershipTypes({
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<OwnershipTypesList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/property/ownership_types/',
            query: {
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Mark real property photo as deleted.
     * @returns void
     * @throws ApiError
     */
    public static propertyDelPhoto({
        id,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/property/photo/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * List all property types.
     * @returns any
     * @throws ApiError
     */
    public static propertyPropertyTypes({
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<RealPropertyList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/property/property_types/',
            query: {
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * List all tags.
     * @returns any
     * @throws ApiError
     */
    public static propertyTags({
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<TagsList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/property/tags/',
            query: {
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Mark real property video as deleted.
     * @returns void
     * @throws ApiError
     */
    public static propertyDelVideo({
        id,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/property/video/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Return the given real property.
     * @returns RealPropertyList
     * @throws ApiError
     */
    public static propertyRead({
        id,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
    }): CancelablePromise<RealPropertyList> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/property/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Update real property data.
     * @returns RealPropertyCreateUpdate
     * @throws ApiError
     */
    public static propertyUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
        data: RealPropertyCreateUpdate,
    }): CancelablePromise<RealPropertyCreateUpdate> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/property/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Partial update real property data.
     * @returns RealPropertyCreateUpdate
     * @throws ApiError
     */
    public static propertyPartialUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
        data: RealPropertyCreateUpdate,
    }): CancelablePromise<RealPropertyCreateUpdate> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/property/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Mark real property as deleted.
     * @returns void
     * @throws ApiError
     */
    public static propertyDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/property/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Add document to real property.
     * @returns Document
     * @throws ApiError
     */
    public static propertyAddDocument({
        id,
        description,
        externalUrl,
        file,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
        description: string,
        externalUrl?: string,
        file?: Blob | null,
    }): CancelablePromise<Document> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/property/{id}/document/',
            path: {
                'id': id,
            },
            formData: {
                'external_url': externalUrl,
                'description': description,
                'file': file,
            },
        });
    }
    /**
     * Add photo to real property.
     * @returns Photo
     * @throws ApiError
     */
    public static propertyAddPhoto({
        id,
        photo,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
        photo: Blob,
    }): CancelablePromise<Photo> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/property/{id}/photo/',
            path: {
                'id': id,
            },
            formData: {
                'photo': photo,
            },
        });
    }
    /**
     * Update photo order.
     * @returns PhotoOrder
     * @throws ApiError
     */
    public static propertyPhotoOrder({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
        data: PhotoOrder,
    }): CancelablePromise<PhotoOrder> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/property/{id}/photo_order/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * View for working with real property data.
     * @returns SavedPropertyCrate
     * @throws ApiError
     */
    public static propertySaveProperty({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
        data: SavedPropertyCrate,
    }): CancelablePromise<SavedPropertyCrate> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/property/{id}/save/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * View for working with real property data.
     * @returns void
     * @throws ApiError
     */
    public static propertyDelSavedProperty({
        id,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/property/{id}/saved/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Add video to real property.
     * @returns Video
     * @throws ApiError
     */
    public static propertyAddVideo({
        id,
        externalUrl,
        video,
    }: {
        /**
         * A unique integer value identifying this real property.
         */
        id: number,
        externalUrl?: string,
        video?: Blob | null,
    }): CancelablePromise<Video> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/property/{id}/video/',
            path: {
                'id': id,
            },
            formData: {
                'external_url': externalUrl,
                'video': video,
            },
        });
    }
}
