/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Agency } from '../models/Agency';
import type { AgencyList } from '../models/AgencyList';
import type { UserFull } from '../models/UserFull';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class AgencyService {
    /**
     * List all agencies.
     * @returns any
     * @throws ApiError
     */
    public static agencyList({
        name,
        ordering,
        page,
        pageSize,
    }: {
        /**
         * name
         */
        name?: string,
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<AgencyList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/agency/',
            query: {
                'name': name,
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Create new agency.
     * @returns Agency
     * @throws ApiError
     */
    public static agencyCreate({
        data,
    }: {
        data: Agency,
    }): CancelablePromise<Agency> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/agency/',
            body: data,
        });
    }
    /**
     * Return the given agency.
     * @returns Agency
     * @throws ApiError
     */
    public static agencyRead({
        id,
    }: {
        /**
         * A unique integer value identifying this agency.
         */
        id: number,
    }): CancelablePromise<Agency> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/agency/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Update agency data.
     * @returns Agency
     * @throws ApiError
     */
    public static agencyUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this agency.
         */
        id: number,
        data: Agency,
    }): CancelablePromise<Agency> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/agency/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Partial update agency data.
     * @returns Agency
     * @throws ApiError
     */
    public static agencyPartialUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this agency.
         */
        id: number,
        data: Agency,
    }): CancelablePromise<Agency> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/agency/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Mark agency as deleted.
     * @returns void
     * @throws ApiError
     */
    public static agencyDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this agency.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/agency/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * View for working with agency data.
     * @returns UserFull
     * @throws ApiError
     */
    public static agencyEmployees({
        id,
    }: {
        /**
         * A unique integer value identifying this agency.
         */
        id: number,
    }): CancelablePromise<Array<UserFull>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/agency/{id}/employees/',
            path: {
                'id': id,
            },
        });
    }
}
