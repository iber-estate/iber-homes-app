/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { SavedPropertyCrate } from '../models/SavedPropertyCrate';
import type { SavedPropertyList } from '../models/SavedPropertyList';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class SavedService {
    /**
     * @returns any
     * @throws ApiError
     */
    public static savedList({
        page,
        pageSize,
    }: {
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<SavedPropertyList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/saved/',
            query: {
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * @returns SavedPropertyCrate
     * @throws ApiError
     */
    public static savedCreate({
        data,
    }: {
        data: SavedPropertyCrate,
    }): CancelablePromise<SavedPropertyCrate> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/saved/',
            body: data,
        });
    }
    /**
     * @returns SavedPropertyList
     * @throws ApiError
     */
    public static savedRead({
        id,
    }: {
        /**
         * A unique integer value identifying this saved property.
         */
        id: number,
    }): CancelablePromise<SavedPropertyList> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/saved/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * @returns void
     * @throws ApiError
     */
    public static savedDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this saved property.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/saved/{id}/',
            path: {
                'id': id,
            },
        });
    }
}
