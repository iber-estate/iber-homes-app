/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Suggest } from '../models/Suggest';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class SuggestService {
    /**
     * @returns Suggest
     * @throws ApiError
     */
    public static suggestList({
        input,
    }: {
        input?: string,
    }): CancelablePromise<Suggest> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/suggest/',
            query: {
                'input': input,
            },
        });
    }
}
