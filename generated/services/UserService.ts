/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { OneTimePasswordCreate } from '../models/OneTimePasswordCreate';
import type { TokenRefresh } from '../models/TokenRefresh';
import type { UserAccessToken } from '../models/UserAccessToken';
import type { UserCreate } from '../models/UserCreate';
import type { UserFull } from '../models/UserFull';
import type { UserLogin } from '../models/UserLogin';
import type { UserUpdate } from '../models/UserUpdate';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class UserService {
    /**
     * List all users.
     * @returns any
     * @throws ApiError
     */
    public static userList({
        name,
        isActive,
        role,
        ordering,
        page,
        pageSize,
    }: {
        /**
         * name
         */
        name?: string,
        /**
         * is_active
         */
        isActive?: string,
        /**
         * role
         */
        role?: 'administrator' | 'realtor' | 'developer' | 'freelancer' | 'customer',
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<UserFull>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/user/',
            query: {
                'name': name,
                'is_active': isActive,
                'role': role,
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * View for working with user data.
     * @returns UserCreate
     * @throws ApiError
     */
    public static userCreate({
        data,
    }: {
        data: UserCreate,
    }): CancelablePromise<UserCreate> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/user/',
            body: data,
        });
    }
    /**
     * Retrieve auth token by pair of username & password.
     * @returns UserAccessToken
     * @throws ApiError
     */
    public static userLogin({
        data,
    }: {
        data: UserLogin,
    }): CancelablePromise<UserAccessToken> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/user/login/',
            body: data,
        });
    }
    /**
     * Current use data.
     * @returns any
     * @throws ApiError
     */
    public static userMy({
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<UserFull>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/user/my/',
            query: {
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Takes a refresh type JSON web token and returns an access type JSON web
     * token if the refresh token is valid.
     * @returns TokenRefresh
     * @throws ApiError
     */
    public static userRefreshCreate({
        data,
    }: {
        data: TokenRefresh,
    }): CancelablePromise<TokenRefresh> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/user/refresh/',
            body: data,
        });
    }
    /**
     * View for working with user data.
     * @returns any
     * @throws ApiError
     */
    public static userSendOtp({
        data,
    }: {
        data: OneTimePasswordCreate,
    }): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/user/send_otp/',
            body: data,
        });
    }
    /**
     * Return the given user.
     * @returns UserFull
     * @throws ApiError
     */
    public static userRead({
        id,
    }: {
        /**
         * A unique integer value identifying this user.
         */
        id: number,
    }): CancelablePromise<UserFull> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/user/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Update user data.
     * @returns UserUpdate
     * @throws ApiError
     */
    public static userUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this user.
         */
        id: number,
        data: UserUpdate,
    }): CancelablePromise<UserUpdate> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/user/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Partial update user data.
     * @returns UserUpdate
     * @throws ApiError
     */
    public static userPartialUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this user.
         */
        id: number,
        data: UserUpdate,
    }): CancelablePromise<UserUpdate> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/user/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Mark user as deleted.
     * @returns void
     * @throws ApiError
     */
    public static userDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this user.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/user/{id}/',
            path: {
                'id': id,
            },
        });
    }
}
