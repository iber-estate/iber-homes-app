/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { DeveloperAvatar } from '../models/DeveloperAvatar';
import type { DeveloperCreateUpdate } from '../models/DeveloperCreateUpdate';
import type { DeveloperList } from '../models/DeveloperList';
import type { UserFull } from '../models/UserFull';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class DeveloperService {
    /**
     * List all developers.
     * @returns any
     * @throws ApiError
     */
    public static developerList({
        name,
        address,
        ordering,
        page,
        pageSize,
    }: {
        /**
         * name
         */
        name?: string,
        /**
         * Filter by address line 1. For example: address=Indonesia
         */
        address?: string,
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<DeveloperList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/developer/',
            query: {
                'name': name,
                'address': address,
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Create new developer.
     * @returns DeveloperCreateUpdate
     * @throws ApiError
     */
    public static developerCreate({
        data,
    }: {
        data: DeveloperCreateUpdate,
    }): CancelablePromise<DeveloperCreateUpdate> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/developer/',
            body: data,
        });
    }
    /**
     * Return the given developer.
     * @returns DeveloperList
     * @throws ApiError
     */
    public static developerRead({
        id,
    }: {
        /**
         * A unique integer value identifying this developer.
         */
        id: number,
    }): CancelablePromise<DeveloperList> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/developer/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Update developer data.
     * @returns DeveloperCreateUpdate
     * @throws ApiError
     */
    public static developerUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this developer.
         */
        id: number,
        data: DeveloperCreateUpdate,
    }): CancelablePromise<DeveloperCreateUpdate> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/developer/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Partial update developer data.
     * @returns DeveloperCreateUpdate
     * @throws ApiError
     */
    public static developerPartialUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this developer.
         */
        id: number,
        data: DeveloperCreateUpdate,
    }): CancelablePromise<DeveloperCreateUpdate> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/developer/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Mark developer as deleted.
     * @returns void
     * @throws ApiError
     */
    public static developerDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this developer.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/developer/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Create new developer avatar.
     * @returns DeveloperAvatar
     * @throws ApiError
     */
    public static developerAvatar({
        id,
        avatar,
    }: {
        /**
         * A unique integer value identifying this developer.
         */
        id: number,
        avatar: Blob,
    }): CancelablePromise<DeveloperAvatar> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/developer/{id}/avatar/',
            path: {
                'id': id,
            },
            formData: {
                'avatar': avatar,
            },
        });
    }
    /**
     * View for working with developer data.
     * @returns UserFull
     * @throws ApiError
     */
    public static developerEmployees({
        id,
    }: {
        /**
         * A unique integer value identifying this developer.
         */
        id: number,
    }): CancelablePromise<Array<UserFull>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/developer/{id}/employees/',
            path: {
                'id': id,
            },
        });
    }
}
