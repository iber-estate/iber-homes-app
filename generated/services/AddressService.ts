/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AddressCreateUpdate } from '../models/AddressCreateUpdate';
import type { AddressList } from '../models/AddressList';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class AddressService {
    /**
     * List all addresses.
     * @returns any
     * @throws ApiError
     */
    public static addressList({
        district,
        postcode,
        line1,
        ordering,
        page,
        pageSize,
    }: {
        /**
         * district
         */
        district?: string,
        /**
         * postcode
         */
        postcode?: string,
        /**
         * Filter by line1.
         */
        line1?: string,
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<AddressList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/address/',
            query: {
                'district': district,
                'postcode': postcode,
                'line1': line1,
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Create new address.
     * @returns AddressCreateUpdate
     * @throws ApiError
     */
    public static addressCreate({
        data,
    }: {
        data: AddressCreateUpdate,
    }): CancelablePromise<AddressCreateUpdate> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/address/',
            body: data,
        });
    }
    /**
     * Return the given address.
     * @returns AddressList
     * @throws ApiError
     */
    public static addressRead({
        id,
    }: {
        /**
         * A unique integer value identifying this address.
         */
        id: number,
    }): CancelablePromise<AddressList> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/address/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Update address data.
     * @returns AddressCreateUpdate
     * @throws ApiError
     */
    public static addressUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this address.
         */
        id: number,
        data: AddressCreateUpdate,
    }): CancelablePromise<AddressCreateUpdate> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/address/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Partial update address data.
     * @returns AddressCreateUpdate
     * @throws ApiError
     */
    public static addressPartialUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this address.
         */
        id: number,
        data: AddressCreateUpdate,
    }): CancelablePromise<AddressCreateUpdate> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/address/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Mark address as deleted.
     * @returns void
     * @throws ApiError
     */
    public static addressDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this address.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/address/{id}/',
            path: {
                'id': id,
            },
        });
    }
}
