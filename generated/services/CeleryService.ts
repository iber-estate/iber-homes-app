/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BasicCeleryResult } from '../models/BasicCeleryResult';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class CeleryService {
    /**
     * View for celery result
     * @returns BasicCeleryResult
     * @throws ApiError
     */
    public static celeryResultRead({
        id,
    }: {
        id: string,
    }): CancelablePromise<BasicCeleryResult> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/celery/result/{id}/',
            path: {
                'id': id,
            },
        });
    }
}
