/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Country } from '../models/Country';
import type { CountryList } from '../models/CountryList';
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';
export class CountryService {
    /**
     * List all countries.
     * @returns any
     * @throws ApiError
     */
    public static countryList({
        name,
        ordering,
        page,
        pageSize,
    }: {
        /**
         * Filter by name. For example: name=Indonesia
         */
        name?: string,
        /**
         * Which field to use when ordering the results.
         */
        ordering?: string,
        /**
         * A page number within the paginated result set.
         */
        page?: number,
        /**
         * Number of results to return per page.
         */
        pageSize?: number,
    }): CancelablePromise<{
        count: number;
        next?: string | null;
        previous?: string | null;
        results: Array<CountryList>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/country/',
            query: {
                'name': name,
                'ordering': ordering,
                'page': page,
                'page_size': pageSize,
            },
        });
    }
    /**
     * Create new country.
     * @returns Country
     * @throws ApiError
     */
    public static countryCreate({
        data,
    }: {
        data: Country,
    }): CancelablePromise<Country> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/country/',
            body: data,
        });
    }
    /**
     * Return the given country.
     * @returns CountryList
     * @throws ApiError
     */
    public static countryRead({
        id,
    }: {
        /**
         * A unique integer value identifying this country.
         */
        id: number,
    }): CancelablePromise<CountryList> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/country/{id}/',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Update country data.
     * @returns Country
     * @throws ApiError
     */
    public static countryUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this country.
         */
        id: number,
        data: Country,
    }): CancelablePromise<Country> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/country/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Partial update country data.
     * @returns Country
     * @throws ApiError
     */
    public static countryPartialUpdate({
        id,
        data,
    }: {
        /**
         * A unique integer value identifying this country.
         */
        id: number,
        data: Country,
    }): CancelablePromise<Country> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/country/{id}/',
            path: {
                'id': id,
            },
            body: data,
        });
    }
    /**
     * Mark country as deleted.
     * @returns void
     * @throws ApiError
     */
    public static countryDelete({
        id,
    }: {
        /**
         * A unique integer value identifying this country.
         */
        id: number,
    }): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/country/{id}/',
            path: {
                'id': id,
            },
        });
    }
}
