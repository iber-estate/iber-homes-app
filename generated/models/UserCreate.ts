/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type UserCreate = {
    readonly id?: number;
    email: string;
    name: string;
    phone?: string | null;
    description?: string | null;
    wa_number?: string | null;
    role?: UserCreate.role;
    readonly date_joined?: string;
};
export namespace UserCreate {
    export enum role {
        ADMINISTRATOR = 'administrator',
        REALTOR = 'realtor',
        DEVELOPER = 'developer',
        FREELANCER = 'freelancer',
        CUSTOMER = 'customer',
    }
}

