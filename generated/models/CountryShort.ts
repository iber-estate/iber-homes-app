/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type CountryShort = {
    readonly id?: number;
    name: string;
    phone_code: string;
};

