/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type Photo = {
    readonly id?: number;
    readonly thumbnail?: string;
    readonly created_at?: string;
    readonly photo?: string;
    order: number;
    real_property: number;
};

