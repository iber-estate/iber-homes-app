/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AddressList } from './AddressList';
import type { DeveloperList } from './DeveloperList';
import type { DocumentLink } from './DocumentLink';
import type { PropertyComplexAmenitiesList } from './PropertyComplexAmenitiesList';
import type { PropertyComplexPhoto } from './PropertyComplexPhoto';
import type { PropertyComplexVideoLink } from './PropertyComplexVideoLink';
export type PropertyComplexList = {
    readonly id?: number;
    documents: Array<DocumentLink>;
    videos: Array<PropertyComplexVideoLink>;
    photos: Array<PropertyComplexPhoto>;
    name: string;
    description: string;
    distance_to_sea?: number | null;
    complex_type?: PropertyComplexList.complex_type;
    land_area?: number | null;
    developer: DeveloperList;
    address: AddressList;
    amenities: Array<PropertyComplexAmenitiesList>;
    readonly min_price?: string;
    readonly max_price?: string;
    total_properties?: number | null;
    readonly total_configurations_options?: string;
    price_list?: string;
};
export namespace PropertyComplexList {
    export enum complex_type {
        RESIDENTIAL_COMPLEX = 'residential_complex',
        VILLA_COMPLEX = 'villa_complex',
        BUSINESS_COMPLEX = 'business_complex',
        HOTEL = 'hotel',
    }
}

