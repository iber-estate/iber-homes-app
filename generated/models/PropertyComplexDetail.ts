/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AddressList } from './AddressList';
import type { DeveloperList } from './DeveloperList';
import type { DocumentLink } from './DocumentLink';
import type { PropertyComplexAmenitiesList } from './PropertyComplexAmenitiesList';
import type { PropertyComplexPhoto } from './PropertyComplexPhoto';
import type { PropertyComplexVideoLink } from './PropertyComplexVideoLink';
import type { RealPropertyShort } from './RealPropertyShort';
export type PropertyComplexDetail = {
    readonly id?: number;
    address: AddressList;
    developer: DeveloperList;
    amenities: Array<PropertyComplexAmenitiesList>;
    readonly numbers_of_properties?: string;
    readonly min_price?: string;
    readonly max_price?: string;
    documents: Array<DocumentLink>;
    photos: Array<PropertyComplexPhoto>;
    videos: Array<PropertyComplexVideoLink>;
    property: Array<RealPropertyShort>;
    readonly created_at?: string;
    name: string;
    description: string;
    distance_to_sea?: number | null;
    complex_type?: PropertyComplexDetail.complex_type;
    land_area?: number | null;
    readonly updated_at?: string;
    commission?: string | null;
    completion_date?: string | null;
    price_list?: string;
    total_properties?: number | null;
    creator: number;
};
export namespace PropertyComplexDetail {
    export enum complex_type {
        RESIDENTIAL_COMPLEX = 'residential_complex',
        VILLA_COMPLEX = 'villa_complex',
        BUSINESS_COMPLEX = 'business_complex',
        HOTEL = 'hotel',
    }
}

