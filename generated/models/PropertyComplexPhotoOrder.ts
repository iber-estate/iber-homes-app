/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type PropertyComplexPhotoOrder = {
    sorted_photo_list: Array<number>;
};

