/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AddressList } from './AddressList';
import type { InvestCategory } from './InvestCategory';
import type { InvestDocumentLink } from './InvestDocumentLink';
import type { InvestProjectPhotoCreate } from './InvestProjectPhotoCreate';
import type { InvestProjectPresentationLink } from './InvestProjectPresentationLink';
import type { InvestProjectVideoLink } from './InvestProjectVideoLink';
import type { UserShort } from './UserShort';
export type InvestProjectList = {
    readonly id?: number;
    name?: string;
    description?: string;
    financial_plan?: string;
    business_plan?: string;
    invest_amount?: number;
    expected_profit?: number;
    payback_period?: number;
    category?: InvestCategory;
    address?: AddressList;
    user?: UserShort;
    readonly documents?: Array<InvestDocumentLink>;
    readonly photos?: Array<InvestProjectPhotoCreate>;
    readonly videos?: Array<InvestProjectVideoLink>;
    readonly presentations?: Array<InvestProjectPresentationLink>;
    readonly created_at?: string;
    user_full_name?: string;
    additional_info?: string;
    phone?: string | null;
    goal?: InvestProjectList.goal;
    display_options?: InvestProjectList.display_options | null;
    contact_type?: InvestProjectList.contact_type;
    draft_address?: string;
    status?: InvestProjectList.status;
};
export namespace InvestProjectList {
    export enum goal {
        INVESTMENT = 'investment',
        SALE = 'sale',
    }
    export enum display_options {
        EXCLUSIVE = 'exclusive',
        URGENT = 'urgent',
    }
    export enum contact_type {
        WHATSAPP = 'whatsapp',
        TELEGRAM = 'telegram',
        EMAIL = 'email',
        PHONE = 'phone',
    }
    export enum status {
        DRAFT = 'draft',
        PUBLISHED = 'published',
        ARCHIVED = 'archived',
    }
}

