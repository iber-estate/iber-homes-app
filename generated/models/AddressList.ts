/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { DistrictList } from './DistrictList';
export type AddressList = {
    readonly id?: number;
    district: DistrictList;
    readonly line_1?: string;
    readonly created_at?: string;
    postcode?: string;
    line_2?: string;
    lat: number;
    lng: number;
    geo_point?: string | null;
};

