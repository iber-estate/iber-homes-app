/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CountryShort } from './CountryShort';
export type CityList = {
    readonly id?: number;
    name: string;
    country: CountryShort;
    readonly created_at?: string;
};

