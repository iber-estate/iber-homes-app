/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type InvestProjectPhoto = {
    readonly id?: number;
    readonly thumbnail?: string;
    readonly created_at?: string;
    readonly photo?: string;
    order: number;
    invest_project: number;
};

