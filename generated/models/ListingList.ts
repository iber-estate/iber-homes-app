/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { RealPropertyShort } from './RealPropertyShort';
import type { UserFull } from './UserFull';
export type ListingList = {
    readonly id?: number;
    properties: Array<RealPropertyShort>;
    user: UserFull;
    readonly created_at?: string;
    uuid?: string;
    name: string;
    description: string;
    readonly updated_at?: string;
    readonly image?: string | null;
};

