/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Document } from './Document';
import type { Photo } from './Photo';
import type { Video } from './Video';
export type RealPropertyCreateUpdate = {
    readonly id?: number;
    readonly photos?: Array<Photo>;
    readonly documents?: Array<Document>;
    readonly videos?: Array<Video>;
    description: string;
    readonly created_at?: string;
    year_built: string;
    name: string;
    plot_area?: number | null;
    house_area: number;
    bathrooms: number;
    bedrooms: number;
    floor?: number | null;
    waterview?: boolean;
    distance_to_sea?: number | null;
    status?: RealPropertyCreateUpdate.status;
    property_type: RealPropertyCreateUpdate.property_type;
    sales_term?: string;
    price: number;
    commission: string;
    price_list_url?: string | null;
    readonly updated_at?: string;
    ownership_expired_date?: string | null;
    property_complex: number;
    address: number;
    ownership_type: number;
    amenities: Array<number>;
    tags?: Array<number>;
};
export namespace RealPropertyCreateUpdate {
    export enum status {
        DRAFT = 'draft',
        PUBLISHED = 'published',
        ARCHIVED = 'archived',
    }
    export enum property_type {
        APARTMENT = 'apartment',
        VILLA = 'villa',
        TOWNHOUSE = 'townhouse',
    }
}

