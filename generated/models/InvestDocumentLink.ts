/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type InvestDocumentLink = {
    readonly id?: number;
    document_link?: string;
    invest_project: number;
};

