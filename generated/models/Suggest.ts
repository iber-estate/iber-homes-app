/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type Suggest = {
    input: string;
    readonly countries?: string;
    readonly addresses?: string;
};

