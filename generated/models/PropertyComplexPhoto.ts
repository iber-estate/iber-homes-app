/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type PropertyComplexPhoto = {
    readonly id?: number;
    readonly thumbnail?: string;
    readonly created_at?: string;
    readonly photo?: string;
    order: number;
    property_complex: number;
};

