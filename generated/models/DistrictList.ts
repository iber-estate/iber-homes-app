/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CityList } from './CityList';
export type DistrictList = {
    readonly id?: number;
    city: CityList;
    readonly created_at?: string;
    name: string;
    geo_polygon?: string | null;
};

