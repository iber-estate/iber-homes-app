/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type DeveloperCreateUpdate = {
    readonly id?: number;
    description: string;
    readonly created_at?: string;
    name: string;
    email?: string | null;
    phone?: string | null;
    wa_number?: string | null;
    website?: string | null;
    partner_url?: string | null;
    private_partner_url?: string | null;
    year_of_founding?: string | null;
    readonly updated_at?: string;
    readonly employees?: Array<number>;
};

