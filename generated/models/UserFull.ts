/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type UserFull = {
    readonly id?: number;
    readonly email?: string;
    readonly name?: string;
    readonly phone?: string | null;
    readonly description?: string | null;
    readonly wa_number?: string | null;
    readonly role?: UserFull.role;
    readonly new_notification?: boolean;
    readonly date_joined?: string;
};
export namespace UserFull {
    export enum role {
        ADMINISTRATOR = 'administrator',
        REALTOR = 'realtor',
        DEVELOPER = 'developer',
        FREELANCER = 'freelancer',
        CUSTOMER = 'customer',
    }
}

