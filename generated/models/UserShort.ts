/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type UserShort = {
    readonly id?: number;
    readonly email?: string;
    readonly name?: string;
    readonly phone?: string | null;
};

