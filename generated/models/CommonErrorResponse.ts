/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type CommonErrorResponse = {
    detail: string;
    status_code: number;
    error: string;
};

