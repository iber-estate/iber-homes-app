/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type AddressCreateUpdate = {
    readonly id?: number;
    readonly line_1?: string;
    readonly line_2?: string;
    readonly district?: string;
    readonly geo_point?: string;
    readonly created_at?: string;
    postcode?: string;
    lat: number;
    lng: number;
};

