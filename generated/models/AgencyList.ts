/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type AgencyList = {
    readonly id?: number;
    readonly employees?: Array<string>;
    readonly created_at?: string;
    name: string;
    email?: string | null;
    website?: string | null;
    description: string;
};

