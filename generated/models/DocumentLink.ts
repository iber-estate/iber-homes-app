/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type DocumentLink = {
    readonly id?: number;
    document_link?: string;
    property_complex: number;
};

