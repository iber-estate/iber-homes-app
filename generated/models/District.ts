/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type District = {
    readonly id?: number;
    readonly created_at?: string;
    name: string;
    geo_polygon?: string | null;
    city: number;
};

