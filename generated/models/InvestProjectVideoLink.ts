/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type InvestProjectVideoLink = {
    readonly id?: number;
    readonly created_at?: string;
    video_link?: string;
    invest_project: number;
};

