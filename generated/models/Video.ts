/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type Video = {
    readonly id?: number;
    readonly created_at?: string;
    readonly video?: string | null;
    external_url?: string;
    real_property: number;
};

