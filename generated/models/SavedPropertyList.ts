/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { RealPropertyList } from './RealPropertyList';
export type SavedPropertyList = {
    real_property?: RealPropertyList;
};

