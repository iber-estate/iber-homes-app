/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type PropertyComplexVideoLink = {
    readonly id?: number;
    readonly created_at?: string;
    video_link?: string;
    property_complex: number;
};

