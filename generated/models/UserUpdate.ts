/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type UserUpdate = {
    name: string;
    phone?: string | null;
    description?: string | null;
    wa_number?: string | null;
    role?: UserUpdate.role;
};
export namespace UserUpdate {
    export enum role {
        ADMINISTRATOR = 'administrator',
        REALTOR = 'realtor',
        DEVELOPER = 'developer',
        FREELANCER = 'freelancer',
        CUSTOMER = 'customer',
    }
}

