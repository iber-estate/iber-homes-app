/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type City = {
    readonly id?: number;
    readonly created_at?: string;
    name: string;
    country: number;
};

