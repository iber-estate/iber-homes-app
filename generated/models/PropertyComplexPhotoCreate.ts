/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type PropertyComplexPhotoCreate = {
    readonly id?: number;
    readonly created_at?: string;
    readonly photo?: string;
};

