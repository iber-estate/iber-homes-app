/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { InvestDocumentLink } from './InvestDocumentLink';
import type { InvestProjectPhotoCreate } from './InvestProjectPhotoCreate';
import type { InvestProjectPresentationLink } from './InvestProjectPresentationLink';
import type { InvestProjectVideoLink } from './InvestProjectVideoLink';
export type InvestProjectCreateUpdate = {
    readonly id?: number;
    readonly documents?: Array<InvestDocumentLink>;
    readonly photos?: Array<InvestProjectPhotoCreate>;
    email: string;
    readonly videos?: Array<InvestProjectVideoLink>;
    readonly presentations?: Array<InvestProjectPresentationLink>;
    draft_address: string;
    user_full_name: string;
    readonly access?: string;
    readonly created_at?: string;
    readonly deleted_at?: string | null;
    name: string;
    description: string;
    invest_amount: number;
    expected_profit: number;
    payback_period: number;
    financial_plan: string;
    business_plan: string;
    additional_info?: string;
    phone?: string | null;
    goal?: InvestProjectCreateUpdate.goal;
    display_options?: InvestProjectCreateUpdate.display_options | null;
    contact_type?: InvestProjectCreateUpdate.contact_type;
    status?: InvestProjectCreateUpdate.status;
    category: number;
    readonly user?: number;
    address?: number | null;
};
export namespace InvestProjectCreateUpdate {
    export enum goal {
        INVESTMENT = 'investment',
        SALE = 'sale',
    }
    export enum display_options {
        EXCLUSIVE = 'exclusive',
        URGENT = 'urgent',
    }
    export enum contact_type {
        WHATSAPP = 'whatsapp',
        TELEGRAM = 'telegram',
        EMAIL = 'email',
        PHONE = 'phone',
    }
    export enum status {
        DRAFT = 'draft',
        PUBLISHED = 'published',
        ARCHIVED = 'archived',
    }
}

