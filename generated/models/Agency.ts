/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type Agency = {
    readonly id?: number;
    readonly created_at?: string;
    name: string;
    email?: string | null;
    website?: string | null;
    description: string;
    readonly employees?: Array<number>;
};

