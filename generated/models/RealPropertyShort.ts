/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AddressList } from './AddressList';
import type { Document } from './Document';
import type { Photo } from './Photo';
import type { RealPropertyAmenitiesList } from './RealPropertyAmenitiesList';
import type { TagsList } from './TagsList';
import type { Video } from './Video';
export type RealPropertyShort = {
    readonly id?: number;
    photos: Array<Photo>;
    videos: Array<Video>;
    amenities: Array<RealPropertyAmenitiesList>;
    tags: Array<TagsList>;
    documents: Array<Document>;
    address: AddressList;
    readonly created_at?: string;
    year_built: string;
    name: string;
    description: string;
    plot_area?: number | null;
    house_area: number;
    bathrooms: number;
    bedrooms: number;
    floor?: number | null;
    waterview?: boolean;
    distance_to_sea?: number | null;
    status?: RealPropertyShort.status;
    property_type: RealPropertyShort.property_type;
    sales_term?: string;
    price: number;
    commission: string;
    price_list_url?: string | null;
    readonly updated_at?: string;
    ownership_expired_date?: string | null;
    property_complex: number;
    ownership_type: number;
    creator: number;
};
export namespace RealPropertyShort {
    export enum status {
        DRAFT = 'draft',
        PUBLISHED = 'published',
        ARCHIVED = 'archived',
    }
    export enum property_type {
        APARTMENT = 'apartment',
        VILLA = 'villa',
        TOWNHOUSE = 'townhouse',
    }
}

