/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type OwnershipTypesList = {
    id: number;
    readonly name?: string;
    description?: string;
    country?: number | null;
};

