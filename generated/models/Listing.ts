/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type Listing = {
    readonly id?: number;
    readonly created_at?: string;
    uuid?: string;
    name?: string;
    description?: string;
    readonly updated_at?: string;
    readonly image?: string | null;
    properties?: Array<number>;
};

