/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type DeveloperList = {
    readonly id?: number;
    readonly employees?: Array<string>;
    readonly numbers_of_properties?: string;
    readonly created_at?: string;
    name: string;
    email?: string | null;
    phone?: string | null;
    wa_number?: string | null;
    website?: string | null;
    partner_url?: string | null;
    private_partner_url?: string | null;
    description?: string | null;
    readonly avatar?: string | null;
    year_of_founding?: string | null;
    readonly updated_at?: string;
    creator: number;
};

