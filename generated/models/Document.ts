/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type Document = {
    readonly id?: number;
    readonly created_at?: string;
    readonly file?: string | null;
    external_url?: string;
    description: string;
    real_property: number;
};

