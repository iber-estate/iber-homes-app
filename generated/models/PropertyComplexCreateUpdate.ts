/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { DocumentLink } from './DocumentLink';
import type { PropertyComplexPhotoCreate } from './PropertyComplexPhotoCreate';
import type { PropertyComplexVideoLink } from './PropertyComplexVideoLink';
export type PropertyComplexCreateUpdate = {
    readonly id?: number;
    readonly documents?: Array<DocumentLink>;
    readonly videos?: Array<PropertyComplexVideoLink>;
    readonly photos?: Array<PropertyComplexPhotoCreate>;
    name: string;
    description: string;
    distance_to_sea?: number | null;
    complex_type?: PropertyComplexCreateUpdate.complex_type;
    land_area?: number | null;
    developer: number;
    address: number;
    amenities: Array<number>;
    total_properties?: number | null;
    price_list?: string;
};
export namespace PropertyComplexCreateUpdate {
    export enum complex_type {
        RESIDENTIAL_COMPLEX = 'residential_complex',
        VILLA_COMPLEX = 'villa_complex',
        BUSINESS_COMPLEX = 'business_complex',
        HOTEL = 'hotel',
    }
}

