import { auth } from "@/auth";
import { redirect } from "next/navigation";

export const protectRoute = () => {
  const session = auth();
  // @ts-ignore
  if (!session?.user) {
    // redirect("/signup");
  }
};
