import { ApiResult } from "@/generated/core/ApiResult";

export const parseErrorMessage = (error: ApiResult) => {
  const errorsKeys = Object.keys(error.body);
  return errorsKeys.reduce((acc, key) => {
    return `${acc}${key}: ${error.body[key].join(", ")}\n`;
  }, "");
};
