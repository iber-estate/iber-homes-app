// apiService.ts

import axios, { AxiosRequestConfig, Method } from "axios";

// Define a base URL for your API
const BASE_URL = "https://dev.iber.homes/api/v1"; // Replace with your API base URL

// Default headers
const defaultHeaders = {
  "Content-Type": "application/json",
  // other default headers
};

// Create an Axios instance
const api = axios.create({
  baseURL: BASE_URL,
  headers: defaultHeaders,
});

// Generic function to handle requests
const makeRequest = async <T = any>({
  method,
  url,
  data,
  params,
  headers,
}: {
  method: Method;
  url: string;
  data?: any;
  params?: any;
  headers?: any;
}): Promise<T> => {
  const config: AxiosRequestConfig = {
    method,
    url,
    data,
    params,
    headers: { ...defaultHeaders, ...headers },
  };

  try {
    const response = await api.request<T>(config);
    return response.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export default makeRequest;
