import { useContext } from "react";
import { StoreContext } from "./StoreProvider";

export const useStore = () => {
  const store = useContext(StoreContext);
  return store;
};
