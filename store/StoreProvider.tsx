"use client";
import React, { createContext, useState } from "react";

type Store = {
  data: {
    isAuthModalOpen: boolean;
    saveBeforeAuth?: number;
  };
  // TODO: make uniq setter for every field/scope
  setValue: (key: keyof Store["data"], value: any) => void;
  setStore: (store: any) => void;
};

const defaultStore = { isAuthModalOpen: false };

export const StoreContext = createContext({
  data: defaultStore,
  setValue: (key: keyof Store["data"], value: any) => {},
} as Store);

export const StoreProvider = ({ children }: any) => {
  const [store, setStore] = useState(defaultStore);
  const setValue = (key: keyof Store["data"], value: any) => {
    setStore({ ...store, [key]: value });
  };
  return (
    <StoreContext.Provider value={{ data: store, setValue, setStore }}>
      {children}
    </StoreContext.Provider>
  );
};
