import { useQuery } from "@tanstack/react-query";
import { ProjectService } from "@/generated";

const useInvestProjects = () => {
  const { data, isLoading } = useQuery({
    queryKey: ["projectList"],
    queryFn: async () => {
      const response = await ProjectService.projectList({});
      return response.results;
    },
  });

  return { projects: data, isProjectLoading: isLoading };
};

export default useInvestProjects;
