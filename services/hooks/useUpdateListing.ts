import { useMutationWrapper } from "@/services/hooks/useMutationDefault";
import { ListingService } from "@/generated";

export const useUpdateListing = () => {
  const res = useMutationWrapper({
    mutationFn: ListingService.listingPartialUpdate,
  });
};
