import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { parseErrorMessage } from "@/utils/errors";

export type useServiceProps = {
  onError?: (error: any) => void;
  onSuccess?: (data: any, variables: any, context: any) => void;
  invalidateKeys?: string[];
  successMessage?: string;
};
type AsyncFunction = (...args: any) => Promise<any>;
type useMutationDefaultProps<T extends AsyncFunction> = {
  mutationFn: T; // Dynamic function type
} & useServiceProps; // Combine with the properties from `useServiceProps`

export const useMutationWrapper = <T extends AsyncFunction>(
  props: useMutationDefaultProps<T>,
) => {
  const queryClient = useQueryClient();
  const { onError, onSuccess, mutationFn, invalidateKeys, successMessage } =
    props;
  const res = useMutation({
    mutationFn,
    onSuccess: (data, variables, context) => {
      if (invalidateKeys) {
        invalidateKeys.forEach((key) => {
          queryClient.invalidateQueries({
            queryKey: [key],
          });
        });
      }
      if (successMessage) toast.success(successMessage);
      if (onSuccess) {
        onSuccess(data, variables, context);
        return;
      }
    },
    onError: (error: any) => {
      if (onError) {
        onError(error);
        return;
      }
      toast.error(parseErrorMessage(error));
    },
  });

  return { ...res, mutate: res.mutate as typeof mutationFn };
};
