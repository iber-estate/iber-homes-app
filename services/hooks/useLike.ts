import { queryKeys } from "@/types";
import { useQueryClient } from "@tanstack/react-query";
import { PropertyService, SavedService } from "@/generated";
import { useMutationWrapper } from "@/services/hooks/useMutationDefault";

type useLikeProps = {
  invalidateProps?: boolean;
};

export const useLike = (props?: useLikeProps) => {
  const { invalidateProps } = props || {};
  const queryClient = useQueryClient();

  const syncData = (id: number, action: "save" | "unsave") => {
    invalidateProps &&
      queryClient.invalidateQueries({ queryKey: [queryKeys.properties] });
    queryClient.invalidateQueries({ queryKey: [queryKeys.me] });
    queryClient.invalidateQueries({ queryKey: [queryKeys.listings] });
    queryClient.setQueriesData(
      { queryKey: [queryKeys.properties] },
      (old: any) => {
        return old.map((property: any) => {
          if (property.id === id) {
            property.is_saved = action === "save";
          }
          return property;
        });
      },
    );
  };

  const { mutate: save } = useMutationWrapper({
    mutationFn: SavedService.savedCreate,
    onSuccess: (_, props) => {
      syncData(props.data.real_property, "save");
    },
  });

  const { mutate: unsave } = useMutationWrapper({
    mutationFn: PropertyService.propertyDelSavedProperty,
    onSuccess: (_, props) => {
      syncData(props.id, "unsave");
    },
  });

  return { save, unsave };
};
