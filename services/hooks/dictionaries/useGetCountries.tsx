import { useQuery } from "@tanstack/react-query";
import { CountryService } from "@/generated";

const useGetCountries = (returnForSelect: boolean) => {
  const { data, isLoading, error } = useQuery({
    queryKey: ["countries"],
    queryFn: async () => {
      const response = await CountryService.countryList({});
      return response.results;
    },
  });
  return {
    countries: data,
    isCountriesLoading: isLoading,
    countriesError: error,
  };
};

export default useGetCountries;
