import { ListingService } from "@/generated";
import {
  useMutationWrapper,
  useServiceProps,
} from "@/services/hooks/useMutationDefault";
import { queryKeys } from "@/types";

const useCreateListing = (props: useServiceProps = {}) => {
  const res = useMutationWrapper({
    mutationFn: ListingService.listingCreate,
    invalidateKeys: [queryKeys.listings],
    successMessage: "Collection created!",
    ...props,
  });

  return res;
};

export default useCreateListing;
