const propertiesService = {
  getProperties: async () => {
    const response = await fetch("https://api.example.com/properties");
    const properties = await response.json();
    return properties;
  },
};
