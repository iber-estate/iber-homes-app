const axios = require("axios");
const OpenAPI = require("openapi-typescript-codegen");

axios
  .get("https://app-dev.iber.homes/api/v1/swagger/?format=openapi")
  // @ts-ignore
  .then((res) => {
    if (res.status !== 200) return;
    OpenAPI.generate({
      input: res.data,
      output: "./generated",
      request: "./request.ts",
      useOptions: true,
      httpClient: "axios",
    });
  });
