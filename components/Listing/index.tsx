import React from "react";
import { ListingView } from "./Listing";
import { FilterType } from "@/app/(main)/search/types";
import { PropertyService } from "@/generated";
import { getCleanFilterValues } from "@/app/(main)/search/utils";

type ListingProps = {
  searchParams: FilterType;
  title?: string;
};

export const Listing = async (props: ListingProps) => {
  const { searchParams } = props;
  try {
    const res = await PropertyService.propertyList({
      ...getCleanFilterValues(searchParams),
      status: "published",
    });
    return <ListingView {...props} data={res.results} />;
  } catch (e) {
    return null;
  }
};
