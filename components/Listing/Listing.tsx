"use client";
import React, { useRef, useState } from "react";
import Slider from "react-slick";
import { FilterType } from "@/app/(main)/search/types";
import { objectToQueryString } from "@/app/(main)/search/utils";
import { RealPropertyList } from "@/generated";
import { PropertyCard } from "@/components/PropertyCard";
import { Typography } from "@/components/Typography";
import { ArrowIcon } from "@/components/Icons";
import { twMerge } from "tailwind-merge";
import { Skeleton } from "@/components/Skeleton";
import Button from "@/components/Button";
import Link from "next/link";

type ListingProps = {
  data: RealPropertyList[];
  searchParams: FilterType;
  title?: string;
};

const Control = ({
  onClick,
  icon,
  className,
}: {
  onClick: () => void;
  icon: string | React.ReactNode;
  className?: string;
}) => (
  <button
    className={twMerge(
      className,
      "flex h-[48px] w-[48px] items-center justify-center rounded-full bg-lightbluethree",
    )}
    onClick={onClick}
  >
    {typeof icon === "object" && icon}
  </button>
);

export const ListingView = (props: ListingProps) => {
  const { title, data = [], searchParams } = props;
  const [currSlide, setCurrSlide] = useState(0);
  let sliderRef = useRef<any>(null);

  const next = () => {
    if (!data) return;
    if (currSlide === data.length - 3) return;
    // @ts-ignore
    sliderRef.slickNext();
  };

  const prev = () => {
    if (currSlide === 0) return;
    // @ts-ignore
    sliderRef.slickPrev();
  };

  const settings = {
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    beforeChange(currentSlide: number, nextSlide: number) {
      setCurrSlide(nextSlide);
    },
  };
  // TODO: tired, check again
  return (
    <div>
      <Typography
        type="h3"
        className="mb-[28px] flex items-center justify-between"
      >
        <Link
          href={`${objectToQueryString("/search", searchParams)}`}
          className="sm:hidden"
        >
          {title}
        </Link>
        <div className="flex gap-[12px] sm:hidden">
          <Control
            onClick={prev}
            className={currSlide === 0 ? "opacity-50" : ""}
            icon={
              <ArrowIcon color="#104551" w="32" h="32" className="rotate-180" />
            }
          />
          <Control
            onClick={next}
            className={currSlide === data.length - 3 ? "opacity-50" : ""}
            icon={<ArrowIcon color="#104551" w="32" h="32" />}
          />
        </div>
      </Typography>
      <Typography type="h2" className="hidden text-primary-dark sm:block">
        <Link href={`${objectToQueryString("/search", searchParams)}`}>
          {title}
        </Link>
      </Typography>
      <div
        className="sm:hidden"
        style={{
          width: "calc(100% + 28px)",
        }}
      >
        <Slider
          ref={(slider) => {
            // @ts-ignore
            sliderRef = slider;
          }}
          {...settings}
        >
          {!data &&
            Array.from({ length: 3 }).map((_, index) => {
              return (
                <div className="pr-[28px]" key={index}>
                  <Skeleton />
                </div>
              );
            })}
          {data?.map((property, index) => (
            <div className="pr-[28px]" key={property.id}>
              {index === data.length - 1 ? (
                <div className="relative">
                  <Link
                    href={`${objectToQueryString("/search", searchParams)}`}
                    className="absolute z-10 flex aspect-[323/220] w-full items-center justify-center rounded-[8px] bg-[rgba(210,210,210,0.6)]"
                  >
                    <Button>See more</Button>
                  </Link>
                  <PropertyCard property={property} />
                </div>
              ) : (
                <PropertyCard property={property} />
              )}
            </div>
          ))}
        </Slider>
      </div>
      <div
        className="hidden overflow-scroll sm:flex sm:w-[calc(100%+24px)]"
        style={{ scrollbarWidth: "none" }}
      >
        {data?.map((property, index) => (
          <div className="min-w-[280px] pr-[28px]" key={property.id}>
            <PropertyCard property={property} />
          </div>
        ))}
      </div>
    </div>
  );
};
