"use client";
import { useState, useEffect, useRef, ReactNode, useCallback } from "react";
import Image from "next/image";
import arrowUp from "@/public/icons/arrowUp.svg";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";

type SelectProps = {
  children: ReactNode;
  label?: string;
  placeholder?: string;
  title?: string;
  className?: string;
  iconLeft?: string;
  iconRight?: string;
  variant?: "default" | "text";
  size?: "md" | "default";
  type?: "text" | "button";
  prefix?: ReactNode | string;
  controls?: {
    isOpen: boolean;
    setIsOpen: (b: boolean) => void;
  };
  customButton?: ReactNode;
};

export const SelectDumb = (props: SelectProps) => {
  const {
    label,
    className,
    iconLeft,
    placeholder = "Any",
    title,
    children,
    type = "button",
    prefix,
    controls,
    customButton,
  } = props;

  const [isOpenInner, setIsOpenInner] = useState(false);
  const isOpen = controls ? controls.isOpen : isOpenInner;
  const selectRef = useRef<HTMLDivElement>(null);
  const handleToggleOptions = () => {
    if (controls?.setIsOpen) {
      controls?.setIsOpen(!controls?.isOpen);
      return;
    }
    setIsOpenInner((prevState) => !prevState);
  };

  const handleClickOutside = useCallback(
    (event: MouseEvent) => {
      if (
        selectRef.current &&
        !selectRef.current.contains(event.target as Node)
      ) {
        if (controls) {
          controls?.setIsOpen(false);
          return;
        }
        setIsOpenInner(false);
      }
    },
    [controls],
  );

  useEffect(() => {
    document.addEventListener("click", handleClickOutside);
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, [handleClickOutside]);

  return (
    <div ref={selectRef} className={`relative ${className}`}>
      <label className="text-gray-700 block text-[14px]">{label}</label>
      <div className="relative">
        <button
          type="button"
          className={`flex h-[48px] w-full items-center justify-between truncate rounded-[8px] border px-[16px] py-[12px] pr-[40px] text-left outline-none  ${isOpen ? "border-primary" : "border-stroke"} ${type === "text" ? "border-transparent" : "hover:border-primary focus:border-primary sm:justify-center sm:border-none sm:bg-[#EFEEEA]"} focus:outline-none`}
          onClick={handleToggleOptions}
        >
          {iconLeft && (
            <Image alt="icon" width={24} height={24} src={iconLeft} />
          )}

          <Typography
            type={window.innerWidth > 639 ? "body" : "subtitle2"}
            className={
              title
                ? "truncate text-black sm:text-primary-dark"
                : "text-defaultText"
            }
          >
            {prefix ? (
              <span className="mr-[6px] text-defaultText">{prefix}</span>
            ) : (
              ""
            )}
            {title || placeholder}
          </Typography>
          <Image
            className={`absolute right-[16px] top-[50%] -translate-y-1/2 transform ${isOpen ? "rotate-[0deg]" : "rotate-[180deg]"}`}
            src={arrowUp}
            alt="arrowDown"
          />
        </button>

        <div
          className={`absolute z-10 mt-[4px] min-w-full ${!isOpen && "hidden"}`}
        >
          {children}
        </div>
      </div>
    </div>
  );
};
