type IconProps = {
  color?: string;
  w?: string;
  h?: string;
  className?: string;
};

export const ArrowIcon = ({
  color = "#222222",
  w = "24",
  h = "24",
  className,
}: IconProps) => (
  <svg
    className={className}
    width={w}
    height={h}
    viewBox={`0 0 24 24`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M9 18L15 12L9 6"
      stroke={color}
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export const WhatsApp = ({
  color = "#222222",
  w = "24",
  h = "24",
  className,
}: IconProps) => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M18.2022 5.76205C16.5562 4.11505 14.3672 3.20705 12.0352 3.20605C7.22819 3.20605 3.31719 7.11505 3.31619 11.9201C3.31419 13.4491 3.71519 14.9521 4.47919 16.2771L3.24219 20.7931L7.86419 19.5811C9.14319 20.2771 10.5752 20.6421 12.0312 20.6421H12.0352C16.8402 20.6421 20.7512 16.7321 20.7532 11.9271C20.7542 9.59905 19.8482 7.41005 18.2022 5.76205Z"
      stroke="#104551"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.0938 13.5599L13.4997 13.1569C13.8727 12.7869 14.4628 12.7399 14.8928 13.0419C15.3088 13.3339 15.6847 13.5959 16.0347 13.8399C16.5907 14.2259 16.6578 15.0179 16.1788 15.4959L15.8198 15.8549"
      stroke="#104551"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M8.14844 8.17938L8.50744 7.82038C8.98544 7.34238 9.77744 7.40938 10.1634 7.96438C10.4064 8.31438 10.6684 8.69038 10.9614 9.10638C11.2634 9.53638 11.2174 10.1264 10.8464 10.4994L10.4434 10.9054"
      stroke="#104551"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M15.8178 15.8553C14.3368 17.3293 11.8478 16.0773 9.88281 14.1113"
      stroke="#104551"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M9.88807 14.1147C7.92307 12.1487 6.67107 9.66069 8.14507 8.17969"
      stroke="#104551"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M10.4375 10.9053C10.7565 11.4083 11.1655 11.9063 11.6275 12.3683L11.6295 12.3703C12.0915 12.8323 12.5895 13.2413 13.0925 13.5603"
      stroke="#104551"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export const CrossIcon = ({
  color = "#222222",
  w = "24",
  h = "24",
  className,
}: IconProps) => (
  <svg
    width={w}
    height={h}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M4.29289 4.29289C4.68342 3.90237 5.31658 3.90237 5.70711 4.29289L12 10.5858L18.2929 4.29289C18.6834 3.90237 19.3166 3.90237 19.7071 4.29289C20.0976 4.68342 20.0976 5.31658 19.7071 5.70711L13.4142 12L19.7071 18.2929C20.0976 18.6834 20.0976 19.3166 19.7071 19.7071C19.3166 20.0976 18.6834 20.0976 18.2929 19.7071L12 13.4142L5.70711 19.7071C5.31658 20.0976 4.68342 20.0976 4.29289 19.7071C3.90237 19.3166 3.90237 18.6834 4.29289 18.2929L10.5858 12L4.29289 5.70711C3.90237 5.31658 3.90237 4.68342 4.29289 4.29289Z"
      fill={color}
    />
  </svg>
);

const HeartFullIcon = ({
  color = "#222222",
  w = "24",
  h = "24",
  className,
}: IconProps) => (
  <svg
    width={w}
    height={h}
    viewBox={`0 0 ${w} ${h}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M15.7 4C18.87 4 21 6.98 21 9.76C21 15.39 12.16 20 12 20C11.84 20 3 15.39 3 9.76C3 6.98 5.13 4 8.3 4C10.12 4 11.31 4.91 12 5.71C12.69 4.91 13.88 4 15.7 4Z"
      fill={color}
    />
  </svg>
);
const HeartStrokeIcon = ({
  color = "#222222",
  w = "24",
  h = "24",
  className,
}: IconProps) => (
  <svg
    width={w}
    height={h}
    viewBox={`0 0 ${w} ${h}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M11.4321 6.19985C11.5745 6.36503 11.7819 6.46 12 6.46C12.2181 6.46 12.4255 6.36503 12.5679 6.19985C13.1633 5.50962 14.162 4.75 15.7 4.75C18.3462 4.75 20.25 7.2754 20.25 9.76C20.25 10.9685 19.775 12.1666 18.9958 13.3138C18.2174 14.4599 17.1634 15.5132 16.0802 16.4148C14.9999 17.314 13.9113 18.0454 13.0827 18.5517C12.6689 18.8046 12.3232 18.9993 12.0798 19.1291C12.0516 19.1441 12.025 19.1582 12 19.1713C11.975 19.1582 11.9484 19.1441 11.9202 19.1291C11.6768 18.9993 11.3311 18.8046 10.9173 18.5517C10.0887 18.0454 9.00011 17.314 7.91982 16.4148C6.83662 15.5132 5.78263 14.4599 5.00418 13.3138C4.22504 12.1666 3.75 10.9685 3.75 9.76C3.75 7.2754 5.65379 4.75 8.3 4.75C9.83802 4.75 10.8367 5.50962 11.4321 6.19985Z"
      stroke={color}
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

const PlusIcon = ({
  color = "#222222",
  w = "16",
  h = "16",
  className,
}: IconProps) => (
  <svg
    width={w}
    height={h}
    viewBox={`0 0 ${"24"} ${"24"}`}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="icons">
      <path
        id="Vector 2 (Stroke)"
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12 3C12.5523 3 13 3.44772 13 4V11H20C20.5523 11 21 11.4477 21 12C21 12.5523 20.5523 13 20 13H13V20C13 20.5523 12.5523 21 12 21C11.4477 21 11 20.5523 11 20V13H4C3.44772 13 3 12.5523 3 12C3 11.4477 3.44772 11 4 11H11V4C11 3.44772 11.4477 3 12 3Z"
        fill={color}
      />
    </g>
  </svg>
);

export const Icons = {
  ArrowIcon,
  CrossIcon,
  HeartFullIcon,
  HeartStrokeIcon,
  PlusIcon,
  WhatsApp,
};
