"use client";
import React, { useContext, useState } from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { PropertyService, RealPropertyList, SavedService } from "@/generated";
import { toast } from "react-toastify";
import { parseErrorMessage } from "@/utils/errors";
import { useSession } from "next-auth/react";
import Image from "next/image";
import Button from "@/components/Button";
import { Icons } from "@/components/Icons";
import { queryKeys } from "@/types";
import { useStore } from "@/store/useStore";
import { useLike } from "@/services/hooks/useLike";

type LikeProps = {
  is_saved: boolean;
  id: number;
  variant?: "button" | "icon";
  size?: "default" | "small";
  className?: string;
};

export const Like = (props: LikeProps) => {
  const { setStore, data } = useStore();
  const {
    is_saved: isInitiallySaved,
    id,
    variant = "icon",
    size = "default",
    className,
  } = props;
  const [isSaved, setIsSaved] = useState<any>(isInitiallySaved);
  const session = useSession();
  const [likeIcon, setLikeIcon] = useState(
    isInitiallySaved ? "/icons/heartFilled.svg" : "/icons/heartOutlined.svg",
  );

  const { save, unsave } = useLike();

  const handleLike = async () => {
    if (!session.data) {
      setStore({ ...data, saveBeforeAuth: id, isAuthModalOpen: true });
      return;
    }
    if (isSaved) {
      setIsSaved(false);
      setLikeIcon("/icons/heartOutlined.svg");
      if (id) unsave({ id });
    } else {
      setIsSaved(true);
      setLikeIcon("/icons/heartFilled.svg");
      if (id) save({ data: { real_property: id } });
    }
  };

  return variant === "button" ? (
    <Button
      className={`${className} w-[240px] sm:w-auto`}
      type="button"
      onClick={handleLike}
      iconRight={
        isSaved ? (
          <Icons.HeartFullIcon color="#fff" />
        ) : (
          <Icons.HeartStrokeIcon color="#fff" />
        )
      }
    >
      {isSaved ? "Remove from listings" : "Save for listings"}
    </Button>
  ) : (
    <button
      type="button"
      className="cursor-pointer outline-none"
      onClick={handleLike}
    >
      <Image width={44} height={44} src={likeIcon} alt="like" />
    </button>
  );
};
