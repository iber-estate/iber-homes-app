import React from "react";
import { twMerge } from "tailwind-merge";

type TextProps = {
  type:
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "h6"
    | "text"
    | "paragraph"
    | "info"
    | "subtitle"
    | "subtitle2"
    | "subtitle3"
    | "caption"
    | "body"
    | "body2";

  children: React.ReactNode;
  className?: string;
  tag?: string;
};

export const Typography: React.FC<TextProps> = ({
  type,
  children,
  className,
  tag,
}) => {
  let resultParams = { tag: "span", className: "" };
  switch (type) {
    case "h1":
      resultParams = {
        className:
          "font-satoshi text-[3.25rem] leading-[3.5rem] font-black sm:text-[28px] sm:leading-[2rem]",
        tag: "h1",
      };
      break;
    case "h2":
      resultParams = {
        className:
          "font-satoshi text-[3rem] leading-[3.75rem] font-black mb-[16px] sm:text-[23px] sm:leading-[28px] sm:font-[600]",
        tag: "h2",
      };
      break;
    case "h3":
      resultParams = {
        className:
          "font-satoshi text-[2.75rem] leading-[3rem] font-black mb-[16px] sm:text-[20px] sm:leading-[24px] sm:font-[600] text-primary-dark",
        tag: "h3",
      };
      break;
    case "h4":
      resultParams = {
        className:
          "font-satoshi text-[2.5rem] leading-[3rem] mb-[16px] font-semibold sm:text-[16px] sm:leading-[20px] sm:mb-[12px]",
        tag: "h4",
      };
      break;
    case "h5":
      resultParams = {
        className: "text-[28px] leading-[32px] font-medium mb-[16px]",
        tag: "h5",
      };
      break;
    case "h6":
      resultParams = {
        className: "text-[20px] leading-[28px] font-[400] mb-[16px]",
        tag: "h6",
      };
      break;
    case "paragraph":
      resultParams = {
        className: "text-base text-[#4E4B66] leading-8",
        tag: "p",
      };
      break;
    case "subtitle":
      resultParams = {
        className:
          "text-[1.75rem] leading-[2rem] font-[300] sm:text-[16px] sm:leading-[20px]",
        tag: "p",
      };
      break;
    case "subtitle2":
      resultParams = {
        className:
          "text-[1.25rem] leading-[1.5rem] font-[200] sm:text-[14px] sm:leading-[16px] sm:font-[400]",
        tag: "p",
      };
      break;
    case "subtitle3":
      resultParams = {
        className:
          "text-[16px] leading-[20px] font-[500] mb-[16px] sm:text-[0.875rem] sm:leading-[1rem] sm:font-[300] text-primary-dark",
        tag: "p",
      };
      break;
    case "caption":
      resultParams = {
        className: "text-[0.825rem] leading-[1.25rem] font-[300]",
        tag: "p",
      };
      break;
    case "body":
      resultParams = {
        className:
          "text-black text-[1rem] leading-[24px] font-[300] sm:text-[0.8125rem] sm:leading-[1rem]",
        tag: "p",
      };
      break;
    case "body2":
      resultParams = {
        className:
          "text-black text-[16px] leading-[24px] font-[300] sm:text-[0.8125rem] sm:leading-[1rem]",
        tag: "p",
      };
      break;
    case "text":
      resultParams = {
        className: "text-base text-[#4E4B66] leading-8",
        tag: "span",
      };
      break;
    case "info":
      resultParams = {
        className: "text-base text-[#6E7191]",
        tag: "small",
      };
      break;
  }

  resultParams.className = twMerge(resultParams.className, className);
  return React.createElement(
    tag || resultParams.tag,
    {
      className: resultParams.className,
      style: { fontVariantLigatures: "no-common-ligatures" },
    },
    children,
  );
};
