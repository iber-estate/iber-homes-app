"use client";
import React, { useState, FunctionComponent } from "react";
import { Typography } from "@/components/Typography";
import Image from "next/image";
type AccordionItem = {
  title: string;
  content: string;
};

type AccordionProps = {
  items: AccordionItem[];
};

const Accordion: FunctionComponent<AccordionProps> = ({ items }) => {
  const [activeIndex, setActiveIndex] = useState<null | number>(null);

  const toggleItem = (index: number) => {
    setActiveIndex(activeIndex === index ? null : index);
  };

  return (
    <div className="flex w-full flex-col gap-[28px]">
      {items.map((item, index) => (
        <div key={index} className="border-b border-[#C9DBD8]">
          <button
            className="mb-[28px] flex w-full justify-between sm:mb-[16px]"
            onClick={() => toggleItem(index)}
          >
            <Typography type="subtitle2" className="text-black sm:text-left">
              {item.title}
            </Typography>
            <Image
              src={
                activeIndex === index ? "/icons/minus.svg" : "/icons/plus.svg"
              }
              alt="plus"
              width={24}
              height={24}
              className="sm:ml-[16px]"
            />
          </button>
          {activeIndex === index && (
            <Typography type="body" className="mb-[28px] text-black">
              {item.content}
            </Typography>
          )}
        </div>
      ))}
    </div>
  );
};

export default Accordion;
