import Image from "next/image";
import { twMerge } from "tailwind-merge";
import { Typography } from "@/components/Typography";

type ButtonProps = {
  variant?: "primary" | "transparent" | "text" | "secondary";
  color?: "grey" | "default" | "white" | "light";
  size?: "base" | "md" | "sm";
  children?: React.ReactNode;
  className?: string;
  textClassName?: string;
  iconLeft?: string | React.ReactNode;
  iconRight?: string | React.ReactNode;
  loading?: boolean;
  disabled?: boolean;
  onClick?: () => void;
  type?: "button" | "submit" | "reset";
};

const Button: React.FC<ButtonProps> = (props) => {
  const {
    variant = "primary",
    size = "base",
    color = "default",
    children,
    className,
    textClassName,
    iconLeft,
    iconRight,
    disabled,
    loading,
    onClick,
    type,
  } = props;

  const colorStyles = {
    default: {
      primary: `
        bg-buttonAquamarine
        border border-[2px] border-buttonAquamarine
        text-white
        font-[200]
        hover:bg-primary
        hover:border hover:border-primary-light hover:border-[2px]
        active:border-transparent
        `,
      transparent: `
        text-primary
        border-primary
        bg-transparent
        hover:bg-blue-100
        `,
      secondary: `
        text-primary-dark
        border-primary
        bg-primary-light
        border border-transparent border-[2px]
        hover:bg-primary
        hover:border hover:border-primary-light hover:border-[2px] hover:text-white
        active:border-transparent
        `,
      text: "text-blue-600",
    },
    light: {
      primary: `
        bg-primary-light
        border border-[2px] border-primary-light
        text-primary-dark
        font-[200]
        hover:bg-primary hover:text-white 
        hover:border hover:border-primary-light hover:border-[2px]
        `,
      secondary: "",
      transparent: "",
      text: "",
    },
    grey: {
      primary: "bg-gray-600 text-white hover:bg-gray-700",
      transparent: "text-slate-600 border-slate-600 hover:bg-gray-100",
      secondary: "",
      text: "text-gray-600",
    },
    white: {
      primary: "bg-beige text-primary-dark hover:bg-gray-200",
      transparent: "text-white border-white",
      secondary: "",
      text: "text-gray-600",
    },
  };

  const sizeStyles = {
    base: `py-[10px]
     px-[24px]
     rounded-[8px]
     line-height-[28px]
     font-[300]
     sm:min-w-[0px]
     w-auto
     sm:text-[14px] 
     sm:leading-[12px] 
     sm:px-[32px] 
     sm:py-[12px]`,
    sm: "py-[6px] px-[16px] rounded-[6px] text-sm line-height-[20px]",
    md: "py-[12px] px-[24px] rounded-[6px] text-base line-height-[24px]",
  };

  return (
    <button
      onClick={onClick}
      disabled={disabled || loading}
      type={type}
      className={twMerge(
        "flex items-center justify-center gap-[8px] font-onest text-[16px] leading-[20px] disabled:opacity-50",
        sizeStyles[size],
        colorStyles[color][variant],
        className,
      )}
    >
      {typeof iconLeft === "string" ? (
        <Image
          width={24}
          height={24}
          src={iconLeft}
          alt="Picture of the author"
        />
      ) : (
        iconLeft
      )}
      <span className="">{loading ? "Loading..." : children}</span>
      {typeof iconRight === "string" ? (
        <Image
          width={24}
          height={24}
          src={iconRight}
          alt="Picture of the author"
        />
      ) : (
        iconRight
      )}
    </button>
  );
};

export default Button;
