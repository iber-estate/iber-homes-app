import React from "react";

export const Skeleton = () => {
  return (
    <div
      role="status"
      className="md:space-y-0 md:space-x-8 md:flex md:items-center animate-pulse space-y-8 rounded-[8px] rtl:space-x-reverse"
    >
      <div className="dark:bg-gray-700 flex h-48 h-[220px] w-full items-center justify-center rounded bg-lightblueone sm:w-96">
        <svg
          className="text-gray-200 dark:text-gray-600 h-10 w-10"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="currentColor"
          viewBox="0 0 20 18"
        >
          <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z" />
        </svg>
      </div>
      <div className="flex w-full flex-col gap-2">
        <div className="bg-gray-200 dark:bg-gray-700 mb-[4px] h-[20px] w-full rounded-full bg-lightblueone"></div>
        <div className="bg-gray-200 dark:bg-gray-700 mb-[4px] h-[20px] w-full rounded-full bg-lightblueone"></div>
        <div className="bg-gray-200 dark:bg-gray-700 mb-[8px] h-[12px] w-[75px] rounded-full bg-lightblueone"></div>
        <div className="bg-gray-200 dark:bg-gray-700 mb-[4px] h-[12px] w-[120px] rounded-full bg-lightblueone"></div>
      </div>
    </div>
  );
};
