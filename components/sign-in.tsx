"use client";
import { useState } from "react";
import { logout, sign } from "@/actions";
import { useRouter } from "next/navigation";
import { FormProvider, useForm } from "react-hook-form";
import TextInput from "@/components/FormComponents/TextInput";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import Link from "next/link";

export function SignIn() {
  const methods = useForm();
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  return (
    <FormProvider {...methods}>
      <form
        className="mx-auto mt-[100px] w-[552px] rounded-[8px] border border-primary-light px-[110px] pb-[40px] pt-[68px]"
        onSubmit={methods.handleSubmit(async () => {})}
      >
        <Typography type="subtitle" className="mb-[28px] text-center">
          Log in
        </Typography>
        <TextInput
          name="email"
          placeholder="E-mail"
          label="Email"
          className="mb-[32px]"
        />
        <Button className="mb-[8px] w-full">Continue</Button>
        <Typography type="caption">
          By creating an account you agree to{" "}
          <Link href="" className="font-[500] text-primary-dark">
            Terms of Use
          </Link>{" "}
          and{" "}
          <Link href="" className="font-[500] text-primary-dark">
            Privacy Policy
          </Link>
        </Typography>
        <Typography type="caption" className="text-center">
          Don’t have an account?{" "}
          <Link href="" className="font-[500] text-primary-dark">
            Sign up
          </Link>
        </Typography>
        {/*<label>*/}
        {/*  Email*/}
        {/*  <input*/}
        {/*    name="email"*/}
        {/*    type="email"*/}
        {/*    onChange={(e) => setEmail(e.target.value)}*/}
        {/*    value={email}*/}
        {/*  />*/}
        {/*</label>*/}
        {/*<label>*/}
        {/*  Password*/}
        {/*  <input*/}
        {/*    name="password"*/}
        {/*    type="password"*/}
        {/*    onChange={(e) => setPassword(e.target.value)}*/}
        {/*    value={password}*/}
        {/*  />*/}
        {/*</label>*/}
        {/*<button*/}
        {/*  type="button"*/}
        {/*  onClick={async () => {*/}
        {/*    try {*/}
        {/*      await sign(email, password);*/}
        {/*      router.push("/");*/}
        {/*    } catch (e) {}*/}
        {/*  }}*/}
        {/*>*/}
        {/*  Sign In*/}
        {/*</button>*/}
        {/*<button*/}
        {/*  type="button"*/}
        {/*  onClick={() => {*/}
        {/*    logout();*/}
        {/*  }}*/}
        {/*>*/}
        {/*  Logout*/}
        {/*</button>*/}
      </form>
    </FormProvider>
  );
}
