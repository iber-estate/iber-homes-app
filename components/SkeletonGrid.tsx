import React from "react";

const SkeletonCard = () => (
  <div className="flex flex-col gap-[4px]">
    <div className="bg-gray-200 dark:bg-gray-700 h-[250px] w-full animate-pulse rounded-[8px] bg-lightblueone"></div>
    <div className="bg-gray-200 dark:bg-gray-700 h-[30px] w-full animate-pulse rounded-[8px] bg-lightblueone"></div>
    <div className="bg-gray-200 dark:bg-gray-700 h-[30px] w-full animate-pulse rounded-[8px] bg-lightblueone"></div>
    <div className="bg-gray-200 dark:bg-gray-700 h-[30px] w-[100px] animate-pulse rounded-[8px] bg-lightblueone"></div>
  </div>
);

const SkeletonGrid = () => {
  return (
    <div className="mb-[76px] grid grid-cols-3 gap-x-[28px] gap-y-[40px] sm:grid-cols-1">
      {Array.from(Array(9).keys()).map((item) => (
        <SkeletonCard key={item} />
      ))}
    </div>
  );
};

export default SkeletonGrid;
