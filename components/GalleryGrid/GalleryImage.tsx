import React from "react";
import Image from "next/image";

const GalleryImage = ({ src }: { src: string }) => (
  <div className="bg-gray-300 h-[136px] overflow-hidden">
    {src ? (
      <Image
        className={"aspect-[241/136] h-[136px] w-full rounded object-cover"}
        src={src}
        width={241}
        height={136}
        alt="hero-picture"
      />
    ) : (
      <div
        className={
          "flex aspect-[241/136] h-[136px] w-full items-center justify-center rounded bg-[#ccc] bg-lightbluethree object-cover"
        }
      >
        <Image src="/icons/noPhoto.svg" alt="no image" width={48} height={48} />
      </div>
    )}
  </div>
);
// <div className="flex h-[288px] w-full items-center justify-center "></div>;

export default GalleryImage;
