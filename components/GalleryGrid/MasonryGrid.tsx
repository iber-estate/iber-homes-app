import React from "react";
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import cross from "@/public/icons/cross.svg";
import arrowLeft from "@/public/icons/arrowLeft.svg";
import Image from "next/image";
import { Typography } from "@/components/Typography";

interface MasonryGridProps {
  className?: string;
  pictures: any;
  openLightbox: (index: number) => void;
  toggleGrid: () => void;
}

const MasonryGrid = (props: MasonryGridProps) => {
  const { toggleGrid, openLightbox, pictures = [] } = props;
  return (
    <div className="wrapper default-grid">
      <div className="col-span-6 flex justify-between pb-[40px] pt-[28px]">
        <button onClick={toggleGrid} className="flex items-center gap-[4px]">
          <Image src={arrowLeft} alt="cross" />
          <Typography type="body">Back to card</Typography>
        </button>
        <button onClick={toggleGrid}>
          <Image src={cross} alt="cross" />
        </button>
      </div>
      <div className="col-span-4 col-start-2 pb-[28px]">
        <Image
          src={pictures[0]}
          alt=""
          className="mb-[28px] h-[380px] w-full object-cover"
          onClick={() => openLightbox(0)}
          width={780}
          height={380}
        />
        <ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 750: 2, 900: 3 }}>
          <Masonry gutter="28px">
            {pictures.slice(1).map((src: string, index: number) => (
              <Image
                width={240}
                height={140}
                alt={""}
                src={src}
                key={src}
                onClick={() => openLightbox(index + 1)}
              />
            ))}
          </Masonry>
        </ResponsiveMasonry>
      </div>
    </div>
  );
};

export default MasonryGrid;
