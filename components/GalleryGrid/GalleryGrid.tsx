"use client";
import React from "react";
import Button from "@/components/Button";
import Image from "next/image";
import Lightbox from "yet-another-react-lightbox";
import MasonryGrid from "./MasonryGrid";
import GalleryImage from "./GalleryImage";

interface GalleryGridProps {
  className?: string;
  pictures: string[];
  cropElems?: boolean;
}

export const GalleryGrid = (props: GalleryGridProps) => {
  const [lightboxOpen, setLightboxOpen] = React.useState(false);
  const [gridOpen, setGridOpen] = React.useState(false);
  const [index, setIndex] = React.useState(0);
  const { className, cropElems } = props;

  const openLightboxWithIndex = (index: number) => {
    setLightboxOpen(true);
    setIndex(index);
  };

  const toggleGrid = () => {
    setGridOpen((prev) => !prev);
  };

  const pictures = props.pictures;
  return (
    <div
      className={`${className} grid h-[288px] w-full grid-cols-12 gap-[16px] overflow-hidden rounded`}
    >
      <div
        className={`bg-gray-300 relative ${cropElems ? "col-span-8" : "col-span-5"} overflow-hidden`}
      >
        <button
          onClick={() => setLightboxOpen(true)}
          className="w-full overflow-hidden rounded bg-[#ccc]"
        >
          {pictures.length && pictures[0] ? (
            <Image
              className="aspect-[498/288] h-[288px] w-full rounded object-cover"
              src={pictures[0]}
              width={670}
              height={500}
              alt="hero-picture"
            />
          ) : (
            <div className="flex h-[288px] w-full items-center justify-center bg-lightbluethree">
              <Image
                src="/icons/noPhoto.svg"
                alt="no image"
                width={48}
                height={48}
              />
            </div>
          )}
        </button>
      </div>
      <div
        className={`relative ${cropElems ? "col-span-4" : "col-span-7"}  grid ${cropElems ? "grid-cols-1" : "grid-cols-2"} gap-x-[16px] gap-y-[16px]`}
      >
        <button onClick={() => setLightboxOpen(true)} key={index}>
          <GalleryImage src={pictures?.length ? pictures[1] : ""} />
        </button>
        <button onClick={() => setLightboxOpen(true)} key={index}>
          <GalleryImage src={pictures?.length ? pictures[2] : ""} />
        </button>
        {!cropElems && (
          <>
            <button onClick={() => setLightboxOpen(true)} key={index}>
              <GalleryImage src={pictures?.length ? pictures[3] : ""} />
            </button>
            <button onClick={() => setLightboxOpen(true)} key={index}>
              <GalleryImage src={pictures?.length ? pictures[4] : ""} />
            </button>
          </>
        )}
        <Button
          variant="secondary"
          className="absolute bottom-[12px] right-[12px]"
          onClick={toggleGrid}
        >
          Show all pictures
        </Button>
      </div>
      {gridOpen && (
        <div className="fixed left-0 top-0 z-10 h-[100vh] w-[100vw] overflow-y-scroll bg-white">
          <MasonryGrid
            pictures={pictures}
            openLightbox={openLightboxWithIndex}
            toggleGrid={toggleGrid}
          />
        </div>
      )}
      <Lightbox
        className="z-50"
        open={lightboxOpen}
        close={() => setLightboxOpen(false)}
        slides={pictures?.map((src: string) => ({ src }))}
        index={index}
      />
    </div>
  );
};
