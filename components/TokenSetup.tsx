"use client";
import React, { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import { OpenAPI, UserService } from "@/generated";
import { ToastContainer } from "react-toastify";
import { logout } from "@/actions";

const TokenSetup = ({ children }: any) => {
  const session = useSession();
  const [tokenSettled, setTokenSettled] = useState<string | null | undefined>(
    null,
  );

  useEffect(() => {
    console.log(session);
    OpenAPI.TOKEN = session.data?.user?.token;
    setTokenSettled(session.data?.user?.token);
  }, [session]);

  useEffect(() => {
    if (session.status === "loading") return;
    // @ts-ignore
    OpenAPI.TOKEN = session.data?.user?.token;
    UserService.userMy({}).catch((reason) => {
      if (reason.status === 401) {
        logout();
      }
    });
  }, [tokenSettled]);

  if (session.status !== "unauthenticated" && !tokenSettled) return null;
  return (
    <div className="flex min-h-[100vh] flex-col">
      <ToastContainer />
      {children}
    </div>
  );
};

export default TokenSetup;
