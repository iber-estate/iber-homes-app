"use client";
import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";
import arrow from "@/public/icons/arrowLeft.svg";
import { Typography } from "@/components/Typography";
import { twMerge } from "tailwind-merge";

interface Props {
  children: string | React.ReactNode;
  maxHeight: number;
  expandText?: string;
  className?: string;
}

export const CollapseContainer: React.FC<Props> = ({
  children,
  expandText = "Show more",
  className,
  maxHeight = 110,
}) => {
  const [expanded, setExpanded] = useState(false);
  const [showToggle, setShowToggle] = useState(false);
  const contentRef = useRef<HTMLDivElement>(null);

  const toggleExpand = () => {
    if (contentRef.current) {
      if (!expanded) {
        const contentHeight = contentRef.current.scrollHeight;
        contentRef.current.style.maxHeight = `${contentHeight}px`;
      } else {
        contentRef.current.style.maxHeight = `${maxHeight}px`;
      }
    }
    setExpanded(!expanded);
  };

  useEffect(() => {
    const contentHeight = contentRef.current?.scrollHeight;
    if (!contentHeight) return;
    if (contentHeight > maxHeight) {
      setShowToggle(true);
      contentRef.current.style.maxHeight = `${maxHeight}px`; // Initial max height
    } else {
      setShowToggle(false);
    }
  }, [children, maxHeight]);

  return (
    <div className={`relative ${className}`}>
      <div
        ref={contentRef}
        className="mb-[16px] overflow-hidden transition-[max-height] duration-500 ease-in-out"
      >
        <div>{children}</div>
      </div>
      {showToggle && (
        <div>
          {!expanded ? (
            <button
              className="flex items-center gap-2 rounded-sm text-base font-medium"
              onClick={toggleExpand}
            >
              <Typography type="subtitle3" className="mb-0">
                {expandText}
              </Typography>
              <Image src={arrow} alt="arrow-down" className="-rotate-90" />
            </button>
          ) : (
            <button
              className="flex items-center gap-2 rounded-sm text-base font-medium"
              onClick={toggleExpand}
            >
              <Typography type="subtitle3" className="mb-0">
                Show less
              </Typography>
              <Image src={arrow} alt="arrow-up" className="rotate-90" />
            </button>
          )}
        </div>
      )}
    </div>
  );
};
