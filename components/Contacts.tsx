"use client";
import React, { useState } from "react";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import { DeveloperList, PropertyComplexList } from "@/generated";
import { useStore } from "@/store/useStore";
import { useSession } from "next-auth/react";
import Link from "next/link";

type TContactsProps = {
  developer: DeveloperList;
};

const Contacts = (props: TContactsProps) => {
  const { developer } = props;
  const [isContactsOpen, setIsContactsOpen] = useState(false);
  const { setStore } = useStore();
  const session = useSession();
  const openAuth = () => {
    setStore({ isAuthModalOpen: true });
  };
  return (
    <div className="w-[498px] rounded-[8px] bg-lightbluethree p-[20px] sm:w-full">
      <Typography type="h6" className="w-full">
        Developer
      </Typography>
      <div className="mb-[16px] flex flex-col gap-[8px]">
        {isContactsOpen && (
          <>
            <Typography type="body">Name: {developer.name}</Typography>
            {developer.phone && (
              <Typography type="body">Phone: {developer.phone}</Typography>
            )}
            {developer.email && (
              <Typography type="body">Email: {developer.email}</Typography>
            )}
            {/*<Typography type="body">*/}
            {/*  About developer:{complex.developer}*/}
            {/*</Typography>*/}
          </>
        )}
      </div>
      <div className="flex gap-[16px] sm:flex-col">
        {!isContactsOpen && (
          <Button
            variant="secondary"
            onClick={() => {
              if (!session.data?.user) {
                openAuth();
                return;
              }
              setIsContactsOpen(true);
            }}
          >
            Get contacts
          </Button>
        )}
        {developer.wa_number && (
          <Link
            href={`https://wa.me/${developer.wa_number}`}
            className="sm:w-full"
          >
            <Button
              className="sm:w-full sm:py-[7.5px]"
              iconLeft="/icons/whatsapp.svg"
            >
              Sales department
            </Button>
          </Link>
        )}
      </div>
    </div>
  );
};

export default Contacts;
