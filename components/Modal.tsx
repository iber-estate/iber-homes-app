"use client";
import React from "react";

type ModalProps = {
  children: React.ReactNode;
  isOpen: boolean;
  onClose: () => void;
};

const Modal = (props: ModalProps) => {
  const { isOpen, onClose, children } = props;
  return (
    <div
      className={`fixed left-0 top-0 z-[100] h-[100vh] w-[100vw] ${!isOpen && "hidden"}`}
    >
      <div
        className="absolute left-0 top-0 h-full w-full cursor-pointer bg-slate-800 opacity-[0.5]"
        onClick={onClose}
      />
      <div className="absolute left-1/2 top-[80px] w-full -translate-x-1/2 sm:top-[20px]">
        {isOpen && children}
      </div>
    </div>
  );
};

export default Modal;
