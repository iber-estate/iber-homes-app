import React, { useState } from "react";
import { Typography } from "@/components/Typography";

interface Tab {
  title: string;
  content: JSX.Element;
}

interface TabsProps {
  tabs: Tab[];
  defaultActive?: number;
  onChange?: (index: number) => void;
  rightElement?: React.ReactNode;
}

export const Tabs = (props: TabsProps) => {
  const { tabs, defaultActive = 0, onChange, rightElement } = props;
  const [activeTab, setActiveTab] = useState<number>(defaultActive);

  return (
    <div className="w-full">
      <div className="mb-[32px] flex items-center gap-x-[40px] sm:mb-[12px] sm:flex-wrap sm:gap-x-[24px] sm:gap-y-[16px]">
        {tabs.map((tab, index) => (
          <button
            key={index}
            className={`border-b-[3px] py-[8px] text-center text-sm font-medium sm:py-[4px] ${
              activeTab === index
                ? "border-primary-dark"
                : "text-gray-600 hover:text-blue-500 border-transparent"
            }`}
            onClick={() => {
              setActiveTab(index);
              onChange && onChange(index);
            }}
          >
            <Typography type="h5" className="mb-0 text-primary-dark sm:hidden">
              {tab.title}
            </Typography>
            <Typography
              type="h3"
              className="mb-0 hidden text-primary-dark sm:block"
            >
              {tab.title}
            </Typography>
          </button>
        ))}
        {rightElement && (
          <div className="ml-auto sm:mb-[12px] sm:w-full">{rightElement}</div>
        )}
      </div>
      <div>{tabs[activeTab] ? tabs[activeTab].content : "No content"}</div>
    </div>
  );
};
