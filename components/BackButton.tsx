"use client";
import React from "react";
import { Typography } from "@/components/Typography";
import Image from "next/image";
import { useRouter } from "next/navigation";

export const BackButton = ({ label }: { label: string }) => {
  const router = useRouter();
  return (
    <button
      onClick={() => {
        router.back();
      }}
      className="mb-[24px] flex gap-[4px] sm:items-center"
    >
      <Image src="/icons/arrowLeft.svg" alt="left" width={24} height={24} />
      <Typography type="body">{label}</Typography>
    </button>
  );
};
