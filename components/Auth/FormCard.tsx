"use client";
import { FormProvider, useForm } from "react-hook-form";
import { ArrowIcon, CrossIcon } from "@/components/Icons";
import { useRouter } from "next/navigation";
import { twMerge } from "tailwind-merge";
import { Typography } from "@/components/Typography";

type FormCardProps = {
  children: React.ReactNode;
  onSubmit: (methods?: any, data?: any) => Promise<void>;
  title?: string;
  defaultValues?: any;
  onError?: (error: any) => void;
  back?: () => void;
  onClose?: () => void;
  className?: string;
};
// TODO: split form logic and view
export function FormCard(props: FormCardProps) {
  const { onSubmit, back, className, defaultValues = {}, title } = props;
  const methods = useForm({
    defaultValues,
  });
  const router = useRouter();

  const closeForm = () => {
    if (props.onClose) {
      props.onClose();
    } else {
      // router.push("/");
    }
  };

  return (
    <FormProvider {...methods}>
      <form
        className={twMerge(
          `relative mx-auto flex w-[400px] flex-col rounded-[8px] border border-primary-light bg-beige px-[24px] pb-[40px] pt-[72px] sm:m-auto sm:mx-[20px] sm:h-[464px] sm:w-[calc(100%-40px)] sm:px-[20px] sm:py-[48px] sm:pb-[28px]`,
          className,
        )}
        onSubmit={methods.handleSubmit((data) => onSubmit(methods, data))}
      >
        <header className="absolute left-0 top-0 flex w-full justify-between">
          <button
            type="button"
            className={`p-[24px] ${back ? "" : "invisible"} sm:p-[20px]`}
            onClick={() => {
              back && back();
            }}
          >
            <ArrowIcon
              w="26px"
              h="26px"
              color="#999A9F"
              className="rotate-180"
            />
          </button>
          <button
            type="button"
            className="p-[24px] sm:p-[20px]"
            onClick={closeForm}
          >
            <CrossIcon color="#999A9F" />
          </button>
        </header>
        {title && (
          <Typography type="subtitle" className="mb-[28px] sm:hidden">
            {title}
          </Typography>
        )}
        {title && (
          <Typography
            type="h3"
            className="mb-[28px] hidden sm:mb-[24px] sm:block sm:text-center"
          >
            {title}
          </Typography>
        )}
        {props.children}
      </form>
    </FormProvider>
  );
}
