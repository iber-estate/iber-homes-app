"use client";

import TextInput from "@/components/FormComponents/TextInput";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import Link from "next/link";
import { UserService } from "@/generated";
import { useMutation } from "@tanstack/react-query";
import { FormCard } from "@/components/Auth/FormCard";
import { toast } from "react-toastify";
import { ApiResult } from "@/generated/core/ApiResult";
import { parseErrorMessage } from "@/utils/errors";
import { stepBaseProps } from "@/components/Auth/Auth";

type SignInProps = {
  setEmail: (email: string) => void;
} & stepBaseProps;

export function Signin(props: SignInProps) {
  const { setStep, setEmail, onClose } = props;

  const { mutate: sendOTP, isPending } = useMutation({
    mutationFn: UserService.userSendOtp,
    onSuccess: () => {
      toast.success("Code sent");
      setStep("code");
    },
    onError: (error: ApiResult) => {
      toast.error(parseErrorMessage(error));
    },
  });

  return (
    <FormCard
      title="Log in"
      className="h-[552px]"
      onSubmit={async (methods) => {
        const { email } = methods.getValues();
        setEmail(email);
        sendOTP({ data: { email } });
      }}
      onClose={onClose}
    >
      <TextInput
        name="email"
        placeholder="E-mail"
        label="Email"
        className="mb-[32px]"
      />
      <Button className="mb-[8px] w-full">
        {isPending ? "Sending..." : "Continue"}
      </Button>
      <Typography type="caption" className="grow">
        By creating an account you agree to{" "}
        <Link href="" className="font-[500] text-primary-dark">
          Terms of Use
        </Link>{" "}
        and{" "}
        <Link href="" className="font-[500] text-primary-dark">
          Privacy Policy
        </Link>
      </Typography>
      <Typography type="caption" className="text-center">
        Don’t have an account?{" "}
        <button
          className="font-[500] text-primary-dark"
          onClick={() => {
            setStep("signup");
          }}
        >
          Sign up
        </button>
      </Typography>
    </FormCard>
  );
}
