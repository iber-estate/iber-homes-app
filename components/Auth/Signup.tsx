"use client";

import TextInput from "@/components/FormComponents/TextInput";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import Link from "next/link";
import { UserService } from "@/generated";
import { useMutation } from "@tanstack/react-query";
import { FormCard } from "@/components/Auth/FormCard";
import { toast } from "react-toastify";
import { ApiResult } from "@/generated/core/ApiResult";
import { parseErrorMessage } from "@/utils/errors";
import { stepBaseProps } from "@/components/Auth/Auth";

type SignInProps = {
  setStep: (step: "signup" | "code" | "signin") => void;
  setEmail: (email: string) => void;
  setName: (name: string) => void;
} & stepBaseProps;

export function Signup(props: SignInProps) {
  const { setStep, setEmail, setName, onClose } = props;

  const { mutate: createUser, isPending } = useMutation({
    mutationFn: UserService.userCreate,
    onSuccess: () => {
      toast.success("Code sent");
      setStep("code");
    },
    onError: (error: ApiResult) => {
      toast.error(parseErrorMessage(error));
    },
  });

  return (
    <FormCard
      title="Sign up"
      className="h-[552px]"
      onSubmit={async (methods) => {
        const { email, name } = methods.getValues();
        setEmail(email);
        setName(name);
        createUser({ data: { email, name } });
      }}
      onClose={onClose}
    >
      <TextInput
        name="name"
        placeholder="Name"
        label="Full name"
        className="mb-[28px] sm:mb-[20px]"
      />
      <TextInput
        name="email"
        placeholder="Email"
        label="Email"
        className="mb-[32px] sm:mb-[28px]"
      />
      <Button className="mb-[8px] w-full">
        {isPending ? "Sending..." : "Continue"}
      </Button>
      <Typography type="caption" className="text-red grow">
        By creating an account you agree to{" "}
        <Link href="" className="font-[500] text-primary-dark">
          Terms of Use
        </Link>{" "}
        and{" "}
        <Link href="" className="font-[500] text-primary-dark">
          Privacy Policy
        </Link>
      </Typography>
      <Typography type="caption" className="text-center">
        Already have an account?{" "}
        <button
          className="font-[500] text-primary-dark"
          onClick={() => {
            setStep("signin");
          }}
        >
          Log in
        </button>
      </Typography>
    </FormCard>
  );
}
