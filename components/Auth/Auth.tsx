"use client";
import React, { useState } from "react";
import { Signin } from "@/components/Auth/Signin";
import { CodeVerification } from "@/components/Auth/CodeVerification";
import { ToastContainer } from "react-toastify";
import { Signup } from "@/components/Auth/Signup";
import { steps } from "@/types";

export type stepBaseProps = {
  setStep: (step: steps) => void;
  onClose?: () => void;
};

type AuthProps = {
  initialState: steps;
  onClose?: () => void;
};
export const Auth = (props: AuthProps) => {
  const { initialState, onClose } = props;
  const [step, setStep] = useState<steps>(initialState);
  const [email, setEmail] = useState<string>("");
  const [name, setName] = useState<string>("");

  const renderStep = (step: steps) => {
    switch (step) {
      case "signup":
        return (
          <Signup
            setStep={setStep}
            setEmail={setEmail}
            setName={setName}
            onClose={onClose}
          />
        );
      case "code":
        return (
          <CodeVerification
            email={email}
            name={name}
            setStep={setStep}
            onClose={onClose}
            back={() => {
              setStep("signin");
            }}
          />
        );
      case "signin":
        return (
          <Signin setStep={setStep} setEmail={setEmail} onClose={onClose} />
        );
    }
  };

  return <>{renderStep(step)}</>;
};
