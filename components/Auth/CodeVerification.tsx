"use client";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import VerificationInput from "react-verification-input";
import { FormCard } from "@/components/Auth/FormCard";
import { useState } from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { OpenAPI, UserService } from "@/generated";
import { toast } from "react-toastify";
import { parseErrorMessage } from "@/utils/errors";
import { sign } from "@/actions";
import { stepBaseProps } from "@/components/Auth/Auth";
import { queryKeys } from "@/types";
import { useStore } from "@/store/useStore";
import { useLike } from "@/services/hooks/useLike";

type CodeVerificationProps = {
  back: () => void;
  email: string;
  name: string;
} & stepBaseProps;

export function CodeVerification(props: CodeVerificationProps) {
  const { data: store } = useStore();
  const { back, email, name, onClose } = props;
  const [code, setCode] = useState<string | null>(null);

  const { save } = useLike({ invalidateProps: true });

  const { mutate: login, isPending } = useMutation({
    mutationFn: UserService.userLogin,
    onError: (error: any) => {
      toast.error(parseErrorMessage(error));
    },
    onSuccess: async (data) => {
      OpenAPI.TOKEN = data.access;
      const user = await UserService.userMy({});
      // await queryClient.invalidateQueries({ queryKey: [queryKeys.properties] });
      if (store.saveBeforeAuth) {
        await save({ data: { real_property: store.saveBeforeAuth } });
      }
      window.localStorage.setItem("locationBeforeAuth", window.location.href);
      await sign({
        token: data.access,
        ...user,
        location: window.location.href,
      });
    },
  });

  const { mutate: sendOTP, isPending: resendOTPPending } = useMutation({
    mutationFn: UserService.userSendOtp,
    onSuccess: () => {
      toast.success("Code sent");
    },
    onError: (error: any) => {
      toast.error(parseErrorMessage(error));
    },
  });

  const resendCode = async () => {
    sendOTP({ data: { email } });
  };

  return (
    <FormCard
      onSubmit={async () => {
        if (!code) return;
        login({ data: { email, password: code } });
      }}
      onClose={onClose}
      back={back}
    >
      <div className="mb-[32px] flex flex-col justify-center">
        <Typography type="caption" className="mb-[24px] text-center">
          Check your e-mail {email} There should be a message with a code from
          us, enter it here.
        </Typography>
        <VerificationInput
          placeholder="_"
          removeDefaultStyles={true}
          autoFocus={true}
          onChange={(value) => setCode(value)}
          classNames={{
            container: "w-full flex gap-[11px]",
            character:
              "w-[44px] h-[56px] grow px-[12px] py-[12px] border rounded-[8px] text-[20px] leading-[24px] font-[200] shadow",
            characterInactive: "border-stroke",
            characterSelected: "border-[#1B5C6B]",
          }}
        />
      </div>
      <Button className="mb-[60px] w-full" disabled={isPending}>
        {isPending || resendOTPPending ? "Sending..." : "Log in"}
      </Button>
      <Typography type="caption" className="text-center">
        Didn&lsquo;t get the code?{" "}
        <button className="font-[500] text-primary-dark" onClick={resendCode}>
          Send it again
        </button>
      </Typography>
    </FormCard>
  );
}
