import React, { useEffect } from "react";
import { ChooseButtons } from "@/components/FormComponents/ChooseButtons";
import { useFormContext } from "react-hook-form";
import { FieldsNames, FilterType } from "@/app/(main)/search/types";
import { getPagesOptions } from "@/app/(main)/search/utils";
import FormButton from "@/components/FormComponents/FormButton";
import Image from "next/image";
import ReactPaginate from "react-paginate";

type PaginationProps = {
  applyFilters?: (data: FilterType, isPagination?: boolean) => void;
  totalPages: number;
  disabled?: boolean;
};
const Arrow = ({
  invert,
  disabled,
}: {
  invert?: boolean;
  disabled?: boolean;
}) => {
  return (
    <Image
      src="/icons/paginationArrow.svg"
      alt="arrow"
      width={24}
      height={24}
      className={`${invert ? "" : "rotate-180"} ${disabled ? "opacity-[0.3]" : ""}`}
    />
  );
};
const Pagination = ({
  applyFilters = () => {},
  totalPages,
  disabled,
}: PaginationProps) => {
  const { getValues, watch, setValue } = useFormContext();
  const page = watch(FieldsNames.PAGE);
  const values = getValues() as FilterType;
  const itemClass =
    "transparent inline-flex h-[36px] min-w-[36px] cursor-pointer items-center justify-center rounded-[4px] border border-lightbluethree px-[6px] py-[8px] text-black text-[16px] leading-[24px] font-[300] sm:text-[0.8125rem] sm:leading-[1rem]";
  const activeItem =
    "text-white bg-primary inline-flex h-[36px] min-w-[36px] cursor-pointer items-center justify-center rounded-[4px] border border-transparent px-[6px] py-[8px]";

  const next = () => {
    const nextPage = parseInt(page) + 1;
    if (nextPage > totalPages) return;
    setValue(FieldsNames.PAGE, nextPage.toString());
    applyFilters({ ...values, page: nextPage.toString() }, true);
  };

  const prev = () => {
    const prevPage = parseInt(page) - 1;
    if (prevPage < 1) return;
    setValue(FieldsNames.PAGE, prevPage.toString());
    applyFilters({ ...values, page: prevPage.toString() }, true);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [page]);

  return (
    <div className="mb-[80px] flex w-full justify-center gap-[8px]">
      {/*<FormButton*/}
      {/*  label={<Arrow disabled={page == 1} />}*/}
      {/*  isSelected={false}*/}
      {/*  onClick={prev}*/}
      {/*  size="small"*/}
      {/*/>*/}
      <div>
        <ReactPaginate
          containerClassName="flex gap-[8px]"
          pageClassName={""}
          pageLinkClassName={itemClass}
          previousClassName={itemClass}
          nextLinkClassName={itemClass}
          breakLabel="..."
          nextLabel=">"
          onPageChange={(e) => {
            const oldFormValues = getValues() as FilterType;
            console.log(oldFormValues);
            const newFormValues = {
              ...oldFormValues,
              page: `${e.selected + 1}`,
            };
            applyFilters(newFormValues, true);
          }}
          pageRangeDisplayed={3}
          marginPagesDisplayed={1}
          pageCount={totalPages}
          activeLinkClassName={activeItem}
          previousLabel="<"
          renderOnZeroPageCount={null}
        />
      </div>
      {/*<ChooseButtons*/}
      {/*  activeColor="bg-primary"*/}
      {/*  invertActiveTextColor*/}
      {/*  type="radio"*/}
      {/*  size="small"*/}
      {/*  name="page"*/}
      {/*  disabled={disabled}*/}
      {/*  onChange={(e) => {*/}
      {/*    const oldFormValues = getValues() as FilterType;*/}
      {/*    const newFormValues = {*/}
      {/*      ...oldFormValues,*/}
      {/*      page: e.target.value,*/}
      {/*    };*/}
      {/*    applyFilters(newFormValues, true);*/}
      {/*  }}*/}
      {/*  options={getPagesOptions(totalPages, page)}*/}
      {/*/>*/}
      {/*<FormButton*/}
      {/*  label={<Arrow invert disabled={page == totalPages} />}*/}
      {/*  isSelected={false}*/}
      {/*  onClick={next}*/}
      {/*  size="small"*/}
      {/*/>*/}
    </div>
  );
};

export default Pagination;
