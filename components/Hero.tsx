"use client";
import React, { useState } from "react";
import { Typography } from "@/components/Typography";
import Suggests from "@/app/(main)/Suggests";
import { FormProvider, useForm } from "react-hook-form";
import { useRouter } from "next/navigation";

// TODO: Move component from app folder
const Hero = () => {
  const router = useRouter();
  const [searchFocused, setSearchFocused] = useState(false);
  const onSubmit = (data: any) => {
    setSearchFocused(false);
    router.replace(`/search?address=${data.address}`);
  };
  const methods = useForm();

  return (
    <div
      className="w-[1280px] bg-cover bg-center bg-no-repeat sm:w-full desktop:w-full"
      style={{
        backgroundImage: "url(/images/hero.jpeg)",
      }}
    >
      <div>
        <div className="wrapper py-[122px] sm:py-[78px]">
          <Typography
            className="mb-[28px] w-[575px] text-white sm:w-full"
            type="h1"
          >
            Complexes and villas from developers by the sea
          </Typography>
          <FormProvider {...methods}>
            <form onSubmit={methods.handleSubmit(onSubmit)}>
              <Suggests
                searchFocused={searchFocused}
                setSearchFocused={setSearchFocused}
                methods={methods}
              />
            </form>
          </FormProvider>
        </div>
      </div>
    </div>
  );
};

export default Hero;
