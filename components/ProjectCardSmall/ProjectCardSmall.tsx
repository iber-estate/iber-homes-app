import React from "react";
import Image from "next/image";
import { Typography } from "@/components/Typography";
import Link from "next/link";

type CardProps = {
  title: string;
  location: string;
  price: number;
  date: string;
  type: "sale of business" | "investment project";
  image: string;
  link: string;
};

export const ProjectCardSmall: React.FC<CardProps> = ({
  title,
  location,
  price,
  date,
  type,
  image,
  link,
}) => {
  return (
    <div className="relative h-[376px] w-full rounded-[16px] bg-lightbluethree sm:h-[280px] sm:min-w-[280px] sm:bg-transparent">
      <Link href={link}>
        <Image
          src={image}
          alt={title}
          width={333}
          height={593}
          className="mb-[16px] h-[220px] w-full rounded-t-[16px] object-cover sm:h-[180px] sm:rounded-[8px]"
        />
        <div className="absolute right-[16px] top-[16px] flex space-x-2">
          <span className="text-gray-800 inline-block rounded bg-primary-light p-[8px]">
            <Typography type="body">{type}</Typography>
          </span>
          <span className="rounded bg-white p-[8px]">
            <Typography type="body"> {date}</Typography>
          </span>
        </div>
        <div className="px-[16px] sm:p-0">
          <Typography
            type="h6"
            className="mb-[4px] truncate whitespace-nowrap text-primary-dark sm:text-[16px] sm:leading-[20px]"
          >
            {title || "Invest project"}
          </Typography>
          <Typography
            type="body"
            className="text-gray-600 mb-4 mb-[12px] max-w-[300px] truncate text-sm sm:text-[14px]"
          >
            {location}
          </Typography>
          <Typography type="h4" className="">
            ${price.toLocaleString()}
          </Typography>
        </div>
      </Link>
    </div>
  );
};
