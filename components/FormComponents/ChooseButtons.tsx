import React from "react";
import { useFormContext } from "react-hook-form";
import { Typography } from "@/components/Typography";
import { twMerge } from "tailwind-merge";

type ChooseButtonsProps = {
  name: string;
  size?: "small" | "big";
  type?: "checkbox" | "radio";
  options: { value: string; label: string }[];
  className?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  disabled?: boolean;
  activeColor?: string;
  invertActiveTextColor?: boolean;
};

type ButtonProps = {
  isSelected: boolean;
  label: string;
  activeColor?: string;
  invertActiveTextColor?: boolean;
};

const BigButton = (props: ButtonProps) => {
  const { isSelected, label } = props;
  return (
    <button
      className={`${isSelected ? "bg-primary sm:bg-primary-light" : "bg-lightgray"} cursor-pointer rounded-[8px] px-[16px] py-[12px] sm:py-[14px]`}
    >
      <Typography
        type="body2"
        className={`${isSelected ? "text-white sm:text-primary-dark" : "text-primary-dark"} whitespace-nowrap sm:truncate`}
      >
        {" "}
        {label}
      </Typography>
    </button>
  );
};

const SmallButton = (props: ButtonProps) => {
  const { isSelected, label, activeColor, invertActiveTextColor } = props;
  return (
    <div
      className={`${isSelected ? activeColor || "bg-primary-light" : "transparent"} inline-flex h-[36px] min-w-[36px] cursor-pointer items-center justify-center rounded-[4px] border ${isSelected ? "border-transparent" : "border-lightbluethree"} px-[6px] py-[8px]`}
    >
      <Typography
        type="body2"
        className={
          invertActiveTextColor ? `${isSelected ? "text-white" : ""}` : ""
        }
      >
        {label}
      </Typography>
    </div>
  );
};

// TODO: add keyboard support
export const ChooseButtons = (props: ChooseButtonsProps) => {
  const {
    options,
    name,
    size = "big",
    className,
    onChange,
    disabled,
    type = "checkbox",
    activeColor,
    invertActiveTextColor,
  } = props;
  const { register, watch, setValue } = useFormContext();
  const values = watch(name);
  const checkboxField = register(name);

  const isSelected = (value: string) =>
    values?.length ? values?.includes(value) : false;

  return (
    <div
      className={twMerge(
        `flex flex-wrap ${size === "small" ? "gap-[8px]" : "gap-[20px] sm:gap-[12px]"} sm:flex-nowrap`,
        className,
      )}
    >
      {options.map((option, index) => (
        <label key={option.value} className="inline-flex items-center">
          <input
            {...checkboxField}
            type={type}
            disabled={disabled}
            onChange={(e) => {
              checkboxField.onChange(e);
              onChange && onChange(e);
            }}
            value={option.value}
            className="hidden"
            id={`checkbox-${index}`}
          />
          {size === "small" ? (
            <SmallButton
              activeColor={activeColor}
              invertActiveTextColor={invertActiveTextColor}
              isSelected={isSelected(option.value)}
              label={option.label}
            />
          ) : (
            <BigButton
              isSelected={isSelected(option.value)}
              label={option.label}
            />
          )}
        </label>
      ))}
    </div>
  );
};
