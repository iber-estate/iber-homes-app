import React from "react";
import { twMerge } from "tailwind-merge";

type TextInputProps = {
  error?: string;
  iconRight?: React.ReactNode | string;
  className?: string;
  other?: Record<string, any>;
};
const getBGIcon = (icon: string) => ({
  backgroundImage: `url('${icon}')`,
  backgroundRepeat: "no-repeat",
  backgroundPosition: "right 16px center",
});

const DumbTextInput = (props: TextInputProps) => {
  const { other, className, error, iconRight } = props;
  return (
    <div>
      <input
        style={typeof iconRight === "string" ? getBGIcon(iconRight) : {}}
        className={twMerge(
          className,
          "h-[48px] w-full rounded-[8px] border border-stroke bg-beige px-[16px] py-[12px] text-[16px] font-[200] focus:border-primary focus:bg-beige focus:outline-none sm:text-[14px]",
        )}
        {...other}
      />
    </div>
  );
};

export default DumbTextInput;
