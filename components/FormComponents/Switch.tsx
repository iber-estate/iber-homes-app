"use client";
import "react-toggle/style.css";
import React, { useEffect } from "react";
import { useFormContext } from "react-hook-form";
import "./switch.css";

type SwitchProps = {
  name: string;
  onChange?: (name: string, isChecked: boolean) => void;
  initialValue?: boolean;
};

const Switch = (props: SwitchProps) => {
  const { name, onChange, initialValue } = props;
  const { register, setValue, watch, getValues } = useFormContext();
  const value = watch(name);
  const switchField = register(name);

  return (
    <label className="cursor-pointer">
      <input
        type="checkbox"
        {...switchField}
        className="hidden"
        onChange={(e) => {
          switchField.onChange(e);
          onChange && onChange(name, e.target.checked);
        }}
      />
      <div
        className={` w-[44px] rounded-[23px] p-[2px]  transition-colors ${value ? "bg-buttonAquamarine" : "bg-[#999A9F]"}`}
      >
        <span
          className={`block h-[18px] w-[18px] rounded-[16px] bg-white shadow transition-all duration-300 ${value && "ml-[22px]"}`}
        ></span>
      </div>
    </label>
  );
};

export default Switch;
