import React, { useState } from "react";
import { set, useFormContext } from "react-hook-form";
import { Typography } from "@/components/Typography";
import SelectDumb from "@/components/SelectDumb";
import Card from "@/components/FilterComponents/Card";

type SelectProps = {
  name: string;
  options: { value: string; label: string }[];
  placeholder?: string;
  type?: "text" | "button";
  prefix?: React.ReactNode | string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  selectedHighlight?: boolean;
  setIsOpen?: (value: boolean) => void;
  className?: string;
};
type ListItemProps = {
  isSelected: boolean;
  label: string;
};

const ListItem = (props: ListItemProps) => {
  const { isSelected, label } = props;
  return (
    <div
      className={`gap-[15px] ${isSelected ? "bg-lightbluethree" : "transparent"} cursor-pointer px-[16px] py-[12px]`}
    >
      <Typography type="body" className="truncate">
        {label}
      </Typography>
    </div>
  );
};

// TODO: add keyboard support
const SelectInner = (props: SelectProps) => {
  const {
    options,
    name,
    onChange,
    selectedHighlight = true,
    setIsOpen,
  } = props;
  const { register, watch, setValue } = useFormContext();
  const value = watch(name);

  return (
    <Card className="flex flex-col">
      {options.map((option, index) => (
        <label
          key={option.value}
          className="flex-inline items-center hover:bg-lightbluethree"
        >
          <input
            {...register(name)}
            type="radio"
            value={option.value}
            className="hidden"
            onChange={(e) => {
              setIsOpen && setIsOpen(false);
              onChange ? onChange(e) : setValue(name, option.value);
            }}
            id={`checkbox-${index}`}
          />
          <ListItem
            isSelected={value === option.value && selectedHighlight}
            label={option.label}
          />
        </label>
      ))}
    </Card>
  );
};

export const Select = (props: SelectProps) => {
  const {
    type = "button",
    name,
    options,
    prefix,
    placeholder,
    className,
  } = props;
  const { watch } = useFormContext();
  const [isOpen, setIsOpen] = useState(false);
  const value = watch(name);
  const title = options.find((item) => item.value === value)?.label;
  return (
    <SelectDumb
      className={className}
      type={type}
      prefix={prefix}
      title={title}
      placeholder={placeholder}
      controls={{
        isOpen,
        setIsOpen,
      }}
    >
      <SelectInner {...props} setIsOpen={setIsOpen} />
    </SelectDumb>
  );
};
