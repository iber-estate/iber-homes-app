import React from "react";
import { Typography } from "@/components/Typography";

type InnerButtonProps = {
  isSelected: boolean;
  label: string | React.ReactNode;
  activeColor?: string;
  invertActiveTextColor?: boolean;
  onClick?: () => void;
};

type OuterButtonProps = InnerButtonProps & {
  size?: "small" | "big";
};

const BigButton = (props: InnerButtonProps) => {
  const { isSelected, label } = props;
  return (
    <button
      type="button"
      onClick={props.onClick}
      className={`${isSelected ? "bg-primary-light" : "bg-lightgray"} cursor-pointer rounded-[8px] px-[16px] py-[12px]`}
    >
      <Typography type="body2"> {label}</Typography>
    </button>
  );
};

const SmallButton = (props: InnerButtonProps) => {
  const { isSelected, label, activeColor, invertActiveTextColor } = props;
  return (
    <button
      type="button"
      onClick={props.onClick}
      className={`${isSelected ? activeColor || "bg-primary-light" : "transparent"} inline-flex h-[36px] min-w-[36px] cursor-pointer items-center justify-center rounded-[4px] border ${isSelected ? "border-transparent" : "border-lightbluethree"} px-[6px] py-[8px]`}
    >
      <Typography
        type="body2"
        className={
          invertActiveTextColor ? `${isSelected ? "text-white" : ""}` : ""
        }
      >
        {label}
      </Typography>
    </button>
  );
};

const FormButton = (props: OuterButtonProps) => {
  const { size } = props;
  return size === "small" ? (
    <SmallButton {...props} />
  ) : (
    <BigButton {...props} />
  );
};

export default FormButton;
