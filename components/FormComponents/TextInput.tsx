"use client";
import React from "react";
import { useFormContext } from "react-hook-form";
import { twMerge } from "tailwind-merge";
import { Typography } from "@/components/Typography";

type TextInputProps = {
  name: string;
  label?: string | React.ReactNode;
  placeholder?: string;
  type?: "text" | "number" | "textarea";
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  pattern?: RegExp;
  iconRight?: React.ReactNode;
  className?: string;
  ref?: React.MutableRefObject<HTMLInputElement | null>;
  onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
  onFocus?: (e: React.FocusEvent<HTMLInputElement>) => void;
  disabled?: boolean;
};

const TextInput = (props: TextInputProps) => {
  const {
    name,
    placeholder,
    label,
    type = "text",
    onChange,
    pattern,
    iconRight,
    className,
    ref: publicRef,
    onBlur: publicOnBlur,
    onFocus: publicOnFocus,
    disabled,
  } = props;
  const { register, formState } = useFormContext();
  const { ref, onBlur, ...rest } = register(name, {
    pattern,
    onChange,
  });

  return (
    <div>
      {label && (
        <Typography type="caption" className="mb-[4px]">
          {label}
        </Typography>
      )}
      {type === "textarea" ? (
        <textarea
          id={name}
          rows={4}
          cols={33}
          disabled={disabled}
          {...register(name)}
          style={{
            backgroundImage: iconRight ? `url('${iconRight}')` : "none",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "right 16px center",
          }}
          placeholder={placeholder || "Any"}
          className={twMerge(
            className,
            `${formState.errors[name] && "border-red-500"} mb-0 w-full rounded-[8px] border border-stroke bg-beige px-[16px] py-[12px] text-[16px] font-[200] focus:border-primary focus:bg-beige focus:outline-none`,
          )}
        />
      ) : (
        <input
          id={name}
          type={type}
          disabled={disabled}
          {...rest}
          ref={(e) => {
            ref(e);
            if (!publicRef) return;
            publicRef.current = e;
          }}
          onBlur={(e) => {
            onBlur(e);
            publicOnBlur && publicOnBlur(e);
          }}
          onFocus={(e) => {
            publicOnFocus && publicOnFocus(e);
          }}
          style={{
            backgroundImage: iconRight ? `url('${iconRight}')` : "none",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "right 16px center",
          }}
          placeholder={placeholder || "Any"}
          className={twMerge(
            // className,
            `${formState.errors[name] && "border-red-500"} h-[48px] w-full rounded-[8px] border border-stroke bg-beige px-[16px] py-[12px] text-[16px] font-[200] focus:border-primary focus:bg-beige focus:outline-none sm:h-[44px] sm:text-[14px]`,
            className,
          )}
        />
      )}
    </div>
  );
};

export default TextInput;
