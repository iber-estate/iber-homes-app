import React from "react";
import { useFormContext } from "react-hook-form";
import { Typography } from "@/components/Typography";
import checkWhite from "@/public/icons/checkWhite.svg";
import Image from "next/image";

type CheckboxButtonsProps = {
  name: string;
  options: { value: string; label: string }[];
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
};
type ButtonProps = {
  isSelected: boolean;
  label: string;
};
type CheckboxProps = {
  isSelected: boolean;
};
const Checkbox = ({ isSelected }: CheckboxProps) => (
  <div
    className={`flex h-[18px] w-[18px] items-center justify-center rounded-[4px] border  ${isSelected ? "border-transparent" : "border-primary-light"} ${isSelected ? "bg-primary" : "bg-transparent"}`}
  >
    {isSelected && <Image src={checkWhite} alt="" width={10} height={10} />}
  </div>
);
const ListItem = (props: ButtonProps) => {
  const { isSelected, label } = props;
  return (
    <div
      className={`flex items-center gap-[15px] ${isSelected ? "bg-lightbluethree" : "transparent"} cursor-pointer px-[16px] py-[12px]`}
    >
      <Checkbox isSelected={isSelected} />
      <Typography type="body2"> {label}</Typography>
    </div>
  );
};

// TODO: add keyboard support
export const CheckboxGroup = (props: CheckboxButtonsProps) => {
  const { options, name, onChange } = props;
  const { register, watch } = useFormContext();
  const values = watch(name);
  const checkboxField = register(name);
  const isSelected = (value: string) =>
    Array.isArray(values) ? values?.includes(value) : false;
  return (
    <div className="flex flex-col">
      {options?.map((option, index) => (
        <label key={option.value} className="flex-inline items-center">
          <input
            {...checkboxField}
            type="checkbox"
            value={option.value}
            onChange={(e) => {
              checkboxField.onChange(e);
              onChange && onChange(e);
            }}
            className="hidden"
            id={`checkbox-${index}`}
          />
          <ListItem
            isSelected={isSelected(option.value)}
            label={option.label}
          />
        </label>
      ))}
    </div>
  );
};
