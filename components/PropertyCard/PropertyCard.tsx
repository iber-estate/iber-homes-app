import React from "react";
import Link from "next/link";
import { Typography } from "@/components/Typography";
import Image from "next/image";
import { RealPropertyList, RealPropertyShort } from "@/generated";
import { strings } from "@/utils/strings";
import { Like } from "@/components/Like";
import { twMerge } from "tailwind-merge";
import Properties from "@/components/PropertyCard/Properties";

type PropertyCardProps = {
  property: RealPropertyList | RealPropertyShort | any;
  className?: string;
  actionButton?: React.ReactNode;
  extended?: boolean;
};

export const PropertyCard = (props: PropertyCardProps) => {
  const { extended, property, className, actionButton } = props;
  const isSaved: any = !!property.is_saved;

  return (
    <div className={twMerge("relative", className)}>
      <div className="absolute right-[14px] top-[14px] ">
        {actionButton ? (
          actionButton
        ) : (
          <Like id={property.id || 0} is_saved={isSaved} />
        )}
      </div>
      <Link href={`/property/${property.id}`} className=" outline-none">
        <Image
          src={
            property.photos[0]?.thumbnail ||
            "https://pbs.twimg.com/media/EkjrKl5XUAEcwEl.jpg"
          }
          alt=""
          className="mb-[16px] aspect-[323/220] w-full rounded-[8px]"
          width={323}
          height={220}
        />
        {extended && (
          <Properties
            bathrooms={property.bathrooms}
            floor={property.floor}
            bedrooms={property.bedrooms}
          />
        )}
        <Typography
          type="h6"
          className="mb-[4px] leading-[28px] text-primary-dark sm:text-[16px] sm:leading-[20px]"
        >
          {property.name}
        </Typography>
        <Typography type="body" className="mb-[8px] truncate sm:text-[14px]">
          {property.address?.district?.city?.name ||
          property.address?.district?.city?.country?.name
            ? `${property.address?.district?.city?.name}, `
            : property.address?.line_1}
        </Typography>
        <span className="sm:text-14px text-[16px] font-[500] leading-[22px] sm:leading-[22px]">
          ${property.price && strings.addCommas(`${property?.price}`)}
        </span>
      </Link>
    </div>
  );
};
