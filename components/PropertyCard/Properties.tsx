import React from "react";
import Image from "next/image";

type PropertiesProps = {
  bedrooms: string;
  bathrooms: string;
  floor: string;
};

const Properties = (props: PropertiesProps) => {
  const { bedrooms, bathrooms, floor } = props;
  return (
    <div className="mb-[8px] flex items-end gap-[8px] font-light">
      <span className="flex items-center gap-[4px]">
        <Image src="/icons/bed.svg" alt="beds" width={22} height={22} />
        <span>{bedrooms}</span>
      </span>
      <span className="flex items-center gap-[4px]">
        <Image src="/icons/bath.svg" alt="beds" width={22} height={22} />
        <span>{bathrooms}</span>
      </span>
      <span className="flex items-center gap-[4px]">
        <Image src="/icons/stair.svg" alt="beds" width={22} height={22} />
        <span>{floor || 1}</span>
      </span>
    </div>
  );
};

export default Properties;
