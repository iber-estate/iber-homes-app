"use client";
import React, { useEffect, useState } from "react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { OpenAPI } from "@/generated";
import { SessionProvider } from "next-auth/react";
import { StoreProvider } from "@/store/StoreProvider";

const ContextsProviders = (props: { children: React.ReactNode }) => {
  useEffect(() => {
    const prevLocation = window.localStorage.getItem("locationBeforeAuth");
    if (prevLocation) {
      window.localStorage.removeItem("locationBeforeAuth");
      window.location.replace(prevLocation);
    }
    OpenAPI.BASE =
      window.location.origin === "http://localhost:3000"
        ? `https://app-dev.iber.homes/api/v1`
        : `${window.location.origin}/api/v1`;
  }, []);

  const [queryClient] = useState(
    () =>
      new QueryClient({
        defaultOptions: {
          queries: {
            placeholderData: (prev: any) => prev,
          },
        },
      }),
  );

  return (
    <SessionProvider basePath="/functions/auth">
      <QueryClientProvider client={queryClient}>
        <StoreProvider>{props.children}</StoreProvider>
      </QueryClientProvider>
    </SessionProvider>
  );
};

export default ContextsProviders;
