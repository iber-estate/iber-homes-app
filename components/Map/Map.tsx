"use client";
import React, { useEffect, useState } from "react";
import mapboxgl from "mapbox-gl";
import { PropertyComplexList, RealPropertyList } from "@/generated";
import Button from "@/components/Button";
import { Icons } from "@/components/Icons";
import { Typography } from "@/components/Typography";

type MapProps = {
  property: RealPropertyList | PropertyComplexList;
  lat?: number;
  lng?: number;
  postfix?: string;
};
export const Map = (props: MapProps) => {
  const [mapMounted, setMapMounted] = useState(false);
  const [mapIsOpen, setMapIsOpen] = useState(false);
  const { property, lat, lng, postfix } = props;
  const coords: [number, number] = [
    lng || property.address.lng,
    lat || property.address.lat,
  ];

  useEffect(() => {
    if (mapMounted) return;
    setMapMounted(true);
  }, []);

  useEffect(() => {
    if (!mapMounted) return;
    mapboxgl.accessToken =
      "pk.eyJ1IjoicG9sb3ZpbmthIiwiYSI6ImNscnJmM3p2YzBkdmoybHBmcWs1ZWw0MGQifQ.nQ6xhl78fGJ4k1PB-YSY-w";
    const map = new mapboxgl.Map({
      container: `map-${postfix}`, // container ID
      style: `mapbox://styles/mapbox/streets-v11`, // style URL
      center: coords, // starting position [lng, lat]
      zoom: 12, // starting zoom
    });
    map.addControl(new mapboxgl.NavigationControl());
    const pinCoordinates = coords; // Replace with your desired coordinates

    // Create a new marker
    const marker = new mapboxgl.Marker().setLngLat(coords).addTo(map);
  }, [mapMounted]);

  // TODO: remove overflow hidden, multiple maps not works correctly
  return (
    <div
      className={`relative h-[380px] w-full overflow-hidden sm:rounded-[8px] ${mapIsOpen ? "z-50 sm:fixed sm:left-0 sm:top-0 sm:h-[100vh] sm:w-[100vw]" : "sm:h-[178px]"}`}
    >
      {!mapIsOpen ? (
        <div className="l-0 t-0 absolute z-50 hidden h-full w-full items-center justify-center bg-[rgba(0,0,0,0.2)] sm:flex">
          <Button
            variant="secondary"
            onClick={() => {
              setMapIsOpen(true);
            }}
          >
            View map
          </Button>
        </div>
      ) : (
        <div className="bg-beige p-[20px]">
          <button
            className="flex items-center gap-[4px]"
            onClick={() => {
              setMapIsOpen(false);
            }}
          >
            <Icons.ArrowIcon className="h-[18px] w-[18px] rotate-180" />
            <Typography type="body">Back to card</Typography>
          </button>
        </div>
      )}
      <div
        id={`map-${postfix}`}
        className="h-[380px] w-full overflow-hidden rounded-[8px] sm:h-[100vh] sm:w-[100vw]"
      ></div>
    </div>
  );
};
