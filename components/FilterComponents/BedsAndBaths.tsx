import React, { useEffect } from "react";
import SelectDumb from "@/components/SelectDumb";
import Card from "@/components/FilterComponents/Card";
import { Typography } from "@/components/Typography";
import { ChooseButtons } from "@/components/FormComponents/ChooseButtons";
import { useFormContext } from "react-hook-form";
import { FieldsNames } from "@/app/(main)/search/types";

const Block = ({ children }: { children: React.ReactNode }) => {
  return <div className="px-[16px] py-[8px] sm:px-0">{children}</div>;
};

const options = [
  { label: "1", value: "1" },
  { label: "2", value: "2" },
  { label: "3", value: "3" },
  { label: "4+", value: "4+" },
  { label: "Any", value: "any" },
];

const BedsAndBaths = () => {
  const { watch, setValue, getValues } = useFormContext();
  const beds: string = watch(FieldsNames.BEDS);
  const qwe = getValues(FieldsNames.BEDS);
  const baths: string = watch(FieldsNames.BATHS);

  const getTitle = (beds?: string, baths?: string) => {
    const bedsTitle = beds === "any" ? "Any" : beds;
    const bathsTitle = baths === "any" ? "Any" : baths;
    let title = `${bedsTitle?.length ? `Beds ${bedsTitle}` : ""}, ${bathsTitle?.length ? `Baths ${bathsTitle}` : ""}`;
    title = title.trim();
    if (!title.replace(/,/g, "").trim()) return;
    if (title[0] === ",") return title.slice(1);
    if (title[title.length - 1] === ",") return title.slice(0, -1);
    return title;
  };

  const onChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    fieldName: string,
  ) => {
    if (!e.target.checked) return;
    if (e.target.value === "any") {
      setValue(fieldName, "any");
    } else if (getValues(fieldName)?.includes("any")) {
      setValue(fieldName, [e.target.value]);
    }
  };

  return (
    <div>
      <SelectDumb
        placeholder="Beds&Baths"
        title={getTitle(beds, baths)}
        className="sm:hidden"
      >
        <Card className="py-[4px]">
          <Block>
            <Typography type="body">Bedrooms</Typography>
          </Block>
          <Block>
            <ChooseButtons
              onChange={(e) => {
                onChange(e, FieldsNames.BEDS);
              }}
              options={options}
              name={FieldsNames.BEDS}
              size="small"
            />
          </Block>
          <Block>
            <Typography type="body">Bathrooms</Typography>
          </Block>
          <Block>
            <ChooseButtons
              onChange={(e) => {
                onChange(e, FieldsNames.BATHS);
              }}
              options={options}
              name={FieldsNames.BATHS}
              size="small"
            />
          </Block>
        </Card>
      </SelectDumb>
      <Card className="hidden py-[4px] sm:block">
        <Typography type="h4">Beds&Bath</Typography>
        <Block>
          <Typography type="body">Bedrooms</Typography>
        </Block>
        <Block>
          <ChooseButtons
            onChange={(e) => {
              onChange(e, FieldsNames.BEDS);
            }}
            options={options}
            name={FieldsNames.BEDS}
            size="small"
          />
        </Block>
        <Block>
          <Typography type="body">Bathrooms</Typography>
        </Block>
        <Block>
          <ChooseButtons
            onChange={(e) => {
              onChange(e, FieldsNames.BATHS);
            }}
            options={options}
            name={FieldsNames.BATHS}
            size="small"
          />
        </Block>
      </Card>
    </div>
  );
};

export default BedsAndBaths;
