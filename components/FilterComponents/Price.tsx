import React from "react";
import { SelectDumb } from "@/components/SelectDumb/SelectDumb";
import TextInput from "@/components/FormComponents/TextInput";
import { Typography } from "@/components/Typography";
import { strings } from "@/utils/strings";
import { useFormContext } from "react-hook-form";
import Card from "@/components/FilterComponents/Card";
import { FieldsNames, FilterType, SearchType } from "@/app/(main)/search/types";
import {
  getCleanFilterValues,
  getFiltersQuery,
} from "@/app/(main)/search/utils";
import { useRouter } from "next/navigation";

type PriceInput = string | undefined | null;
type PriceProps = {
  setCurrentFilters: (filters: SearchType) => void;
};
const PriceInner = (props: PriceProps) => {
  const { setCurrentFilters } = props;
  const { setValue, getValues } = useFormContext();
  const router = useRouter();

  const handlePrice = (name: string, value: string) => {
    const prevValue = value.slice(0, -1);
    // check if value is a string with spaces, dollar sign and numbers
    if (!/^[0-9 $]+$/g.test(value)) {
      setValue(name, prevValue);
      return;
    }
    const valueWithSpaces = strings.formatNumberWithSpaces(value);
    setValue(name, `${value && "$ "}${valueWithSpaces}`);
  };

  const clearPrice = () => {
    setValue(FieldsNames.MIN_PRICE, undefined);
    setValue(FieldsNames.MAX_PRICE, undefined);
    const values: any = {
      ...getValues(),
      [FieldsNames.MIN_PRICE]: undefined,
      [FieldsNames.MAX_PRICE]: undefined,
    };
    setCurrentFilters(getCleanFilterValues(values));
    router.push(getFiltersQuery(values));
  };

  const digitsWithSpaceAndDollar = /^[0-9 $]+$/;
  return (
    <Card className="flex flex-col gap-[16px] px-[16px] py-[12px] sm:grid sm:grid-cols-2 sm:px-0 sm:py-0">
      <div className="flex justify-between sm:hidden">
        <Typography type="body2">Price, USD</Typography>{" "}
        <button
          className="font-[300] text-primary"
          type="button"
          onClick={clearPrice}
        >
          Clear
        </button>
      </div>
      <div className="sm:col-span-1">
        <TextInput
          name={FieldsNames.MIN_PRICE}
          placeholder="Min"
          pattern={digitsWithSpaceAndDollar}
          onChange={(e) => {
            handlePrice(FieldsNames.MIN_PRICE, e.target.value);
          }}
        />
      </div>
      <div className="sm:col-span-1">
        <TextInput
          name={FieldsNames.MAX_PRICE}
          placeholder="Max"
          pattern={digitsWithSpaceAndDollar}
          onChange={(e) => {
            handlePrice(FieldsNames.MAX_PRICE, e.target.value);
          }}
        />
      </div>
    </Card>
  );
};

const Price = (props: PriceProps) => {
  const { watch } = useFormContext();
  const price_min = watch(FieldsNames.MIN_PRICE);
  const price_max = watch(FieldsNames.MAX_PRICE);

  function parsePrice(price: PriceInput): number | null {
    if (!price) return null; // Handle undefined, null, or empty string
    const numericPart = price.replace(/[^0-9]/g, ""); // Remove all non-digit characters
    return parseInt(numericPart, 10);
  }

  function formatPrice(price: number): string {
    if (price >= 1_000_000) {
      return `$${(price / 1_000_000).toFixed(1)}M`;
    } else if (price >= 10_000) {
      return `$${(price / 1_000).toFixed(0)}K`;
    } else {
      return `$${price}`;
    }
  }

  function generatePriceTitle(
    minPrice: PriceInput,
    maxPrice: PriceInput,
  ): string | undefined {
    const min = parsePrice(minPrice);
    const max = parsePrice(maxPrice);

    if (min !== null && max !== null) {
      return `${formatPrice(min)} – ${formatPrice(max)}`;
    } else if (min !== null) {
      return `from ${formatPrice(min)}`;
    } else if (max !== null) {
      return `up to ${formatPrice(max)}`;
    } else {
      return undefined;
    }
  }

  return (
    <div>
      <SelectDumb
        className="sm:hidden"
        title={generatePriceTitle(price_min, price_max)}
        placeholder="Price"
      >
        <PriceInner {...props} />
      </SelectDumb>
      <div className="hidden sm:block">
        <Typography type="h4">Price</Typography>
        <PriceInner {...props} />
      </div>
    </div>
  );
};

export default Price;
