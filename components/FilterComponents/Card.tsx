import React from "react";
import { Typography } from "@/components/Typography";
import { twMerge } from "tailwind-merge";

const Card = ({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) => {
  return (
    <div
      className={twMerge(
        className,
        "overflow-hidden rounded-[8px] border border-stroke bg-beige sm:border-none",
      )}
    >
      {children}
    </div>
  );
};

export default Card;
