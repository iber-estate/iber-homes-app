import React from "react";
import SelectDumb from "@/components/SelectDumb";
import { CheckboxGroup } from "@/components/FormComponents/CheckboxGroup";
import { useFormContext } from "react-hook-form";
import Card from "@/components/FilterComponents/Card";
import { FieldsNames } from "@/app/(main)/search/types";
import { ChooseButtons } from "@/components/FormComponents/ChooseButtons";
import { Typography } from "@/components/Typography";

type PropertyTypeProps = {
  propTypes: { value: string; label: string }[];
};
const PropertyType = (props: PropertyTypeProps) => {
  const { propTypes } = props;
  const { watch } = useFormContext();
  const property_type: string[] = watch(FieldsNames.PROPERTY_TYPE);
  const getTitle = (property_type: string[]) => {
    const titleArr = propTypes.reduce((acc, item) => {
      if (property_type.includes(item.value)) {
        acc.push(item.label);
      }
      return acc;
    }, [] as string[]);
    return titleArr.join(", ");
  };
  const title = property_type?.length ? getTitle(property_type) : undefined;

  return (
    <div className="sm:order-first">
      <SelectDumb
        placeholder="Property type"
        title={title}
        className="sm:hidden"
      >
        <Card>
          <CheckboxGroup options={propTypes} name={FieldsNames.PROPERTY_TYPE} />
        </Card>
      </SelectDumb>
      <Card className="hidden sm:block">
        <Typography type="h4">Property type</Typography>
        <ChooseButtons options={propTypes} name={FieldsNames.PROPERTY_TYPE} />
      </Card>
    </div>
  );
};

export default PropertyType;
