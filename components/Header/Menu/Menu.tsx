"use client";
import { MenuItem } from "@/components/Header/MenuItem";
import Link from "next/link";
import { Typography } from "@/components/Typography";
import React, { useState } from "react";
import { usePathname } from "next/navigation";
import Image from "next/image";
import { ArrowIcon } from "@/components/Icons";

interface MenuProps {
  containerClassName?: string;
  children?: React.ReactNode;
  closeOverlay?: any;
}

const menuLinks = [
  {
    name: "Properties",
    link: "/search",
    auth: false,
  },
  {
    name: "My listings",
    link: "/listings?tab=saved",
    auth: true,
  },
];

const SubmenuItem = ({ icon, title, subtitle, link, close }: any) => {
  return (
    <li className="multilevel-menu__elem cursor-pointer" onClick={close}>
      <Link href={link} className="flex w-[300px] gap-[16px] sm:w-auto">
        <Image
          src={icon}
          alt="invest icon"
          width={22}
          height={22}
          className="sm:hidden"
        />
        <div>
          <p className="multilevel-menu__elem-title w-full text-[16px] leading-[16px] text-primary-dark hover:text-primary sm:text-[16px] sm:font-[300] sm:leading-[20px]">
            {title}
          </p>
          <p className="w-full text-[14px] font-light leading-[21px] text-[#999A9F] sm:hidden">
            {subtitle}
          </p>
        </div>
      </Link>
    </li>
  );
};

export const Menu: React.FC<MenuProps> = (props) => {
  const { closeOverlay } = props;
  const pathname = usePathname();
  const [investOpen, setInvestOpen] = useState(false);

  return (
    <ul className="flex items-center space-x-[48px] sm:mb-[20px] sm:mr-[85px] sm:w-[240px] sm:flex-col sm:items-start sm:space-x-0">
      {menuLinks.map((item) => (
        <MenuItem menuItem={item} key={item.link} closeOverlay={closeOverlay} />
      ))}
      <li className="multilevel-menu relative gap-[12px]">
        <Link
          href="/investment"
          className="block h-[44px] pb-[8px] pt-[16px] sm:mr-[8px] sm:hidden sm:h-auto sm:py-[10px]"
        >
          <Typography
            type={pathname.includes("/invest") ? "subtitle3" : "body"}
            className="relative mb-[0] leading-[20px] text-primary-dark sm:text-[16px]"
          >
            <span
              className={`absolute -right-[10px] -top-[4px] block h-[8px] w-[8px] rounded-full`}
            />
            Investment
          </Typography>
        </Link>

        <button
          className="hidden sm:mr-[8px] sm:flex sm:h-auto sm:py-[10px]"
          onClick={() => setInvestOpen(!investOpen)}
        >
          <Typography
            type={pathname.includes("/invest") ? "subtitle3" : "body"}
            className={`relative mb-[0] leading-[20px] text-primary-dark sm:flex sm:text-[16px] ${investOpen && "text-primary"}`}
          >
            Investment
          </Typography>
          <span className="hidden sm:block">
            <ArrowIcon
              color={investOpen ? "#559D88" : "#104551"}
              className={investOpen ? "-rotate-90" : "rotate-90"}
              w="20px"
              h="20px"
            />
          </span>
        </button>
        <div
          className={`multilevel-menu__inner absolute z-50 rounded-[16px] bg-beige px-[32px] py-[40px] sm:relative sm:m-0 sm:p-0 ${investOpen ? "sm:block" : "sm:hidden"}`}
        >
          <ul className="flex flex-col gap-[24px] sm:gap-[20px] sm:pt-[10px]">
            <SubmenuItem
              link="/investment"
              title="About investment"
              subtitle="Information about the investment"
              icon="/icons/invest.svg"
              close={closeOverlay}
            />
            <SubmenuItem
              link="/invest-search"
              title="List of investment projects"
              subtitle=" All projects"
              icon="/icons/list.svg"
              close={closeOverlay}
            />
            <SubmenuItem
              link="/admin/invest-project/create"
              title="Fill in form"
              subtitle="Form to fill out investment data"
              icon="/icons/fill.svg"
              close={closeOverlay}
            />
          </ul>
        </div>
      </li>
    </ul>
  );
};
