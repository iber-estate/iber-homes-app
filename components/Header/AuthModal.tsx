"use client";
import React, { useContext, useState } from "react";
import { Auth } from "@/components/Auth";
import Modal from "@/components/Modal";
import { Profile } from "@/components/Header/Profile";
import Button from "@/components/Button";
import { steps } from "@/types";
import { StoreContext } from "@/store/StoreProvider";
import { useSession } from "next-auth/react";

type AuthModalProps = {
  modalOnly?: boolean;
};
export const AuthModal = (props: AuthModalProps) => {
  const { modalOnly } = props;
  const session = useSession();
  const { data, setValue } = useContext(StoreContext);
  const [initialForm, setInitialForm] = useState<steps>("signup");

  const onClose = () => {
    setValue("isAuthModalOpen", false);
  };
  const onOpen = (formId: steps) => {
    setInitialForm(formId);
    setValue("isAuthModalOpen", true);
  };

  if (modalOnly) {
    return (
      <Modal isOpen={data.isAuthModalOpen} onClose={onClose}>
        <Auth initialState={initialForm} onClose={onClose} />
      </Modal>
    );
  }

  return (
    <div>
      <Modal isOpen={data.isAuthModalOpen} onClose={onClose}>
        <Auth initialState={initialForm} onClose={onClose} />
      </Modal>
      {session.data ? (
        <Profile name={session.data.user?.name || "No name"} />
      ) : (
        <div className="flex gap-[16px] sm:mr-[85px] sm:flex-col">
          <Button
            type="button"
            variant="secondary"
            onClick={() => onOpen("signin")}
          >
            Log in
          </Button>
          <Button type="button" onClick={() => onOpen("signup")}>
            Sign up
          </Button>
        </div>
      )}
    </div>
  );
};
