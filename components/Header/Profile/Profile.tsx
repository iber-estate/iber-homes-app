"use client";
import React, { useRef, useState } from "react";
import { logout } from "@/actions";
import Image from "next/image";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import { CrossIcon } from "@/components/Icons";
import { useClickOutside } from "@/utils/hooks/useClickOutside";

type ProfileProps = {
  name: string;
};

export const Profile = (props: ProfileProps) => {
  const { name } = props;
  const [modalOpen, setModalOpen] = useState(false);
  const ref = useRef(null);
  useClickOutside(ref, () => {
    setModalOpen(false);
  });
  const toggleModal = () => {
    setModalOpen((prev) => !prev);
  };
  return (
    <div className="relative" ref={ref}>
      <button
        className="rounded-full bg-primary-light p-[10px] sm:hidden"
        onClick={toggleModal}
      >
        <Image src="/icons/profile.svg" alt="profile" width={24} height={24} />
      </button>
      <button
        className="hidden gap-[8px] sm:flex sm:py-[14px]"
        onClick={() => {
          logout(window.location.href);
        }}
      >
        <Image src="/icons/exit.svg" alt="exit icon" width={16} height={16} />
        <Typography type="subtitle2" className="text-buttonAquamarine">
          Sign out
        </Typography>
      </button>
      {modalOpen && (
        <div className="absolute -bottom-[28px] right-0 z-10 mb-[24px] w-[360px] translate-y-full rounded-[4px] border border-[#ececec] bg-beige px-[24px] py-[28px] pt-[22px] shadow">
          <header className="flex w-full justify-end">
            <button
              type="button"
              className="p-[6px]"
              onClick={() => {
                setModalOpen(false);
              }}
            >
              <CrossIcon color="#999A9F" w="24" h="24" />
            </button>
          </header>
          <Typography type="h6"> {name}</Typography>
          <Button
            onClick={() => {
              logout(window.location.href);
            }}
          >
            Sign out
          </Button>
        </div>
      )}
    </div>
  );
};
