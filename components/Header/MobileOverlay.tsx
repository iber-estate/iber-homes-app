import React from "react";
import { Menu } from "./Menu";
import Image from "next/image";
import { AuthModal } from "@/components/Header/AuthModal";
import { Typography } from "@/components/Typography";
import { useSession } from "next-auth/react";

type MobileOverlayProps = {
  mobileMenuOpen: boolean;
  setMobileMenuOpen: (p: boolean) => void;
};

const MobileOverlay = (props: MobileOverlayProps) => {
  const session = useSession();
  const { setMobileMenuOpen, mobileMenuOpen } = props;
  if (!mobileMenuOpen)
    return (
      <div className="hidden sm:block">
        <AuthModal modalOnly />
      </div>
    );

  const onClose = (e: any) => {
    setMobileMenuOpen(false);
  };

  return (
    <div className="fixed left-0 top-0 z-10 hidden h-[100vh] w-[100vw] sm:block">
      <div
        className="absolute left-0 top-0 h-full w-full cursor-pointer bg-slate-800 opacity-[0.5]"
        onClick={onClose}
      />
      <div className="absolute right-0 top-0 z-50 h-[100vh] bg-beige p-[20px]">
        <button className="mb-[4px] ml-auto block" onClick={onClose}>
          <Image src="/icons/cross.svg" width={16} height={16} alt={"cross"} />
        </button>
        <Typography type="h6" className="hidden sm:block sm:text-primary-dark">
          {session.data?.user?.name}
        </Typography>
        <Menu closeOverlay={onClose} />
        <AuthModal />
      </div>
    </div>
  );
};

export default MobileOverlay;
