"use client";
import React from "react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { Typography } from "@/components/Typography";
import { useSession } from "next-auth/react";
import { useStore } from "@/store/useStore";
import { useQuery } from "@tanstack/react-query";
import { queryKeys } from "@/types";
import { UserService } from "@/generated";

interface MenuItemProps {
  menuItem?: any;
  closeOverlay?: any;
}

export const MenuItem = (props: MenuItemProps) => {
  const session = useSession();
  const { setValue } = useStore();
  const { menuItem, closeOverlay } = props;
  const { link, auth, name } = menuItem;
  const pathname = usePathname();
  const isSelected = pathname.includes(link.split("?")[0]);

  const openAuthModal = () => {
    setValue("isAuthModalOpen", true);
  };

  const getLink = () => {
    if (auth && !session.data?.user) {
      return "";
    }
    return link;
  };

  const { data: user } = useQuery({
    queryFn: async () => UserService.userMy({}),
    queryKey: [queryKeys.me],
    enabled: link === "/listings?tab=saved",
  });
  const myUser: any = link === "/listings?tab=saved" ? user : {};
  return (
    <li
      onClick={() => {
        closeOverlay && closeOverlay();
        !getLink() && openAuthModal();
      }}
    >
      <Link
        className="block h-[44px] pb-[8px] pt-[16px] sm:h-auto sm:py-[10px]"
        href={getLink()}
      >
        <Typography
          type={isSelected ? "subtitle3" : "body"}
          className="relative mb-[0] leading-[20px] text-primary-dark sm:text-[16px]"
        >
          <span
            className={`absolute -right-[10px] -top-[4px] block h-[8px] w-[8px] rounded-full ${myUser?.new_notification ? "bg-[#DF1010]" : "transparent"}`}
          />
          {name}
        </Typography>
      </Link>
    </li>
  );
};
