"use client";
import React, { useEffect, useState } from "react";
import { Menu } from "@/components/Header/Menu";
import logo from "@/public/icons/logo.svg";
import Link from "next/link";
import Image from "next/image";
import { AuthModal } from "@/components/Header/AuthModal";
import MobileOverlay from "@/components/Header/MobileOverlay";
import { useSearchParams } from "next/navigation";
import { useStore } from "@/store/useStore";

export const Header = () => {
  const searchParams = useSearchParams();
  const { setValue } = useStore();
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  useEffect(() => {
    const isAuth = searchParams.get("auth");
    if (isAuth) {
      setValue("isAuthModalOpen", true);
    }
  }, []);
  return (
    <div className="wrapper default-grid m-auto mb-[48px] pt-[28px] sm:mb-0 sm:py-[20px]">
      <MobileOverlay
        setMobileMenuOpen={setMobileMenuOpen}
        mobileMenuOpen={mobileMenuOpen}
      />
      <div className="col-span-6 flex items-center justify-between sm:flex-col">
        <div className="flex items-end sm:w-full sm:items-center sm:justify-between ">
          <Link href="/" className="mr-[108px]">
            <Image src={logo} alt="logo" />
          </Link>
          <div className="sm:hidden">
            <Menu />
          </div>
          <button
            className="hidden flex-col gap-[4px] sm:flex"
            onClick={() => {
              setMobileMenuOpen(true);
            }}
          >
            <span className="block h-[3px] w-[32px] rounded-[1px] bg-primary-dark"></span>
            <span className="block h-[3px] w-[32px] rounded-[1px] bg-primary-dark"></span>
            <span className="block h-[3px] w-[32px] rounded-[1px] bg-primary-dark"></span>
          </button>
        </div>
        <div className="sm:hidden">
          <AuthModal />
        </div>
      </div>
    </div>
  );
};
