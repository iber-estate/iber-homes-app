"use client";
import React from "react";
import { Typography } from "@/components/Typography";
import { ArrowIcon } from "@/components/Icons";
import { useRouter } from "next/navigation";

export const Breadcrumbs = ({
  startTitle,
  endTitle,
  prevLink,
}: {
  prevLink?: string;
  endTitle: string;
  startTitle: string;
}) => {
  const router = useRouter();
  return (
    <div className="mb-[10px] flex items-center">
      <button
        onClick={() => {
          if (prevLink) {
            router.push(prevLink);
            return;
          }
          router.back();
        }}
      >
        <Typography type="body" className="mr-[4px] text-primary-dark">
          {startTitle}
        </Typography>
      </button>
      <ArrowIcon color="#104551" className="mr-[4px]" />
      <Typography type="body">{endTitle}</Typography>
    </div>
  );
};
