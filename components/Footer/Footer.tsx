import Image from "next/image";
import { Typography } from "@/components/Typography";
import Link from "next/link";
import inst from "@/public/icons/inst-outlined.svg";
import tg from "@/public/icons/tg-outlined.svg";
import inIcon from "@/public/icons/in-outlined.svg";

const Links = ({ classname }: { classname: string }) => (
  <div className={classname}>
    <Typography type="body" className="mb-[8px] sm:hidden">
      Iber Home & Invest
    </Typography>
    <Typography
      type="subtitle2"
      className="mb-[8px] hidden text-black sm:block sm:w-[144px]"
    >
      Iber Home & Invest
    </Typography>
    <div className="flex gap-[16px]">
      <Link href="https://www.instagram.com/iconsult_iber/" target="_blank">
        <Image src={inst} alt="inst" width={32} height={32} />
      </Link>
      <Link href="https://www.linkedin.com/company/iberrest/" target="_blank">
        <Image src={inIcon} alt="in" width={32} height={32} />
      </Link>
      <Link href="https://t.me/IberhomesRu" target="_blank">
        <Image src={tg} alt="tg" width={32} height={32} />
      </Link>
    </div>
  </div>
);

export function Footer() {
  return (
    <section className="w-fit bg-primary-light xl:w-[100vw]">
      <div className="wrapper default-grid pb-[80px] pt-[80px] sm:gap-0 sm:pb-[28px] sm:pt-[60px]">
        <div className="col-span-2 sm:col-span-6">
          <Link href="/">
            {" "}
            <Image
              src="/icons/logo.svg"
              width={67}
              height={44}
              alt="logo-white"
              className="mb-[18px]"
            />
          </Link>
          <Links classname="sm:hidden" />
        </div>
        <div className="col-span-2 flex justify-center sm:col-span-6 sm:mb-[20px] sm:justify-start">
          <div className="flex flex-col gap-[24px] sm:gap-[20px]">
            <Link href="/search">
              <Typography type="body" className="text-black">
                Browse
              </Typography>
            </Link>
            <Link href="https://drive.google.com/file/d/1g5aKbTPG1PN8HyKZC-cU7ug8w5QVJ0XI/view?usp=sharing">
              <Typography type="body" className="text-black">
                Listings
              </Typography>
            </Link>
            <Link href="/investment">
              <Typography type="body" className="text-black">
                Investment
              </Typography>
            </Link>
          </div>
        </div>
        <div className="col-span-2 flex justify-center sm:col-span-6 sm:mb-[32px] sm:justify-start">
          <div className="flex flex-col gap-[24px] sm:gap-[20px]">
            <Typography type="body" className="text-black sm:font-[400]">
              Contacts
            </Typography>
            <Typography type="body" className="text-black">
              <span>Email: </span>
              <u>
                <Link href="mailto:info@iber.homes">info@iber.homes</Link>
              </u>
            </Typography>
            <Typography type="body" className="text-black">
              <span>Telegram: </span>
              <span>
                <u>
                  <Link href="https://t.me/iberhomes">@iberhomes</Link>
                </u>
              </span>
            </Typography>
          </div>
        </div>
        <div className="col-span-6 mx-auto flex w-full gap-[12px] pb-[16px] pt-[70px] sm:order-last sm:mx-0 sm:mb-[16px] sm:w-auto sm:min-w-[0] sm:justify-start sm:p-0 sm:px-[0px] sm:pt-[24px] sm:text-left">
          <Link href="/assets/privacy_policy.pdf" target="_blank">
            <Typography type="body" className="text-black">
              Terms and Conditions
            </Typography>
          </Link>
          <Link href="/assets/privacy_policy.pdf" target="_blank">
            <Typography type="body" className="text-black">
              Privacy Policy
            </Typography>
          </Link>
        </div>
        <Links classname="hidden sm:block col-span-6" />
      </div>
    </section>
  );
}
