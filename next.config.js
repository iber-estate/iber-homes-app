/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  async headers() {
    return [
      {
        source: "/listings",
        headers: [
          {
            key: "Cache-Control",
            value: "no-store",
          },
        ],
      },
    ];
  },
  output: "standalone",
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "pbs.twimg.com",
        port: "",
      },
      {
        protocol: "http",
        hostname: "app-dev.iber.homes",
        port: "",
      },
      {
        protocol: "https",
        hostname: "app-dev.iber.homes",
        port: "",
      },
      {
        protocol: "https",
        hostname: "app.iber.homes",
        port: "",
      },
      {
        protocol: "http",
        hostname: "app.iber.homes",
        port: "",
      },
      {
        protocol: "https",
        hostname: "app-demo.iber.homes",
        port: "",
      },
      {
        protocol: "http",
        hostname: "app-demo.iber.homes",
        port: "",
      },
    ],
  },
};

module.exports = nextConfig;
